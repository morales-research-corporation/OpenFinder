<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <location filename="../../build/src/ui_about.h" line="140"/>
        <source>About</source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="../about.ui" line="37"/>
        <location filename="../../build/src/ui_about.h" line="142"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="60"/>
        <location filename="../../build/src/ui_about.h" line="144"/>
        <source>The Desktop Experience</source>
        <translation>La Experiencia de Escritorio</translation>
    </message>
    <message>
        <location filename="../about.ui" line="70"/>
        <location filename="../../build/src/ui_about.h" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="99"/>
        <location filename="../../build/src/ui_about.h" line="146"/>
        <source>Programming:
* Simon Peter (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Application icon:
* Raphael Lopes (https://raphaellopes.me/)</source>
        <translation>Programación:
* Simón Pedro (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Ícono de aplicación:
* Raphael Lopes (https://raphaellopes.me/)</translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../../build/src/ui_about.h" line="154"/>
        <source>Filer

Copyright (C) 2020-21 Simon Peter
Copyright (C) 2021 Chris Moore

Originally based on PCMan File Manager
Portions Copyright (C) 2009-14 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Application icon

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

Copyright (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

Used with permission of the creator https://raphaellopes.me/</source>
        <translation>Filer

Copyright (C) 2020-21 Simón Pedro
Derechos de autor (C) 2021 Chris Moore

Basado originalmente en PCMan File Manager
Porciones Copyright (C) 2009-14 洪 任 諭 (Hong Jen Yee)

Este programa es software gratuito; puedes redistribuirlo y / o
modificarlo bajo los términos de la Licencia Pública General GNU
según lo publicado por la Free Software Foundation; ya sea la versión 2
de la Licencia, o (a su elección) cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea de utilidad,
pero SIN NINGUNA GARANTÍA; sin siquiera la garantía implícita de
COMERCIABILIDAD o APTITUD PARA UN PROPÓSITO EN PARTICULAR. Ver el
Licencia pública general GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General GNU
junto con este programa; si no, escriba al software libre
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, EE. UU.

Icono de aplicación

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

Copyright (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

Usado con permiso del creador https://raphaellopes.me/</translation>
    </message>
    <message>
        <source>Lightweight file manager</source>
        <translation type="vanished">Administrador de archivos ligero</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://lxqt.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://lxqt-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="90"/>
        <location filename="../../build/src/ui_about.h" line="153"/>
        <source>Authors</source>
        <translation>Autores</translation>
    </message>
    <message>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translatorcomment>Programado por:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;</translatorcomment>
        <translation type="vanished">Programación:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="116"/>
        <location filename="../../build/src/ui_about.h" line="184"/>
        <source>License</source>
        <translation>Licencia</translation>
    </message>
    <message>
        <source>PCMan File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation type="vanished">Administrador de archivos Filer

Copyright (C) 2009-2014 洪 任 諭 (Hong Jen Yee)

Este programa es software libre; puede redistribuirlo y / o
modificarlo bajo los términos de la Licencia Pública General GNU
publicada por la Fundación para el Software Libre; ya sea la versión 2
de la Licencia, o (a su elección) cualquier versión posterior.

Este programa se distribuye con la esperanza de que sea útil,
pero SIN NINGUNA GARANTÍA; ni siquiera la garantía implícita de
COMERCIALIZACIÓN o IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la
Licencia Pública General GNU para más detalles.

Debería haber recibido una copia de la Licencia Pública General GNU
junto con este programa; si no, escriba a la Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
</context>
<context>
    <name>AppChooserDialog</name>
    <message>
        <location filename="../app-chooser-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="150"/>
        <source>Choose an Application</source>
        <translation>Seleccione una Aplicación</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="36"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="151"/>
        <source>Installed Applications</source>
        <translation>Aplicaciones Instaladas</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="46"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="163"/>
        <source>Custom Command</source>
        <translation>Comando Personalizado</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="52"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="152"/>
        <source>Command line to execute:</source>
        <translation>Línea de comandos a ejecutar:</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="62"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="153"/>
        <source>Application name:</source>
        <translation>Nombre de la aplicación:</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="72"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="154"/>
        <source>&lt;b&gt;These special codes can be used in the command line:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Represents a single file name&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Represents multiple file names&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Represents a single URI of the file&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Represents multiple URIs&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation>&lt;b&gt;Estos códigos pueden ser usados en la línea de comando:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Representa un nombre de archivo&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Representa múltiples nombres de archivos&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Representa una URI para el archivo&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Representa múltiples URIs&lt;/li&gt;
&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="91"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="161"/>
        <source>Keep terminal window open after command execution</source>
        <translation>Mantener la terminal abierta luego de la ejecución del comando</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="98"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="162"/>
        <source>Execute in terminal emulator</source>
        <translation>Ejecutar en un emulador de terminal</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="109"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="164"/>
        <source>Set selected application as default action of this file type</source>
        <translation>Configurar la aplicación seleccionada como por defecto para este tipo de archivo</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <location filename="../../build/src/ui_autorun.h" line="108"/>
        <source>Removable medium is inserted</source>
        <translation>Dispositivo extraíble insertado</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <location filename="../../build/src/ui_autorun.h" line="110"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;Dispositivo extraíble insertado&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <location filename="../../build/src/ui_autorun.h" line="111"/>
        <source>Type of medium:</source>
        <translation>Tipo de dispositivo:</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <location filename="../../build/src/ui_autorun.h" line="112"/>
        <source>Detecting...</source>
        <translation>Detectando...</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <location filename="../../build/src/ui_autorun.h" line="113"/>
        <source>Please select the action you want to perform:</source>
        <translation>Seleccione la acción que desea ejecutar:</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="72"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="73"/>
        <source>Desktop</source>
        <translation>Escritorio</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="74"/>
        <source>Desktop folder:</source>
        <translation>Carpeta de Escritorio:</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="76"/>
        <source>Image file</source>
        <translation>Archivo de imagen</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="81"/>
        <source>Folder path</source>
        <translation>Ruta de la carpeta</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="82"/>
        <source>&amp;Browse</source>
        <translation>E&amp;xaminar</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="205"/>
        <source>Desktop Preferences</source>
        <translation>Preferencias de Escritorio</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">General</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="20"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="206"/>
        <source>Background</source>
        <translation>Fondo</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="207"/>
        <source>Wallpaper mode:</source>
        <translation>Modo de papel tapiz:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="55"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="208"/>
        <source>Wallpaper image file:</source>
        <translation>Imagen de papel tapiz:</translation>
    </message>
    <message>
        <source>Select background color:</source>
        <translation type="vanished">Seleccione color de fondo:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="75"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="210"/>
        <source>Background color:</source>
        <translation>Color de fondo:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="84"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="212"/>
        <source>Image file</source>
        <translation>Archivo de imagen</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="90"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="217"/>
        <source>Image file path</source>
        <translation>Ruta de archivo de imagen</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="97"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="218"/>
        <source>&amp;Browse</source>
        <translation>E&amp;xaminar</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="109"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="219"/>
        <source>Label Text</source>
        <translation>Texto de etiquetas</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="147"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="222"/>
        <source>Text color:</source>
        <translation>Color de texto:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="160"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="223"/>
        <source>Shadow color:</source>
        <translation>Color de sombra:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="173"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="224"/>
        <source>Font:</source>
        <translation>Fuente:</translation>
    </message>
    <message>
        <source>Select  text color:</source>
        <translation type="vanished">Seleccione color de texto:</translation>
    </message>
    <message>
        <source>Select shadow color:</source>
        <translation type="vanished">Seleccione color de sombra:</translation>
    </message>
    <message>
        <source>Select font:</source>
        <translation type="vanished">Seleccione el tipo de letra:</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Avanzado</translation>
    </message>
    <message>
        <source>Show menus provided by window managers when desktop is clicked</source>
        <translation type="vanished">Mostrar menús de los administradores de ventanas cuando se hace click sobre el escritorio</translation>
    </message>
</context>
<context>
    <name>EditBookmarksDialog</name>
    <message>
        <location filename="../edit-bookmarks.ui" line="14"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="103"/>
        <source>Edit Bookmarks</source>
        <translation>Editar marcadores</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="42"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="106"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="47"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="105"/>
        <source>Location</source>
        <translation>Localización</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="67"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="107"/>
        <source>&amp;Add Item</source>
        <translation>&amp;Agregar elemento</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="77"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="108"/>
        <source>&amp;Remove Item</source>
        <translation>&amp;Eliminar elemento</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="102"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="109"/>
        <source>Use drag and drop to reorder the items</source>
        <translation>Arrastre y suelte para reordenar los elementos</translation>
    </message>
</context>
<context>
    <name>ExecFileDialog</name>
    <message>
        <location filename="../exec-file.ui" line="14"/>
        <location filename="../../build/src/ui_exec-file.h" line="114"/>
        <source>Execute file</source>
        <translation>Ejecutar archivo</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="39"/>
        <location filename="../../build/src/ui_exec-file.h" line="116"/>
        <source>&amp;Open</source>
        <translation>&amp;Abrir</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="52"/>
        <location filename="../../build/src/ui_exec-file.h" line="117"/>
        <source>E&amp;xecute</source>
        <translation>&amp;Ejecutar</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="62"/>
        <location filename="../../build/src/ui_exec-file.h" line="118"/>
        <source>Execute in &amp;Terminal</source>
        <translation>Ejecutar en &amp;Terminal</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="85"/>
        <location filename="../../build/src/ui_exec-file.h" line="119"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>FileOperationDialog</name>
    <message>
        <location filename="../file-operation-dialog.ui" line="25"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="124"/>
        <source>Destination:</source>
        <translation>Destino:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="48"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="126"/>
        <source>Processing:</source>
        <translation>Procesando:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="61"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="127"/>
        <source>Preparing...</source>
        <translation>Preparando...</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="68"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="128"/>
        <source>Progress</source>
        <translation>Progreso</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="88"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="129"/>
        <source>Time remaining:</source>
        <translation>Tiempo restante:</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <source>File Properties</source>
        <translation type="vanished">Propiedades de Archivo</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="133"/>
        <location filename="../../build/src/ui_file-props.h" line="365"/>
        <source>General</source>
        <translation>General</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="208"/>
        <location filename="../../build/src/ui_file-props.h" line="372"/>
        <source>Location:</source>
        <translation>Localización:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="139"/>
        <location filename="../../build/src/ui_file-props.h" line="366"/>
        <source>File type:</source>
        <translation>Tipo de archivo:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="317"/>
        <location filename="../../build/src/ui_file-props.h" line="380"/>
        <source>Open with</source>
        <translation>Abrir con</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="323"/>
        <location filename="../../build/src/ui_file-props.h" line="381"/>
        <source>Mime type:</source>
        <translation>Tipo Mime:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="162"/>
        <location filename="../../build/src/ui_file-props.h" line="368"/>
        <source>File size:</source>
        <translation>Tamaño del archivo:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="20"/>
        <location filename="../../build/src/ui_file-props.h" line="362"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="60"/>
        <location filename="../../build/src/ui_file-props.h" line="363"/>
        <source>TextLabel</source>
        <translation>Etiqueta de texto</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="185"/>
        <location filename="../../build/src/ui_file-props.h" line="370"/>
        <source>On-disk size:</source>
        <translation>Tamaño en disco:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="260"/>
        <location filename="../../build/src/ui_file-props.h" line="376"/>
        <source>Last modified:</source>
        <translation>Última modificación:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="423"/>
        <location filename="../../build/src/ui_file-props.h" line="385"/>
        <source>Everyone</source>
        <translation>Todos</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="234"/>
        <location filename="../../build/src/ui_file-props.h" line="374"/>
        <source>Link target:</source>
        <translation>Destino del enlace:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="346"/>
        <location filename="../../build/src/ui_file-props.h" line="383"/>
        <source>Open With:</source>
        <translation>Abrir con:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="283"/>
        <location filename="../../build/src/ui_file-props.h" line="378"/>
        <source>Last accessed:</source>
        <translation>Último acceso:</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Permisos</translation>
    </message>
    <message>
        <source>Ownership</source>
        <translation type="vanished">Propiedad</translation>
    </message>
    <message>
        <source>Group:</source>
        <translation type="vanished">Grupo:</translation>
    </message>
    <message>
        <source>Owner:</source>
        <translation type="vanished">Dueño:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="380"/>
        <location filename="../../build/src/ui_file-props.h" line="384"/>
        <source>Access Control</source>
        <translation>Control de acceso</translation>
    </message>
    <message>
        <source>Other:</source>
        <translation type="vanished">Otro:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="440"/>
        <location filename="../../build/src/ui_file-props.h" line="386"/>
        <source>Make the file executable</source>
        <translation>Marcar como ejecutable</translation>
    </message>
    <message>
        <source>Read</source>
        <translation type="vanished">Lectura</translation>
    </message>
    <message>
        <source>Write</source>
        <translation type="vanished">Escritura</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation type="vanished">Ejecución</translation>
    </message>
    <message>
        <source>Sticky</source>
        <translation type="vanished">Sticky</translation>
    </message>
    <message>
        <source>SetUID</source>
        <translation type="vanished">SetUID</translation>
    </message>
    <message>
        <source>SetGID</source>
        <translation type="vanished">SetGID</translation>
    </message>
    <message>
        <source>Advanced Mode</source>
        <translation type="vanished">Modo avanzado</translation>
    </message>
</context>
<context>
    <name>Filer::Application</name>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Name of configuration profile</source>
        <translation>Nombre del perfil ce configuración</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>PROFILE</source>
        <translation>PERFIL</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Run Filer as a daemon</source>
        <translation>Ejecutar Filer como un servicio</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Quit Filer</source>
        <translation>Cerrar Filer</translation>
    </message>
    <message>
        <source>Launch desktop manager</source>
        <translation type="vanished">Abrir administrador de escritorio</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Launch desktop manager (deprecated)</source>
        <translation>Lanzar gestor de escritorio (obsoleto)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>Cerrar administrador de escritorio si está en ejecución</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>Abrir diálogo de preferencias de escritorio en la página especificada</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <location filename="../application.cpp" line="220"/>
        <source>NAME</source>
        <translation>NOMBRE</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="207"/>
        <source>Open new window</source>
        <translation>Abrir nueva ventana</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="210"/>
        <source>Open Find Files utility</source>
        <translation>Abrir búsqueda de archivos</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>Configurar papel tapiz desde un archivo</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>FILE</source>
        <translation>ARCHIVO</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>Set mode of desktop wallpaper. MODE=(color|stretch|fit|center|tile)</source>
        <translation>Definir modo de papel tapiz. MODO =(color|estirar|ajustar|centrar|repetir)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>MODE</source>
        <translation>MODO</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="220"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>Abrir diálogo de preferencias en la página especificada</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>Files or directories to open</source>
        <translation>Archivos o directorios a abrir</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[Archivo 1, Archivo 2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="512"/>
        <location filename="../application.cpp" line="519"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="519"/>
        <source>Terminal emulator is not set.</source>
        <translation>El emulador de terminal no está configurado.</translation>
    </message>
</context>
<context>
    <name>Filer::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="43"/>
        <source>Open in file manager</source>
        <translation>Abrir en administrador de archivos</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="133"/>
        <source>Removable Disk</source>
        <translation>Disco Removible</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="49"/>
        <source>Fill with background color only</source>
        <translation>Llenar solamente con el color de fondo</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="50"/>
        <source>Transparent</source>
        <translation>Transparente</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="51"/>
        <source>Stretch to fill the entire screen</source>
        <translation>Estirar para llenar la pantalla</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="52"/>
        <source>Stretch to fit the screen</source>
        <translation>Estirar para ajustar a la pantalla</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Center on the screen</source>
        <translation>Centrar en la pantalla</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>Repetir la imagen hasta llenar la pantalla</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="145"/>
        <source>Image Files</source>
        <translation>Archivos de Imagen</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="439"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>&amp;Pegar en la posición actual</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="461"/>
        <source>Desktop Preferences</source>
        <translation>Preferencias de Escritorio</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="528"/>
        <source>Version: %1</source>
        <translation>Versión: %1</translation>
    </message>
</context>
<context>
    <name>Filer::GotoFolderDialog</name>
    <message>
        <location filename="../gotofolderwindow.cpp" line="47"/>
        <source>Go</source>
        <translation>Ir</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="48"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="59"/>
        <source>Go To Folder</source>
        <translation>Ir a la carpeta</translation>
    </message>
</context>
<context>
    <name>Filer::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="210"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>Eliminar texto (Ctrl+K)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>Version: %1</source>
        <translation>Versión: %1</translation>
    </message>
    <message>
        <source>&amp;Move to Trash</source>
        <translation type="vanished">&amp;Mover a la Papelera</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1464"/>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Switch user command is not set.</source>
        <translation>El comando para cambiar de usuario no está definido.</translation>
    </message>
</context>
<context>
    <name>Filer::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="192"/>
        <source>Icon View</source>
        <translation>Vista de Íconos</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="193"/>
        <source>Compact Icon View</source>
        <translation>Vista Compacta de Íconos</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="194"/>
        <source>Thumbnail View</source>
        <translation>Vista de Miniaturas</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="195"/>
        <source>Detailed List View</source>
        <translation>Lista Detallada</translation>
    </message>
</context>
<context>
    <name>Filer::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="246"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="278"/>
        <source>%n items</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="280"/>
        <source>1 item</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="436"/>
        <source>%1 items selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="440"/>
        <source>%1 item selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Free space: %1 (Total: %2)</source>
        <translation type="vanished">Espacio libre: %1 (Total %2)</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="265"/>
        <source>%1 available</source>
        <translation>%1 disponible</translation>
    </message>
    <message numerus="yes">
        <source>%n item(s)</source>
        <translation type="vanished">
            <numerusform>%n elemento</numerusform>
            <numerusform>%n elementos</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n hidden)</source>
        <translation type="vanished">
            <numerusform> (%n oculto)</numerusform>
            <numerusform> (%n ocultos)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 item(s) selected</source>
        <translation type="vanished">
            <numerusform>%1 elemento seleccionado</numerusform>
            <numerusform>%1 elementos seleccionados</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Filer::View</name>
    <message>
        <source>Open in New T&amp;ab</source>
        <translation type="vanished">&amp;Abrir en Nueva Pestaña</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="111"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Abrir en Nueva &amp;Ventana</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="119"/>
        <source>Open in Termina&amp;l</source>
        <translation>Abrir en Termina&amp;l</translation>
    </message>
</context>
<context>
    <name>FindFilesDialog</name>
    <message>
        <location filename="../file-search.ui" line="14"/>
        <source>Find Files</source>
        <translation>Buscar archivos</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="24"/>
        <source>Name/Location</source>
        <translation>Nombre/Posición</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="30"/>
        <source>File name patterns</source>
        <translation>Patrones de nombre de archivos</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="36"/>
        <source>Pattern:</source>
        <translation>Patrón:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="46"/>
        <location filename="../file-search.ui" line="221"/>
        <source>Case insensitive</source>
        <translation>No coincidir mayúsculas</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="53"/>
        <location filename="../file-search.ui" line="228"/>
        <source>Use regular expression</source>
        <translation>Usar expresión regular</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="63"/>
        <source>Places to search</source>
        <translation>Lugares donde buscar</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="76"/>
        <source>Add</source>
        <translation>Agregar</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="88"/>
        <source>Remove</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="117"/>
        <source>Search in sub directories</source>
        <translation>Buscar en sub directorios</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="124"/>
        <source>Search hidden files</source>
        <translation>Buscar archivos ocultos</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="135"/>
        <location filename="../file-search.ui" line="141"/>
        <source>File Type</source>
        <translation>Tipo de Archivo</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="147"/>
        <source>Only search for files of following types:</source>
        <translation>Solo buscar archivos de los siguientes tipos:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="154"/>
        <source>Text files</source>
        <translation>Archivos de texto</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="161"/>
        <source>Image files</source>
        <translation>Archivos de imágenes</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="168"/>
        <source>Audio files</source>
        <translation>Archivos de audio</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="175"/>
        <source>Video files</source>
        <translation>Archivos de video</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="182"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="206"/>
        <source>Content</source>
        <translation>Contenido</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="212"/>
        <source>File contains</source>
        <translation>Contenidos del archivo</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="252"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Propiedades</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="258"/>
        <source>File Size</source>
        <translation>Tamaño del Archivo</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="264"/>
        <source>Bigger than:</source>
        <translation>Mayor a:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="289"/>
        <source>Smaller than:</source>
        <translation>Menor a:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="317"/>
        <source>Last Modified Time</source>
        <translation>Fecha de Modificación</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="323"/>
        <source>Earlier than:</source>
        <translation>Antes de:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="340"/>
        <source>Later than:</source>
        <translation>Después de:</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserComboBox</name>
    <message>
        <location filename="../appchoosercombobox.cpp" line="79"/>
        <source>Customize</source>
        <translation>Personalizar</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserDialog</name>
    <message>
        <location filename="../appchooserdialog.cpp" line="262"/>
        <source>Select an application to open &quot;%1&quot; files</source>
        <translation>Seleccione una aplicación para abrir archivos &quot;%1&quot;</translation>
    </message>
</context>
<context>
    <name>Fm::CreateNewMenu</name>
    <message>
        <location filename="../createnewmenu.cpp" line="29"/>
        <source>Folder</source>
        <translation>Directorio</translation>
    </message>
    <message>
        <location filename="../createnewmenu.cpp" line="33"/>
        <source>Blank File</source>
        <translation>Archivo Vacío</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeModel</name>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="77"/>
        <source>Loading...</source>
        <translation>Cargando...</translation>
    </message>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="208"/>
        <source>&lt;No sub folders&gt;</source>
        <translation>&lt;No hay subdirectorios&gt;</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeView</name>
    <message>
        <location filename="../dirtreeview.cpp" line="220"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Abrir en Nueva Ven&amp;tana</translation>
    </message>
    <message>
        <location filename="../dirtreeview.cpp" line="226"/>
        <source>Open in Termina&amp;l</source>
        <translation>Abrir en Termina&amp;l</translation>
    </message>
</context>
<context>
    <name>Fm::DndActionMenu</name>
    <message>
        <location filename="../dndactionmenu.cpp" line="26"/>
        <source>Copy here</source>
        <translation>Copiar aquí</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="27"/>
        <source>Move here</source>
        <translation>Mover aquí</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="28"/>
        <source>Create symlink here</source>
        <translation>Crear enlace simbólico aquí</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="30"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
</context>
<context>
    <name>Fm::EditBookmarksDialog</name>
    <message>
        <location filename="../editbookmarksdialog.cpp" line="96"/>
        <source>New bookmark</source>
        <translation>Nuevo marcador</translation>
    </message>
</context>
<context>
    <name>Fm::ExecFileDialog</name>
    <message>
        <location filename="../execfiledialog.cpp" line="40"/>
        <source>This text file &apos;%1&apos; seems to be an executable script.
What do you want to do with it?</source>
        <translation>El archivo de texto &apos;%1&apos; parece ser un script ejecutable.
¿Qué desea hacer con él?</translation>
    </message>
    <message>
        <location filename="../execfiledialog.cpp" line="45"/>
        <source>This file &apos;%1&apos; is executable. Do you want to execute it?</source>
        <translation>El archivo &apos;%1&apos; es ejecutable. ¿Quiere ejecutarlo?</translation>
    </message>
</context>
<context>
    <name>Fm::FileMenu</name>
    <message>
        <location filename="../filemenu.cpp" line="93"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="136"/>
        <source>Open With...</source>
        <translation>Abrir con...</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="164"/>
        <source>Other Applications</source>
        <translation>Otras Aplicaciones</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="170"/>
        <source>Create &amp;New</source>
        <translation>Crear &amp;Nuevo</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="128"/>
        <source>&amp;Restore</source>
        <translation>&amp;Restaurar</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="99"/>
        <source>Show Contents</source>
        <translation>Mostrar Contenidos</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="177"/>
        <source>Cut</source>
        <translation>Cortar</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="181"/>
        <source>Copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="185"/>
        <source>Paste</source>
        <translation>Pegar</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="190"/>
        <source>&amp;Move to Trash</source>
        <translation>&amp;Mover a la Papelera</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="194"/>
        <source>Rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="242"/>
        <source>Get Info</source>
        <translation>Obtener Info</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="247"/>
        <source>&amp;Empty Trash</source>
        <translation>&amp;Papelera vacía</translation>
    </message>
    <message>
        <source>Extract to...</source>
        <translation type="vanished">Extraer en...</translation>
    </message>
    <message>
        <source>Extract Here</source>
        <translation type="vanished">Extraer aquí</translation>
    </message>
    <message>
        <source>Compress</source>
        <translation type="vanished">Comprimir</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Propiedades</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="349"/>
        <source>Output</source>
        <translation>Salida</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Borrar</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperation</name>
    <message>
        <location filename="../fileoperation.cpp" line="221"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="222"/>
        <source>Some files cannot be moved to trash can because the underlying file systems don&apos;t support this operation.
Do you want to delete them instead?</source>
        <translation>Algunos archivos no pueden moverse a la papelera porque el subsistema de archivos no permite esta operación.
¿Quiere eliminarlos en su lugar?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="263"/>
        <location filename="../fileoperation.cpp" line="279"/>
        <source>Confirm</source>
        <translation>Confirmación</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="264"/>
        <source>Do you want to delete the selected files?</source>
        <translation>¿Quiere borrar los archivos seleccionados?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="280"/>
        <source>Do you want to move the selected files to trash can?</source>
        <translation>¿Quiere mover los archivos seleccionados a la papelera?</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperationDialog</name>
    <message>
        <location filename="../fileoperationdialog.cpp" line="41"/>
        <source>Move files</source>
        <translation>Mover archivos</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="42"/>
        <source>Moving the following files to destination folder:</source>
        <translation>Moviendo los siguientes archivos al directorio destino:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="45"/>
        <source>Copy Files</source>
        <translation>Copiar archivos</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="46"/>
        <source>Copying the following files to destination folder:</source>
        <translation>Copiando los siguientes archivos al directorio destino:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="49"/>
        <source>Trash Files</source>
        <translation>Enviar archivos a la papelera</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="50"/>
        <source>Moving the following files to trash can:</source>
        <translation>Moviendo los siguientes archivos a la papelera:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="53"/>
        <source>Delete Files</source>
        <translation>Borrar archivos</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="54"/>
        <source>Deleting the following files</source>
        <translation>Borrando los siguientes archivos</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="59"/>
        <source>Create Symlinks</source>
        <translation>Crear enlaces simbólicos</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="60"/>
        <source>Creating symlinks for the following files:</source>
        <translation>Creando enlaces simbólicos para los siguientes archivos:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="63"/>
        <source>Change Attributes</source>
        <translation>Cambiar atributos</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="64"/>
        <source>Changing attributes of the following files:</source>
        <translation>Cambiando atributos para los siguientes archivos:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="69"/>
        <source>Restore Trashed Files</source>
        <translation>Restaurar archivos borrados</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="70"/>
        <source>Restoring the following files from trash can:</source>
        <translation>Restaurando los siguientes archivos desde la papelera:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="139"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
</context>
<context>
    <name>Fm::FilePropsDialog</name>
    <message>
        <location filename="../filepropsdialog.cpp" line="148"/>
        <source>View folder content</source>
        <translation>Ver contenido del directorio</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="149"/>
        <source>View and modify folder content</source>
        <translation>Ver y modificar el contenido del directorio</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="153"/>
        <source>Read</source>
        <translation>Lectura</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="154"/>
        <source>Read and write</source>
        <translation>Lectura y escritura</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="156"/>
        <source>Forbidden</source>
        <translation>Prohibido</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="274"/>
        <source>Files of different types</source>
        <translation>Archivos de diferentes tipos</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="304"/>
        <source>Multiple Files</source>
        <translation>Múltiples archivos</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="432"/>
        <source>Apply changes</source>
        <translation>Aplicar cambios</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="433"/>
        <source>Do you want to recursively apply these changes to all files and sub-folders?</source>
        <translation>¿Quiere aplicar los cambios a todos los archivos y subdirectorios?</translation>
    </message>
</context>
<context>
    <name>Fm::FileSearchDialog</name>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>You should add at least add one directory to search.</source>
        <translation>Debe agregar al menos un directorio para buscar.</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="127"/>
        <source>Select a folder</source>
        <translation>Seleccionar una carpeta</translation>
    </message>
</context>
<context>
    <name>Fm::FolderMenu</name>
    <message>
        <location filename="../foldermenu.cpp" line="37"/>
        <source>Create &amp;New</source>
        <translation>Crear &amp;Nuevo</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="44"/>
        <source>&amp;Paste</source>
        <translation>&amp;Pegar</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="50"/>
        <source>Select &amp;All</source>
        <translation>Seleccion&amp;ar todo</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="54"/>
        <source>Invert Selection</source>
        <translation>Invertir Selección</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="60"/>
        <source>Sorting</source>
        <translation>Ordenar</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="65"/>
        <source>Show Hidden</source>
        <translation>Mostrar Ocultos</translation>
    </message>
    <message>
        <source>Folder Pr&amp;operties</source>
        <translation type="vanished">Pr&amp;opiedades del directorio</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="vanished">Directorio</translation>
    </message>
    <message>
        <source>Blank File</source>
        <translation type="vanished">Archivo Vacío</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="73"/>
        <source>Get Info</source>
        <translation>Obtener Info</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="99"/>
        <source>By File Name</source>
        <translation>Por nombre de archivo</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="100"/>
        <source>By Modification Time</source>
        <translation>Por fecha de modificación</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="101"/>
        <source>By File Size</source>
        <translation>Por tamaño de archivo</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="102"/>
        <source>By File Type</source>
        <translation>Por tipo de archivo</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="103"/>
        <source>By File Owner</source>
        <translation>Por dueño del archivo</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="115"/>
        <source>Ascending</source>
        <translation>Ascendente</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="120"/>
        <source>Descending</source>
        <translation>Descendente</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="135"/>
        <source>Folder First</source>
        <translation>Primero Directorios</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="144"/>
        <source>Case Sensitive</source>
        <translation>Coincidir mayúsculas</translation>
    </message>
</context>
<context>
    <name>Fm::FolderModel</name>
    <message>
        <location filename="../foldermodel.cpp" line="313"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="316"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="319"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="322"/>
        <source>Modified</source>
        <translation>Modificado</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="325"/>
        <source>Owner</source>
        <translation>Dueño</translation>
    </message>
</context>
<context>
    <name>Fm::FontButton</name>
    <message>
        <location filename="../fontbutton.cpp" line="46"/>
        <source>Bold</source>
        <translation>Negrita</translation>
    </message>
    <message>
        <location filename="../fontbutton.cpp" line="50"/>
        <source>Italic</source>
        <translation>Itálica</translation>
    </message>
</context>
<context>
    <name>Fm::MountOperationPasswordDialog</name>
    <message>
        <location filename="../mountoperationpassworddialog.cpp" line="40"/>
        <source>&amp;Connect</source>
        <translation>&amp;Conectar</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesModel</name>
    <message>
        <location filename="../placesmodel.cpp" line="82"/>
        <source>Places</source>
        <translation>Lugares</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="90"/>
        <source>Desktop</source>
        <translation>Escritorio</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="99"/>
        <source>Computer</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <source>Applications</source>
        <translation type="vanished">Aplicaciones</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="126"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="40"/>
        <source>Devices</source>
        <translation>Dispositivos</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="137"/>
        <source>Bookmarks</source>
        <translation>Marcadores</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="212"/>
        <source>Trash</source>
        <translation>Papelera</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesView</name>
    <message>
        <location filename="../placesview.cpp" line="362"/>
        <source>Empty Trash</source>
        <translation>Vaciar Papelera</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Renombrar</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Borrar</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="351"/>
        <source>Open in New Window</source>
        <translation>Abrir en una Nueva Ventana</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="371"/>
        <source>Move Bookmark Up</source>
        <translation>Mover Marcador hacia Arriba</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="376"/>
        <source>Move Bookmark Down</source>
        <translation>Mover Marcador hacia Abajo</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="380"/>
        <source>Rename Bookmark</source>
        <translation>Renombrar Marcador</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="383"/>
        <source>Remove Bookmark</source>
        <translation>Eliminar Marcador</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="392"/>
        <location filename="../placesview.cpp" line="409"/>
        <source>Unmount</source>
        <translation>Desmontar</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="396"/>
        <source>Mount</source>
        <translation>Montar</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="402"/>
        <source>Eject</source>
        <translation>Expulsar</translation>
    </message>
</context>
<context>
    <name>Fm::RenameDialog</name>
    <message>
        <location filename="../renamedialog.cpp" line="50"/>
        <location filename="../renamedialog.cpp" line="69"/>
        <source>Type: %1
Size: %2
Modified: %3</source>
        <translation>Tipo: %1
Tamaño: %2
Modificado: %3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="56"/>
        <source>Type: %1
Modified: %2</source>
        <translation>Tipo: %1
Modificado: %2</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="75"/>
        <source>Type: %1
Modified: %3</source>
        <translation>Tipo: %1
Modificado: %3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="89"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Sobreescribir</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="91"/>
        <source>&amp;Rename</source>
        <translation>&amp;Renombrar</translation>
    </message>
</context>
<context>
    <name>Fm::SidePane</name>
    <message>
        <location filename="../sidepane.cpp" line="49"/>
        <location filename="../sidepane.cpp" line="133"/>
        <source>Places</source>
        <translation>Lugares</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="50"/>
        <location filename="../sidepane.cpp" line="135"/>
        <source>Directory Tree</source>
        <translation>Árbol de Directorios</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="143"/>
        <source>Shows list of common places, devices, and bookmarks in sidebar</source>
        <translation>Muestra lista de lugares comunes, dispositivos y marcadores en la barra lateral</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="145"/>
        <source>Shows tree of directories in sidebar</source>
        <translation>Muestra árbol de directorios en barra lateral</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <location filename="../../build/src/ui_main-win.h" line="602"/>
        <source>File Manager</source>
        <translation>Administrador de archivos</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="277"/>
        <location filename="../../build/src/ui_main-win.h" line="608"/>
        <source>Ctrl+Up</source>
        <translation>Ctrl+Arriba</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="289"/>
        <location filename="../../build/src/ui_main-win.h" line="612"/>
        <source>Ctrl+Shift+H</source>
        <translation>Ctrl+Mayúsc+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="301"/>
        <location filename="../../build/src/ui_main-win.h" line="616"/>
        <source>Ctrl+Shift+R</source>
        <translation>Ctrl+Mayúsc+R</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="334"/>
        <location filename="../../build/src/ui_main-win.h" line="620"/>
        <source>&amp;About Filer</source>
        <translation>&amp;Sobre Filer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="389"/>
        <location filename="../main-win.ui" line="723"/>
        <location filename="../../build/src/ui_main-win.h" line="639"/>
        <location filename="../../build/src/ui_main-win.h" line="735"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Mayúsc+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="735"/>
        <location filename="../../build/src/ui_main-win.h" line="739"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="740"/>
        <location filename="../../build/src/ui_main-win.h" line="741"/>
        <source>&amp;Find Files</source>
        <translation>&amp;Buscar Archivos</translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="vanished">F3</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="751"/>
        <location filename="../../build/src/ui_main-win.h" line="745"/>
        <source>Filter</source>
        <translation>Filtrar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="93"/>
        <location filename="../../build/src/ui_main-win.h" line="797"/>
        <source>Filter by string...</source>
        <translation>Filtrar por texto...</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="117"/>
        <location filename="../../build/src/ui_main-win.h" line="798"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="121"/>
        <location filename="../../build/src/ui_main-win.h" line="799"/>
        <source>C&amp;reate New</source>
        <translation>C&amp;rear Nueva</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <location filename="../../build/src/ui_main-win.h" line="800"/>
        <source>&amp;Help</source>
        <translation>Ay&amp;uda</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="146"/>
        <location filename="../../build/src/ui_main-win.h" line="801"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="150"/>
        <location filename="../../build/src/ui_main-win.h" line="802"/>
        <source>&amp;Sorting</source>
        <translation>&amp;Ordenar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="176"/>
        <location filename="../../build/src/ui_main-win.h" line="803"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="194"/>
        <location filename="../../build/src/ui_main-win.h" line="804"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Marcadores</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="201"/>
        <location filename="../../build/src/ui_main-win.h" line="805"/>
        <source>&amp;Go</source>
        <translation>&amp;Ir</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="222"/>
        <location filename="../../build/src/ui_main-win.h" line="806"/>
        <source>&amp;Tool</source>
        <translation>&amp;Herramientas</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="239"/>
        <location filename="../../build/src/ui_main-win.h" line="807"/>
        <source>Main Toolbar</source>
        <translation>Barra de Herramientas Principal</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="271"/>
        <location filename="../../build/src/ui_main-win.h" line="603"/>
        <source>Go &amp;Up</source>
        <translation>S&amp;ubir</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="274"/>
        <location filename="../../build/src/ui_main-win.h" line="605"/>
        <source>Go Up</source>
        <translation>Subir</translation>
    </message>
    <message>
        <source>Alt+Up</source>
        <translation type="vanished">Alt+Arriba</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Carpeta personal</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <location filename="../../build/src/ui_main-win.h" line="610"/>
        <source>&amp;Home</source>
        <translation>&amp;Inicio</translation>
    </message>
    <message>
        <source>Alt+Home</source>
        <translation type="vanished">Alt+Inicio</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <location filename="../../build/src/ui_main-win.h" line="614"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recargar</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="vanished">F5</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="313"/>
        <location filename="../../build/src/ui_main-win.h" line="618"/>
        <source>Go</source>
        <translation>Ir</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="325"/>
        <location filename="../../build/src/ui_main-win.h" line="619"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">&amp;Acerca de</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="343"/>
        <location filename="../../build/src/ui_main-win.h" line="621"/>
        <source>&amp;New Window</source>
        <translation>&amp;Nueva ventana</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <location filename="../../build/src/ui_main-win.h" line="623"/>
        <source>New Window</source>
        <translation>Nueva ventana</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <location filename="../../build/src/ui_main-win.h" line="626"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="357"/>
        <location filename="../../build/src/ui_main-win.h" line="628"/>
        <source>Show &amp;Hidden</source>
        <translation>Mo&amp;strar ocultos</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="366"/>
        <location filename="../../build/src/ui_main-win.h" line="629"/>
        <source>&amp;Computer</source>
        <translation>&amp;Sistema</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="369"/>
        <location filename="../../build/src/ui_main-win.h" line="631"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Mayúsc+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="378"/>
        <location filename="../../build/src/ui_main-win.h" line="633"/>
        <source>&amp;Trash</source>
        <translation>&amp;Papelera</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <location filename="../../build/src/ui_main-win.h" line="635"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Mayúsc+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <location filename="../../build/src/ui_main-win.h" line="637"/>
        <source>&amp;Network</source>
        <translation>&amp;Red</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="398"/>
        <location filename="../../build/src/ui_main-win.h" line="641"/>
        <source>&amp;Desktop</source>
        <translation>&amp;Escritorio</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="401"/>
        <location filename="../../build/src/ui_main-win.h" line="643"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Mayúsc+D</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="410"/>
        <location filename="../../build/src/ui_main-win.h" line="645"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>&amp;Agregar a marcadores</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="415"/>
        <location filename="../../build/src/ui_main-win.h" line="646"/>
        <source>&amp;Applications</source>
        <translation>&amp;Aplicaciones</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="418"/>
        <location filename="../../build/src/ui_main-win.h" line="648"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Mayúsc+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="426"/>
        <location filename="../../build/src/ui_main-win.h" line="650"/>
        <source>Reload</source>
        <translation>Recargar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="434"/>
        <location filename="../../build/src/ui_main-win.h" line="651"/>
        <source>&amp;Icon View</source>
        <translation>V&amp;ista de Íconos</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="437"/>
        <location filename="../../build/src/ui_main-win.h" line="653"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="445"/>
        <location filename="../../build/src/ui_main-win.h" line="655"/>
        <source>&amp;Compact View</source>
        <translation>Vista &amp;Compacta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="453"/>
        <location filename="../../build/src/ui_main-win.h" line="656"/>
        <source>&amp;Detailed List</source>
        <translation>Lista &amp;Detallada</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="456"/>
        <location filename="../../build/src/ui_main-win.h" line="658"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="464"/>
        <location filename="../../build/src/ui_main-win.h" line="660"/>
        <source>&amp;Thumbnail View</source>
        <translation>Vista de &amp;Miniaturas</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="473"/>
        <location filename="../../build/src/ui_main-win.h" line="661"/>
        <source>Cu&amp;t</source>
        <translation>Cor&amp;tar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="476"/>
        <location filename="../../build/src/ui_main-win.h" line="663"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="485"/>
        <location filename="../../build/src/ui_main-win.h" line="665"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copiar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="488"/>
        <location filename="../../build/src/ui_main-win.h" line="667"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="497"/>
        <location filename="../../build/src/ui_main-win.h" line="669"/>
        <source>&amp;Paste</source>
        <translation>&amp;Pegar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="500"/>
        <location filename="../../build/src/ui_main-win.h" line="671"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="505"/>
        <location filename="../../build/src/ui_main-win.h" line="673"/>
        <source>Select &amp;All</source>
        <translation>Seleccion&amp;ar Todo</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="508"/>
        <location filename="../../build/src/ui_main-win.h" line="675"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="513"/>
        <location filename="../../build/src/ui_main-win.h" line="677"/>
        <source>Pr&amp;eferences</source>
        <translation>Pr&amp;eferencias</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="521"/>
        <location filename="../../build/src/ui_main-win.h" line="678"/>
        <source>&amp;Ascending</source>
        <translation>&amp;Ascendiente</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="529"/>
        <location filename="../../build/src/ui_main-win.h" line="679"/>
        <source>&amp;Descending</source>
        <translation>&amp;Descendiente</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="537"/>
        <location filename="../../build/src/ui_main-win.h" line="680"/>
        <source>&amp;By File Name</source>
        <translation>&amp;Por Nombre de Archivo</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="545"/>
        <location filename="../../build/src/ui_main-win.h" line="681"/>
        <source>By &amp;Modification Time</source>
        <translation>Por &amp;Fecha de Modificación</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="553"/>
        <location filename="../../build/src/ui_main-win.h" line="682"/>
        <source>By File &amp;Type</source>
        <translation>Por &amp;Tipo de Archivo</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="561"/>
        <location filename="../../build/src/ui_main-win.h" line="683"/>
        <source>By &amp;Owner</source>
        <translation>Por &amp;Propietario</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="569"/>
        <location filename="../../build/src/ui_main-win.h" line="684"/>
        <source>&amp;Folder First</source>
        <translation>&amp;Carpetas Primero</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="640"/>
        <location filename="../../build/src/ui_main-win.h" line="707"/>
        <source>&amp;Move to Trash</source>
        <translation>&amp;Mover a la Papelera</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="643"/>
        <location filename="../../build/src/ui_main-win.h" line="709"/>
        <source>Ctrl+Backspace</source>
        <translation>Ctrl + Retroceso</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <location filename="../../build/src/ui_main-win.h" line="716"/>
        <source>Get &amp;Info</source>
        <translation>Obtener Info</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <location filename="../../build/src/ui_main-win.h" line="718"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <location filename="../../build/src/ui_main-win.h" line="720"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;Distingue mayúsculas y minúsculas</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="680"/>
        <location filename="../../build/src/ui_main-win.h" line="721"/>
        <source>By File &amp;Size</source>
        <translation>Por &amp;Tamaño de Archivo</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <location filename="../../build/src/ui_main-win.h" line="722"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Cerrar Ventana</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="720"/>
        <location filename="../../build/src/ui_main-win.h" line="733"/>
        <source>&amp;Folder</source>
        <translation>&amp;Carpeta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="732"/>
        <location filename="../../build/src/ui_main-win.h" line="737"/>
        <source>&amp;Blank File</source>
        <translation>&amp;Archivo Vacío</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="743"/>
        <location filename="../../build/src/ui_main-win.h" line="743"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="760"/>
        <location filename="../../build/src/ui_main-win.h" line="746"/>
        <source>&amp;Go To Folder</source>
        <translation>&amp;Ir a la Carpeta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="763"/>
        <location filename="../../build/src/ui_main-win.h" line="748"/>
        <source>Go To Folder</source>
        <translation>Ir a la Carpeta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="766"/>
        <location filename="../../build/src/ui_main-win.h" line="751"/>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Mayúsc+G</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="771"/>
        <location filename="../../build/src/ui_main-win.h" line="753"/>
        <source>&amp;Downloads</source>
        <translation>&amp;Descargas</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="774"/>
        <location filename="../../build/src/ui_main-win.h" line="755"/>
        <source>Downloads</source>
        <translation>Descargas</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="777"/>
        <location filename="../../build/src/ui_main-win.h" line="758"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Mayúsc+L</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="785"/>
        <location filename="../../build/src/ui_main-win.h" line="760"/>
        <source>&amp;Utilities</source>
        <translation>&amp;Utilidades</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="788"/>
        <location filename="../../build/src/ui_main-win.h" line="762"/>
        <source>Utilities</source>
        <translation>Utilidades</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="791"/>
        <location filename="../../build/src/ui_main-win.h" line="765"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Mayúsc+U</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="799"/>
        <location filename="../../build/src/ui_main-win.h" line="767"/>
        <source>&amp;Documents</source>
        <translation>&amp;Documentos</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="802"/>
        <location filename="../../build/src/ui_main-win.h" line="769"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="805"/>
        <location filename="../../build/src/ui_main-win.h" line="772"/>
        <source>Ctrl+Shift+O</source>
        <translation>Ctrl+Mayúsc+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="813"/>
        <location filename="../../build/src/ui_main-win.h" line="774"/>
        <source>Open</source>
        <translation>Abrir</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="816"/>
        <location filename="../../build/src/ui_main-win.h" line="776"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../../build/src/ui_main-win.h" line="778"/>
        <source>&amp;Duplicate</source>
        <translation>&amp;Duplicar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="824"/>
        <location filename="../../build/src/ui_main-win.h" line="780"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="829"/>
        <location filename="../../build/src/ui_main-win.h" line="782"/>
        <source>Empty Trash</source>
        <translation>Vaciar Papelera</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <location filename="../../build/src/ui_main-win.h" line="784"/>
        <source>Ctrl+Alt+Backspace</source>
        <translation>Ctrl+Alt+Retroceso</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <location filename="../../build/src/ui_main-win.h" line="786"/>
        <source>Show Contents</source>
        <translation>Mostrar Contenidos</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="840"/>
        <location filename="../../build/src/ui_main-win.h" line="788"/>
        <source>Ctrl+Alt+O</source>
        <translation>Ctrl+Alt+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="849"/>
        <location filename="../main-win.ui" line="852"/>
        <location filename="../../build/src/ui_main-win.h" line="790"/>
        <location filename="../../build/src/ui_main-win.h" line="792"/>
        <source>Go Up and Close Current</source>
        <translation>Subir y cerrar actual</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="855"/>
        <location filename="../../build/src/ui_main-win.h" line="795"/>
        <source>Ctrl+Shift+Up</source>
        <translation>Ctrl+Mayúsc+Up</translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation type="vanished">Ascendente</translation>
    </message>
    <message>
        <source>Descending</source>
        <translation type="vanished">Descendente</translation>
    </message>
    <message>
        <source>By File Name</source>
        <translation type="vanished">Por Nombre de Archivo</translation>
    </message>
    <message>
        <source>By Modification Time</source>
        <translation type="vanished">Por Fecha de Modificación</translation>
    </message>
    <message>
        <source>By File Type</source>
        <translation type="vanished">Por Tipo de Archivo</translation>
    </message>
    <message>
        <source>By Owner</source>
        <translation type="vanished">Por Dueño</translation>
    </message>
    <message>
        <source>Folder First</source>
        <translation type="vanished">Carpetas Primero</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="578"/>
        <location filename="../../build/src/ui_main-win.h" line="685"/>
        <source>New &amp;Tab</source>
        <translation>Nueva &amp;Pestaña</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="581"/>
        <location filename="../../build/src/ui_main-win.h" line="687"/>
        <source>New Tab</source>
        <translation>Nueva Pestaña</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="584"/>
        <location filename="../main-win.ui" line="701"/>
        <location filename="../../build/src/ui_main-win.h" line="690"/>
        <location filename="../../build/src/ui_main-win.h" line="729"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="596"/>
        <location filename="../../build/src/ui_main-win.h" line="692"/>
        <source>Go &amp;Back</source>
        <translation>Re&amp;troceder</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="599"/>
        <location filename="../../build/src/ui_main-win.h" line="694"/>
        <source>Go Back</source>
        <translation>Retroceder</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="602"/>
        <location filename="../../build/src/ui_main-win.h" line="697"/>
        <source>Alt+Left</source>
        <translation>Alt+Izquierda</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="611"/>
        <location filename="../../build/src/ui_main-win.h" line="699"/>
        <source>Go &amp;Forward</source>
        <translation>A&amp;vanzar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="614"/>
        <location filename="../../build/src/ui_main-win.h" line="701"/>
        <source>Go Forward</source>
        <translation>Avanzar</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="617"/>
        <location filename="../../build/src/ui_main-win.h" line="704"/>
        <source>Alt+Right</source>
        <translation>Alt+Derecha</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="631"/>
        <location filename="../../build/src/ui_main-win.h" line="706"/>
        <source>&amp;Invert Selection</source>
        <translation>&amp;Invertir Selección</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Eliminar</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Supr</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <location filename="../../build/src/ui_main-win.h" line="711"/>
        <source>&amp;Rename</source>
        <translation>Cambia&amp;r Nombre</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="651"/>
        <location filename="../../build/src/ui_main-win.h" line="713"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <location filename="../../build/src/ui_main-win.h" line="715"/>
        <source>C&amp;lose Tab</source>
        <translation>&amp;Cerrar Pestaña</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="688"/>
        <location filename="../../build/src/ui_main-win.h" line="724"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>File &amp;Properties</source>
        <translation type="vanished">Propiedades de &amp;Archivo</translation>
    </message>
    <message>
        <source>Alt+Return</source>
        <translation type="vanished">Alt+Enter</translation>
    </message>
    <message>
        <source>&amp;Folder Properties</source>
        <translation type="vanished">&amp;Propiedades de &amp;Carpeta</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation type="vanished">Coincidir Mayúsculas</translation>
    </message>
    <message>
        <source>By File Size</source>
        <translation type="vanished">Por Tamaño de Archivo</translation>
    </message>
    <message>
        <source>Close Window</source>
        <translation type="vanished">Cerrar Ventana</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <location filename="../../build/src/ui_main-win.h" line="726"/>
        <source>Edit Bookmarks</source>
        <translation>Editar Marcadores</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <location filename="../../build/src/ui_main-win.h" line="727"/>
        <source>Open &amp;Terminal</source>
        <translation>Abrir &amp;terminal</translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="vanished">F4</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <location filename="../../build/src/ui_main-win.h" line="731"/>
        <source>Open as &amp;Root</source>
        <translation>Abrir como &amp;root</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <location filename="../../build/src/ui_main-win.h" line="732"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>&amp;Editar Marcadores</translation>
    </message>
</context>
<context>
    <name>MountOperationPasswordDialog</name>
    <message>
        <location filename="../mount-operation-password.ui" line="20"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="172"/>
        <source>Mount</source>
        <translation>Montar</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="48"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="174"/>
        <source>Connect &amp;anonymously</source>
        <translation>Conectar &amp;anonimamente</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="58"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="175"/>
        <source>Connect as u&amp;ser:</source>
        <translation>Conectar como u&amp;suario:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="79"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="176"/>
        <source>&amp;Username:</source>
        <translation>&amp;Usuario:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="102"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="177"/>
        <source>&amp;Password:</source>
        <translation>&amp;Contraseña:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="112"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="178"/>
        <source>&amp;Domain:</source>
        <translation>&amp;Dominio:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="127"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="179"/>
        <source>Forget password &amp;immediately</source>
        <translation>Descartar contraseña &amp;inmediatamente</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="137"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="180"/>
        <source>Remember password until you &amp;logout</source>
        <translation>&amp;Recordar contraseña hasta cerrar sesión</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="147"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="181"/>
        <source>Remember &amp;forever</source>
        <translation>Recordar &amp;para siempre</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <location filename="../../build/src/ui_preferences.h" line="641"/>
        <source>Preferences</source>
        <translation>Preferencias</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <location filename="../../build/src/ui_preferences.h" line="650"/>
        <source>User Interface</source>
        <translation>Interfaz de Usuario</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <location filename="../../build/src/ui_preferences.h" line="646"/>
        <source>Behavior</source>
        <translation>Comportamiento</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="451"/>
        <location filename="../../build/src/ui_preferences.h" line="652"/>
        <location filename="../../build/src/ui_preferences.h" line="701"/>
        <source>Thumbnail</source>
        <translation>Miniaturas</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">Volumen</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <location filename="../../build/src/ui_preferences.h" line="654"/>
        <source>Advanced</source>
        <translation>Avanzado</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="92"/>
        <location filename="../../build/src/ui_preferences.h" line="659"/>
        <source>Save metadata to directories (.DirInfo files)</source>
        <translation>Guardar metadatos en los directorios (archivos .DirInfo)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="102"/>
        <location filename="../../build/src/ui_preferences.h" line="660"/>
        <source>Spatial mode (folders open in a new window)</source>
        <translation>Modo espacial (las carpetas se abren en una nueva ventana)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="166"/>
        <location filename="../../build/src/ui_preferences.h" line="666"/>
        <source>Icons</source>
        <translation>Íconos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <location filename="../../build/src/ui_preferences.h" line="668"/>
        <source>Size of big icons:</source>
        <translation>Tamaño de íconos grandes:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="205"/>
        <location filename="../../build/src/ui_preferences.h" line="669"/>
        <source>Size of small icons:</source>
        <translation>Tamaño de íconos pequeños:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="215"/>
        <location filename="../../build/src/ui_preferences.h" line="670"/>
        <source>Size of thumbnails:</source>
        <translation>Tamaño de miniaturas:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="225"/>
        <location filename="../../build/src/ui_preferences.h" line="671"/>
        <source>Size of side pane icons:</source>
        <translation>Tamaño de íconos del panel lateral:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="178"/>
        <location filename="../../build/src/ui_preferences.h" line="667"/>
        <source>Icon theme:</source>
        <translation>Tema de íconos:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="298"/>
        <location filename="../../build/src/ui_preferences.h" line="677"/>
        <source>Window</source>
        <translation>Ventana</translation>
    </message>
    <message>
        <source>Always show the tab bar</source>
        <translation type="vanished">Mostrar siempre la barra de pestañas</translation>
    </message>
    <message>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation type="vanished">Mostrar botones de cerrar en las pestañas</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="307"/>
        <location filename="../../build/src/ui_preferences.h" line="678"/>
        <source>Remember the size of the last closed window</source>
        <translation>Recordar el tamaño de la última ventana cerrada</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="314"/>
        <location filename="../../build/src/ui_preferences.h" line="679"/>
        <source>Default width of new windows:</source>
        <translation>Ancho para nuevas ventanas:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="328"/>
        <location filename="../../build/src/ui_preferences.h" line="680"/>
        <source>Default height of new windows:</source>
        <translation>Altura para nuevas ventanas:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="76"/>
        <location filename="../../build/src/ui_preferences.h" line="657"/>
        <source>Browsing</source>
        <translation>Navegación</translation>
    </message>
    <message>
        <source>Open files with single click</source>
        <translation type="vanished">Abrir archivos con un solo click</translation>
    </message>
    <message>
        <source>Delay of auto-selection in single click mode (0 to disable)</source>
        <translation type="vanished">Demora de selección automática en modo de un click (0 para deshabilitar)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="82"/>
        <location filename="../../build/src/ui_preferences.h" line="658"/>
        <source>Default view mode:</source>
        <translation>Modo de visualización por defecto:</translation>
    </message>
    <message>
        <source> sec</source>
        <translation type="vanished">seg</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <location filename="../../build/src/ui_preferences.h" line="661"/>
        <source>File Operations</source>
        <translation>Operaciones de Archivos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="118"/>
        <location filename="../../build/src/ui_preferences.h" line="662"/>
        <source>Confirm before deleting files</source>
        <translation>Confirmar antes de borrar</translation>
    </message>
    <message>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation type="vanished">Mover archivos a la papelera en lugar de eliminarlos del disco.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="481"/>
        <location filename="../../build/src/ui_preferences.h" line="705"/>
        <source>Show thumbnails of files</source>
        <translation>Mostrar miniaturas de archivos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="474"/>
        <location filename="../../build/src/ui_preferences.h" line="704"/>
        <source>Only show thumbnails for local files</source>
        <translation>Solo mostrar miniaturas para archivos locales</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <location filename="../../build/src/ui_preferences.h" line="648"/>
        <source>Display</source>
        <translation>Monitor</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="125"/>
        <location filename="../../build/src/ui_preferences.h" line="663"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>Borrar archivos en medios extraíbles en lugar de crear una &quot;papelera&quot;</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="132"/>
        <location filename="../../build/src/ui_preferences.h" line="664"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>Confirmar antes de mover archivos a la &quot;papelera&quot;</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <location filename="../../build/src/ui_preferences.h" line="665"/>
        <source>Don&apos;t ask options on launch executable file</source>
        <translation>No pregunte opciones al iniciar el archivo ejecutable</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="238"/>
        <location filename="../../build/src/ui_preferences.h" line="672"/>
        <source>User interface</source>
        <translation>Interfaz de usuario</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="251"/>
        <location filename="../../build/src/ui_preferences.h" line="674"/>
        <source>Treat backup files as hidden</source>
        <translation>Trate los archivos de respaldo como ocultos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="261"/>
        <location filename="../../build/src/ui_preferences.h" line="675"/>
        <source>Always show full file names</source>
        <translation>Siempre mostrar los nombres de archivos completos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <location filename="../../build/src/ui_preferences.h" line="676"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>Mostrar sombreados los íconos de los archivos ocultos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="345"/>
        <location filename="../../build/src/ui_preferences.h" line="681"/>
        <source>Show in places</source>
        <translation>Mostrar en lugares</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <location filename="../../build/src/ui_preferences.h" line="686"/>
        <source>Home</source>
        <translation>Carpeta personal</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="364"/>
        <location filename="../../build/src/ui_preferences.h" line="688"/>
        <source>Desktop</source>
        <translation>Escritorio</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="376"/>
        <location filename="../../build/src/ui_preferences.h" line="690"/>
        <source>Trash can</source>
        <translation>Papelera</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="388"/>
        <location filename="../../build/src/ui_preferences.h" line="692"/>
        <source>Computer</source>
        <translation>Equipo</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="400"/>
        <location filename="../../build/src/ui_preferences.h" line="694"/>
        <source>Applications</source>
        <translation>Aplicaciones</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="408"/>
        <location filename="../../build/src/ui_preferences.h" line="696"/>
        <source>Devices</source>
        <translation>Dispositivos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="416"/>
        <location filename="../../build/src/ui_preferences.h" line="698"/>
        <source>Network</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="457"/>
        <location filename="../../build/src/ui_preferences.h" line="702"/>
        <source>Do not generate thumbnails for image files exceeding this size:</source>
        <translation>No generar miniaturas para archivos de imágenes mayores a:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="464"/>
        <location filename="../../build/src/ui_preferences.h" line="703"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="605"/>
        <location filename="../../build/src/ui_preferences.h" line="715"/>
        <source>Auto Mount</source>
        <translation>Montar Automáticamente</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <location filename="../../build/src/ui_preferences.h" line="716"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>Montar discos removibles automáticamente al inicio</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="618"/>
        <location filename="../../build/src/ui_preferences.h" line="717"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>Montar discos removibles automáticamente cuando se insertan</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="625"/>
        <location filename="../../build/src/ui_preferences.h" line="718"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>Mostrar opciones disponibles para los discos removibles al insertarlos</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="635"/>
        <location filename="../../build/src/ui_preferences.h" line="719"/>
        <source>When removable medium unmounted:</source>
        <translation>Cuando se desconecta un disco removible:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="641"/>
        <location filename="../../build/src/ui_preferences.h" line="720"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>Cerrar la &amp;pestaña que contiene el dispositivo extraíble</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="648"/>
        <location filename="../../build/src/ui_preferences.h" line="721"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>Cam&amp;biar la carpeta en la pestaña hacia la carpeta Inicio</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="524"/>
        <location filename="../../build/src/ui_preferences.h" line="708"/>
        <source>Switch &amp;user command:</source>
        <translation>Cambiar el comando de &amp;usuario:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="558"/>
        <location filename="../../build/src/ui_preferences.h" line="711"/>
        <source>Templates</source>
        <translation>Plantillas</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <location filename="../../build/src/ui_preferences.h" line="712"/>
        <source>Show only user defined templates in menu</source>
        <translation>Mostrar solo plantillas definidas por el usuario en el menú</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="571"/>
        <location filename="../../build/src/ui_preferences.h" line="713"/>
        <source>Show only one template for each MIME type</source>
        <translation>Mostrar solo una plantilla para cada tipo MIME</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <location filename="../../build/src/ui_preferences.h" line="714"/>
        <source>Run default application after creation from template</source>
        <translation>Ejecutar la aplicación predeterminada después de la creación a partir de la plantilla</translation>
    </message>
    <message>
        <source>Close tab containing removable medium</source>
        <translation type="vanished">Cerrar la pestaña que contiene al disco removible</translation>
    </message>
    <message>
        <source>Change folder in the tab to home folder</source>
        <translation type="vanished">Cambiar el directorio en la pestaña a la carpeta personal</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="508"/>
        <location filename="../../build/src/ui_preferences.h" line="706"/>
        <source>Programs</source>
        <translation>Programas</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="517"/>
        <location filename="../../build/src/ui_preferences.h" line="707"/>
        <source>Terminal emulator:</source>
        <translation>Emulador de terminal:</translation>
    </message>
    <message>
        <source>Switch user command:</source>
        <translation type="vanished">Comando para cambiar de usuario:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="537"/>
        <location filename="../../build/src/ui_preferences.h" line="709"/>
        <source>Examples: &quot;xterm -e %s&quot; for terminal or &quot;gksu %s&quot; for switching user.
%s = the command line you want to execute with terminal or su.</source>
        <translation>Ejemplos: &quot;xterm -e %s&quot; para terminal o &quot;gksu %s&quot; para cambiar de usuario.
%s = la línea de comandos a ejecutar en la terminal o como otro usuario.</translation>
    </message>
    <message>
        <source>Archiver integration:</source>
        <translation type="vanished">Integración con programas de archivo:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="244"/>
        <location filename="../../build/src/ui_preferences.h" line="673"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>Usar prefijos decimales SI en lugar de prefijos binarios IEC</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filelauncher.cpp" line="186"/>
        <location filename="../mountoperation.cpp" line="185"/>
        <location filename="../utilities.cpp" line="133"/>
        <location filename="../utilities.cpp" line="209"/>
        <location filename="../utilities.cpp" line="285"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <source>Rename File</source>
        <translation type="vanished">Renombrar Archivo</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="127"/>
        <source>Rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="128"/>
        <source>Please enter a new name:</source>
        <translation>Introduzca un nuevo nombre:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="133"/>
        <source>The startvolume cannot be renamed.</source>
        <translation>No se puede cambiar el nombre del volumen inicial.</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="221"/>
        <source>Create Folder</source>
        <translation>Crear carpeta</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="226"/>
        <source>Please enter a new file name:</source>
        <translation>Introduzca un nuevo nombre de archivo:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="227"/>
        <source>New text file</source>
        <translation>Nuevo archivo de texto</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="231"/>
        <source>Please enter a new folder name:</source>
        <translation>Ingrese un nuevo nombre de directorio:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="232"/>
        <source>New folder</source>
        <translation>Nuevo directorio</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="237"/>
        <source>Enter a name for the new %1:</source>
        <translation>Ingresar un nombre para el nuevo %1:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="222"/>
        <source>Create File</source>
        <translation>Crear Archivo</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <location filename="../rename-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="156"/>
        <source>Confirm to replace files</source>
        <translation>Confirmación de sustitución</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="35"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;There is already a file with the same name in this location.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Do you want to replace the existing file?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Ya existe un archivo con el mismo nombre en este lugar.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;¿Quiere reemplazar el archivo existente?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="56"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="158"/>
        <source>dest</source>
        <translation>destino</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="63"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="159"/>
        <source>with the following file?</source>
        <translation>con el siguiente archivo?</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="76"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="160"/>
        <source>src file info</source>
        <translation>información del archivo de origen</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="89"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="161"/>
        <source>dest file info</source>
        <translation>información del archivo de destino</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="102"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="162"/>
        <source>src</source>
        <translation>origen</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="122"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="163"/>
        <source>&amp;File name:</source>
        <translation>Nombre de &amp;archivo:</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="137"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="164"/>
        <source>Apply this option to all existing files</source>
        <translation>Aplicar esta opción a todos los archivos</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../filesearch.ui" line="14"/>
        <location filename="../../build/src/ui_filesearch.h" line="398"/>
        <source>Search Files</source>
        <translation>Buscar Archivos</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="29"/>
        <location filename="../../build/src/ui_filesearch.h" line="408"/>
        <source>Name/Location</source>
        <translation>Nombre/Posición</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="35"/>
        <location filename="../../build/src/ui_filesearch.h" line="399"/>
        <source>File Name Patterns:</source>
        <translation>Patrones de nombre de archivo:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="41"/>
        <location filename="../../build/src/ui_filesearch.h" line="400"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="48"/>
        <location filename="../../build/src/ui_filesearch.h" line="401"/>
        <source>Case insensitive</source>
        <translation>No distingue entre mayúsculas y minúsculas</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="55"/>
        <location filename="../../build/src/ui_filesearch.h" line="402"/>
        <source>Use regular expression</source>
        <translation>Usar expresión regular</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="65"/>
        <location filename="../../build/src/ui_filesearch.h" line="403"/>
        <source>Places to Search:</source>
        <translation>Lugares para Buscar:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="78"/>
        <location filename="../../build/src/ui_filesearch.h" line="404"/>
        <source>&amp;Add</source>
        <translation>&amp;Añadir</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="90"/>
        <location filename="../../build/src/ui_filesearch.h" line="405"/>
        <source>&amp;Remove</source>
        <translation>&amp;Eliminar</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="119"/>
        <location filename="../../build/src/ui_filesearch.h" line="406"/>
        <source>Search in sub directories</source>
        <translation>Buscar en subdirectorios</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="126"/>
        <location filename="../../build/src/ui_filesearch.h" line="407"/>
        <source>Search for hidden files</source>
        <translation>Buscar archivos ocultos</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="137"/>
        <location filename="../../build/src/ui_filesearch.h" line="416"/>
        <source>File Type</source>
        <translation>Tipo de Archivo</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="143"/>
        <location filename="../../build/src/ui_filesearch.h" line="409"/>
        <source>Only search for files of following types:</source>
        <translation>Solo buscar archivos de los siguientes tipos:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="149"/>
        <location filename="../../build/src/ui_filesearch.h" line="410"/>
        <source>Text files</source>
        <translation>Archivos de texto</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="156"/>
        <location filename="../../build/src/ui_filesearch.h" line="411"/>
        <source>Image files</source>
        <translation>Imágenes</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="163"/>
        <location filename="../../build/src/ui_filesearch.h" line="412"/>
        <source>Audio files</source>
        <translation>Audios</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="170"/>
        <location filename="../../build/src/ui_filesearch.h" line="413"/>
        <source>Video files</source>
        <translation>Videos</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="177"/>
        <location filename="../../build/src/ui_filesearch.h" line="414"/>
        <source>Documents</source>
        <translation>Documentos</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="184"/>
        <location filename="../../build/src/ui_filesearch.h" line="415"/>
        <source>Folders</source>
        <translation>Carpetas</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="208"/>
        <location filename="../../build/src/ui_filesearch.h" line="420"/>
        <source>Content</source>
        <translation>Contenido</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="214"/>
        <location filename="../../build/src/ui_filesearch.h" line="417"/>
        <source>File contains:</source>
        <translation>El Archivo contiene:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="223"/>
        <location filename="../../build/src/ui_filesearch.h" line="418"/>
        <source>Case insensiti&amp;ve</source>
        <translation>No distingue entre mayús&amp;culas y minús&amp;culas</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="230"/>
        <location filename="../../build/src/ui_filesearch.h" line="419"/>
        <source>&amp;Use regular expression</source>
        <translation>&amp;Usar expresión regular</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="254"/>
        <location filename="../../build/src/ui_filesearch.h" line="437"/>
        <source>Properties</source>
        <translation>Propiedades</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="260"/>
        <location filename="../../build/src/ui_filesearch.h" line="421"/>
        <source>File Size:</source>
        <translation>Tamaño del Archivo:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="266"/>
        <location filename="../../build/src/ui_filesearch.h" line="422"/>
        <source>Larger than:</source>
        <translation>Mayor que:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="282"/>
        <location filename="../filesearch.ui" line="323"/>
        <location filename="../../build/src/ui_filesearch.h" line="423"/>
        <location filename="../../build/src/ui_filesearch.h" line="429"/>
        <source>Bytes</source>
        <translation>Bytes</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="287"/>
        <location filename="../filesearch.ui" line="328"/>
        <location filename="../../build/src/ui_filesearch.h" line="424"/>
        <location filename="../../build/src/ui_filesearch.h" line="430"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="292"/>
        <location filename="../filesearch.ui" line="333"/>
        <location filename="../../build/src/ui_filesearch.h" line="425"/>
        <location filename="../../build/src/ui_filesearch.h" line="431"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="297"/>
        <location filename="../filesearch.ui" line="338"/>
        <location filename="../../build/src/ui_filesearch.h" line="426"/>
        <location filename="../../build/src/ui_filesearch.h" line="432"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="307"/>
        <location filename="../../build/src/ui_filesearch.h" line="428"/>
        <source>Smaller than:</source>
        <translation>Menor a:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="351"/>
        <location filename="../../build/src/ui_filesearch.h" line="434"/>
        <source>Last Modified Time:</source>
        <translation>Última Fecha de Modificación:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="357"/>
        <location filename="../../build/src/ui_filesearch.h" line="435"/>
        <source>Earlier than:</source>
        <translation>Antes de:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="364"/>
        <location filename="../../build/src/ui_filesearch.h" line="436"/>
        <source>Later than:</source>
        <translation>Después de:</translation>
    </message>
</context>
</TS>
