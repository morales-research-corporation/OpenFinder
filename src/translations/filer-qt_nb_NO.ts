<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nb_NO">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <location filename="../../build/src/ui_about.h" line="140"/>
        <source>About</source>
        <translation>Om</translation>
    </message>
    <message>
        <location filename="../about.ui" line="37"/>
        <location filename="../../build/src/ui_about.h" line="142"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="60"/>
        <location filename="../../build/src/ui_about.h" line="144"/>
        <source>The Desktop Experience</source>
        <translation>Skrivebordsopplevelsen</translation>
    </message>
    <message>
        <location filename="../about.ui" line="70"/>
        <location filename="../../build/src/ui_about.h" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="99"/>
        <location filename="../../build/src/ui_about.h" line="146"/>
        <source>Programming:
* Simon Peter (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Application icon:
* Raphael Lopes (https://raphaellopes.me/)</source>
        <translation>Programmering:
* Simon Peter (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Programikon:
* Raphael Lopes (https://raphaellopes.me/)</translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../../build/src/ui_about.h" line="154"/>
        <source>Filer

Copyright (C) 2020-21 Simon Peter
Copyright (C) 2021 Chris Moore

Originally based on PCMan File Manager
Portions Copyright (C) 2009-14 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Application icon

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

Copyright (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

Used with permission of the creator https://raphaellopes.me/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lightweight file manager</source>
        <translation type="vanished">Lett filbehandler</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://lxqt.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://lxqt-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation type="vanished">Programmering:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="90"/>
        <location filename="../../build/src/ui_about.h" line="153"/>
        <source>Authors</source>
        <translation>Utviklere</translation>
    </message>
    <message>
        <source>PCMan File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation type="vanished">PCMan-filbehandler

Opphavsrett © 2009–2014 洪任諭 (Hong Jen Yee)

Dette programmet er fri programvare; du kan redistribuere det og/eller
endre det i henhold til vilkårene i GNU General Public-lisensen
som utgitt av Free Software-stiftelsen; enten versjon 2 av lisensen
eller (etter eget valg) enhver senere versjon.

Dette programmet distribueres i håp om at det skal være nyttig,
men UTEN GARANTI AV NOEN ART; selv uten implisitt garanti om
SALGBARHET eller FORMÅLSTJENLIGHET. Sjekk
GNU General Public-lisensen for flere detaljer.

Du skal ha mottatt en kopi av GNU General Public-lisensen
sammen med programmet; hvis ikke, skriv til Free Software-stiftelsen
Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="../about.ui" line="116"/>
        <location filename="../../build/src/ui_about.h" line="184"/>
        <source>License</source>
        <translation>Lisens</translation>
    </message>
</context>
<context>
    <name>AppChooserDialog</name>
    <message>
        <location filename="../app-chooser-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="150"/>
        <source>Choose an Application</source>
        <translation>Velg et program</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="36"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="151"/>
        <source>Installed Applications</source>
        <translation>Installerte programmer</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="46"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="163"/>
        <source>Custom Command</source>
        <translation>Egendefinert kommando</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="52"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="152"/>
        <source>Command line to execute:</source>
        <translation>Kommando å kjøre:</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="62"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="153"/>
        <source>Application name:</source>
        <translation>Programnavn:</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="72"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="154"/>
        <source>&lt;b&gt;These special codes can be used in the command line:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Represents a single file name&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Represents multiple file names&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Represents a single URI of the file&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Represents multiple URIs&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation>&lt;b&gt;Disse spesialkodene kan brukes på kommandolinjen:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Representerer ett filnavn&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Representerer flere filnavn&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Representerer én URI for filen&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Representerer flere URI-er&lt;/li&gt;
&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="91"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="161"/>
        <source>Keep terminal window open after command execution</source>
        <translation>Hold terminalvindu åpent etter kjøring av kommando</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="98"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="162"/>
        <source>Execute in terminal emulator</source>
        <translation>Kjør i terminalemulator</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="109"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="164"/>
        <source>Set selected application as default action of this file type</source>
        <translation>Sett valgt program som forvalgt handling for denne filtypen</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <location filename="../../build/src/ui_autorun.h" line="108"/>
        <source>Removable medium is inserted</source>
        <translation>Flyttbart medium satt inn</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <location filename="../../build/src/ui_autorun.h" line="110"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;Flyttbart medium satt inn&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <location filename="../../build/src/ui_autorun.h" line="111"/>
        <source>Type of medium:</source>
        <translation>Type medium:</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <location filename="../../build/src/ui_autorun.h" line="112"/>
        <source>Detecting...</source>
        <translation>Oppdager …</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <location filename="../../build/src/ui_autorun.h" line="113"/>
        <source>Please select the action you want to perform:</source>
        <translation>Velg handlingen du ønske rå utføre:</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="72"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="73"/>
        <source>Desktop</source>
        <translation>Skrivebord</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="74"/>
        <source>Desktop folder:</source>
        <translation>Skrivebordsmappe:</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="76"/>
        <source>Image file</source>
        <translation type="unfinished">Bildefil</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="81"/>
        <source>Folder path</source>
        <translation>Mappesti</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="82"/>
        <source>&amp;Browse</source>
        <translation>&amp;Utforsk</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="205"/>
        <source>Desktop Preferences</source>
        <translation>Skrivebordsinnstillinger</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="20"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="206"/>
        <source>Background</source>
        <translation>Bakgrunn</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="207"/>
        <source>Wallpaper mode:</source>
        <translation>Bakgrunnsbildemodus:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="55"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="208"/>
        <source>Wallpaper image file:</source>
        <translation>Bakgrunnsbildefil:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="75"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="210"/>
        <source>Background color:</source>
        <translation>Bakgrunnsfarge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="147"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="222"/>
        <source>Text color:</source>
        <translation>Tekstfarge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="160"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="223"/>
        <source>Shadow color:</source>
        <translation>Skyggefarge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="173"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="224"/>
        <source>Font:</source>
        <translation>Skrift:</translation>
    </message>
    <message>
        <source>Select background color:</source>
        <translation type="vanished">Velg bakgrunnsbildefarge:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="84"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="212"/>
        <source>Image file</source>
        <translation>Bildefil</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="90"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="217"/>
        <source>Image file path</source>
        <translation>Bildefil-sti</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="97"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="218"/>
        <source>&amp;Browse</source>
        <translation>&amp;Utforsk</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="109"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="219"/>
        <source>Label Text</source>
        <translation>Etikett-tekst</translation>
    </message>
    <message>
        <source>Select  text color:</source>
        <translation type="vanished">Velg tekstfarge:</translation>
    </message>
    <message>
        <source>Select shadow color:</source>
        <translation type="vanished">Velg skyggefarge:</translation>
    </message>
    <message>
        <source>Select font:</source>
        <translation type="vanished">Velg skrift:</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Generelt</translation>
    </message>
    <message>
        <source>Window Manager</source>
        <translation type="vanished">Vindusbehandler</translation>
    </message>
    <message>
        <source>Show menus provided by window managers when desktop is clicked</source>
        <translation type="vanished">Vis menyer angitt av vindusbehandlere når skrivebordet klikkes</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Avansert</translation>
    </message>
</context>
<context>
    <name>EditBookmarksDialog</name>
    <message>
        <location filename="../edit-bookmarks.ui" line="14"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="103"/>
        <source>Edit Bookmarks</source>
        <translation type="unfinished">Rediger bokmerker</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="42"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="106"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="47"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="105"/>
        <source>Location</source>
        <translation>Sted</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="67"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="107"/>
        <source>&amp;Add Item</source>
        <translation>&amp;Legg til element</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="77"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="108"/>
        <source>&amp;Remove Item</source>
        <translation>&amp;Fjern element</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="102"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="109"/>
        <source>Use drag and drop to reorder the items</source>
        <translation>Bruk dra-og-slipp for å omorganisere elementene</translation>
    </message>
</context>
<context>
    <name>ExecFileDialog</name>
    <message>
        <location filename="../exec-file.ui" line="14"/>
        <location filename="../../build/src/ui_exec-file.h" line="114"/>
        <source>Execute file</source>
        <translation>Kjør fil</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="39"/>
        <location filename="../../build/src/ui_exec-file.h" line="116"/>
        <source>&amp;Open</source>
        <translation>&amp;Åpne</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="52"/>
        <location filename="../../build/src/ui_exec-file.h" line="117"/>
        <source>E&amp;xecute</source>
        <translation>K&amp;jør</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="62"/>
        <location filename="../../build/src/ui_exec-file.h" line="118"/>
        <source>Execute in &amp;Terminal</source>
        <translation>Kjør i &amp;terminal</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="85"/>
        <location filename="../../build/src/ui_exec-file.h" line="119"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>FileOperationDialog</name>
    <message>
        <location filename="../file-operation-dialog.ui" line="25"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="124"/>
        <source>Destination:</source>
        <translation>Mål:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="48"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="126"/>
        <source>Processing:</source>
        <translation type="unfinished">Behandler:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="61"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="127"/>
        <source>Preparing...</source>
        <translation>Forbereder …</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="68"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="128"/>
        <source>Progress</source>
        <translation>Framdrift</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="88"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="129"/>
        <source>Time remaining:</source>
        <translation>Gjenstående tid:</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../file-props.ui" line="20"/>
        <location filename="../../build/src/ui_file-props.h" line="362"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="60"/>
        <location filename="../../build/src/ui_file-props.h" line="363"/>
        <source>TextLabel</source>
        <translation>TekstEtikett</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="133"/>
        <location filename="../../build/src/ui_file-props.h" line="365"/>
        <source>General</source>
        <translation type="unfinished">Generelt</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="139"/>
        <location filename="../../build/src/ui_file-props.h" line="366"/>
        <source>File type:</source>
        <translation>Filtype:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="162"/>
        <location filename="../../build/src/ui_file-props.h" line="368"/>
        <source>File size:</source>
        <translation>Filstørrelse:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="185"/>
        <location filename="../../build/src/ui_file-props.h" line="370"/>
        <source>On-disk size:</source>
        <translation>Størrelse på disk:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="208"/>
        <location filename="../../build/src/ui_file-props.h" line="372"/>
        <source>Location:</source>
        <translation>Sted:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="234"/>
        <location filename="../../build/src/ui_file-props.h" line="374"/>
        <source>Link target:</source>
        <translation>Lenkemål:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="260"/>
        <location filename="../../build/src/ui_file-props.h" line="376"/>
        <source>Last modified:</source>
        <translation>Sist endret:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="283"/>
        <location filename="../../build/src/ui_file-props.h" line="378"/>
        <source>Last accessed:</source>
        <translation>Sist brukt:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="317"/>
        <location filename="../../build/src/ui_file-props.h" line="380"/>
        <source>Open with</source>
        <translation>Åpne med</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="323"/>
        <location filename="../../build/src/ui_file-props.h" line="381"/>
        <source>Mime type:</source>
        <translation type="unfinished">Media-type:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="346"/>
        <location filename="../../build/src/ui_file-props.h" line="383"/>
        <source>Open With:</source>
        <translation>Åpne med:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="380"/>
        <location filename="../../build/src/ui_file-props.h" line="384"/>
        <source>Access Control</source>
        <translation>Tilgangskontroll</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="423"/>
        <location filename="../../build/src/ui_file-props.h" line="385"/>
        <source>Everyone</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="440"/>
        <location filename="../../build/src/ui_file-props.h" line="386"/>
        <source>Make the file executable</source>
        <translation>Gjør filen kjørbar</translation>
    </message>
</context>
<context>
    <name>Filer::Application</name>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Name of configuration profile</source>
        <translation>Navn på oppsettsprofil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>PROFILE</source>
        <translation>Profil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Run Filer as a daemon</source>
        <translation>Kjør Filer som nisse</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Quit Filer</source>
        <translation>Avslutt Filer</translation>
    </message>
    <message>
        <source>Launch desktop manager</source>
        <translation type="vanished">Kjør skrivebordsbehandler</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Launch desktop manager (deprecated)</source>
        <translation>Start skrivebordsbehandler (foreldet)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>Skru av skrivebordsbehandler hvis den kjører</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>Åpne skrivebordinnstillingsdialogen på siden med angitt navn</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <location filename="../application.cpp" line="220"/>
        <source>NAME</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="207"/>
        <source>Open new window</source>
        <translation>Åpne nytt vindu</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="210"/>
        <source>Open Find Files utility</source>
        <translation type="unfinished">Åpne søkeverktøyet</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation type="unfinished">Sett bakgrunnsbilde fra bildefil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>FILE</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>Set mode of desktop wallpaper. MODE=(color|stretch|fit|center|tile)</source>
        <translation>Sett modus for bakgrunnen. MODE=(color|stretch|fit|center|tile)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>MODE</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="220"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>Åpne innstillingsdialogen på side med angitt navn.</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>Files or directories to open</source>
        <translation>Filer eller mapper å åpne</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[Fil1, fil2, …]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="512"/>
        <location filename="../application.cpp" line="519"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="519"/>
        <source>Terminal emulator is not set.</source>
        <translation>Terminalemulator er ikke satt.</translation>
    </message>
</context>
<context>
    <name>Filer::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="43"/>
        <source>Open in file manager</source>
        <translation>Åpne i filbehandler</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="133"/>
        <source>Removable Disk</source>
        <translation>Flyttbar disk</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="49"/>
        <source>Fill with background color only</source>
        <translation>Fyll kun med bakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="50"/>
        <source>Transparent</source>
        <translation>Gjennomsiktig</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="51"/>
        <source>Stretch to fill the entire screen</source>
        <translation>Strekk for å dekke hele skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="52"/>
        <source>Stretch to fit the screen</source>
        <translation>Strekk for å dekke skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Center on the screen</source>
        <translation>Sentrer på skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>Flislegg for å fylle hele skjermen</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="145"/>
        <source>Image Files</source>
        <translation type="unfinished">Bildefiler</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="439"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>&amp;Fest til nåværende posisjon</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="461"/>
        <source>Desktop Preferences</source>
        <translation>Skrivebordsegenskaper</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="528"/>
        <source>Version: %1</source>
        <translation type="unfinished">Versjon: %1</translation>
    </message>
</context>
<context>
    <name>Filer::GotoFolderDialog</name>
    <message>
        <location filename="../gotofolderwindow.cpp" line="47"/>
        <source>Go</source>
        <translation type="unfinished">Start</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="48"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="59"/>
        <source>Go To Folder</source>
        <translation>Gå til mappe</translation>
    </message>
</context>
<context>
    <name>Filer::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="210"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>Tøm tekst (Ctrl+K)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>Version: %1</source>
        <translation>Versjon: %1</translation>
    </message>
    <message>
        <source>&amp;Move to Trash</source>
        <translation type="vanished">&amp;Flytt til papirkurv</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Slett</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1464"/>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Switch user command is not set.</source>
        <translation>Kommando for bytting av kommando er ikke satt.</translation>
    </message>
</context>
<context>
    <name>Filer::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="192"/>
        <source>Icon View</source>
        <translation>Ikonvisning</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="193"/>
        <source>Compact Icon View</source>
        <translation>Kompakt ikonvisning</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="194"/>
        <source>Thumbnail View</source>
        <translation>Miniatyrbildevisning</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="195"/>
        <source>Detailed List View</source>
        <translation>Detaljert listevisning</translation>
    </message>
</context>
<context>
    <name>Filer::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="246"/>
        <source>Error</source>
        <translation>Feil</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="278"/>
        <source>%n items</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="280"/>
        <source>1 item</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="436"/>
        <source>%1 items selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="440"/>
        <source>%1 item selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Free space: %1 (Total: %2)</source>
        <translation type="vanished">Ledig plass: %1 (Totalt: %2)</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="265"/>
        <source>%1 available</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%n item(s)</source>
        <translation type="obsolete">
            <numerusform>%n element</numerusform>
            <numerusform>%n elementer</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n hidden)</source>
        <translation type="vanished">
            <numerusform> (%n skjult)</numerusform>
            <numerusform> (%n skjulte)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 item(s) selected</source>
        <translation type="obsolete">
            <numerusform>%1 element(er) valgt</numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Filer::View</name>
    <message>
        <source>Open in New T&amp;ab</source>
        <translation type="vanished">Åpne i ny &amp;fane</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="111"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Åpne i nytt &amp;vindu</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="119"/>
        <source>Open in Termina&amp;l</source>
        <translation>Åpne i &amp;terminal</translation>
    </message>
</context>
<context>
    <name>FindFilesDialog</name>
    <message>
        <location filename="../file-search.ui" line="14"/>
        <source>Find Files</source>
        <translation>Finn filer</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="24"/>
        <source>Name/Location</source>
        <translation>Navn/sted</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="30"/>
        <source>File name patterns</source>
        <translation>Filnavnsmønster</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="36"/>
        <source>Pattern:</source>
        <translation>Mønster:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="46"/>
        <location filename="../file-search.ui" line="221"/>
        <source>Case insensitive</source>
        <translation>Ingen forskjell på små og store bokstaver</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="53"/>
        <location filename="../file-search.ui" line="228"/>
        <source>Use regular expression</source>
        <translation>Bruk regulært uttrykk</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="63"/>
        <source>Places to search</source>
        <translation>Steder å søke</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="76"/>
        <source>Add</source>
        <translation>Legg til</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="88"/>
        <source>Remove</source>
        <translation>Fjern</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="117"/>
        <source>Search in sub directories</source>
        <translation>Søk i undermapper</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="124"/>
        <source>Search hidden files</source>
        <translation>Søk i skjulte filer</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="135"/>
        <location filename="../file-search.ui" line="141"/>
        <source>File Type</source>
        <translation>Filtype</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="147"/>
        <source>Only search for files of following types:</source>
        <translation>Kun søk etter filer av følgende typer:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="154"/>
        <source>Text files</source>
        <translation>Tekstfiler</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="161"/>
        <source>Image files</source>
        <translation>Bildefiler</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="168"/>
        <source>Audio files</source>
        <translation>Lydfiler</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="175"/>
        <source>Video files</source>
        <translation>Videofiler</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="182"/>
        <source>Documents</source>
        <translation>Dokumenter</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="206"/>
        <source>Content</source>
        <translation>Innhold</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="212"/>
        <source>File contains</source>
        <translation type="unfinished">Filinnhold</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="252"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="258"/>
        <source>File Size</source>
        <translation>Filstørrelse</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="264"/>
        <source>Bigger than:</source>
        <translation>Større enn:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="289"/>
        <source>Smaller than:</source>
        <translation>Mindre enn:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="317"/>
        <source>Last Modified Time</source>
        <translation type="unfinished">Siste endringstid</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="323"/>
        <source>Earlier than:</source>
        <translation>Tidligere enn:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="340"/>
        <source>Later than:</source>
        <translation>Senere enn:</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserComboBox</name>
    <message>
        <location filename="../appchoosercombobox.cpp" line="79"/>
        <source>Customize</source>
        <translation>Tilpass</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserDialog</name>
    <message>
        <location filename="../appchooserdialog.cpp" line="262"/>
        <source>Select an application to open &quot;%1&quot; files</source>
        <translation>Velg et program å åpne «%1»-filer med</translation>
    </message>
</context>
<context>
    <name>Fm::CreateNewMenu</name>
    <message>
        <location filename="../createnewmenu.cpp" line="29"/>
        <source>Folder</source>
        <translation>Mappe</translation>
    </message>
    <message>
        <location filename="../createnewmenu.cpp" line="33"/>
        <source>Blank File</source>
        <translation>Tom fil</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeModel</name>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="77"/>
        <source>Loading...</source>
        <translation>Laster inn …</translation>
    </message>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="208"/>
        <source>&lt;No sub folders&gt;</source>
        <translation>&lt;Ingen undermapper&gt;</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeView</name>
    <message>
        <location filename="../dirtreeview.cpp" line="220"/>
        <source>Open in New Win&amp;dow</source>
        <translation type="unfinished">Åpne i nytt &amp;vindu</translation>
    </message>
    <message>
        <location filename="../dirtreeview.cpp" line="226"/>
        <source>Open in Termina&amp;l</source>
        <translation type="unfinished">Åpne i &amp;terminal</translation>
    </message>
</context>
<context>
    <name>Fm::DndActionMenu</name>
    <message>
        <location filename="../dndactionmenu.cpp" line="26"/>
        <source>Copy here</source>
        <translation type="unfinished">Kopier hit</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="27"/>
        <source>Move here</source>
        <translation>Flytt hit</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="28"/>
        <source>Create symlink here</source>
        <translation type="unfinished">Opprett symbolsk lenke hit</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="30"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>Fm::EditBookmarksDialog</name>
    <message>
        <location filename="../editbookmarksdialog.cpp" line="96"/>
        <source>New bookmark</source>
        <translation>Nytt bokmerke</translation>
    </message>
</context>
<context>
    <name>Fm::ExecFileDialog</name>
    <message>
        <location filename="../execfiledialog.cpp" line="40"/>
        <source>This text file &apos;%1&apos; seems to be an executable script.
What do you want to do with it?</source>
        <translation>Tekstfilen «%1» ser ut til å være et kjørbart skript.
Hva vil du gjøre med den?</translation>
    </message>
    <message>
        <location filename="../execfiledialog.cpp" line="45"/>
        <source>This file &apos;%1&apos; is executable. Do you want to execute it?</source>
        <translation>Filen «%1» er kjørbar. Vil du starte den?</translation>
    </message>
</context>
<context>
    <name>Fm::FileMenu</name>
    <message>
        <location filename="../filemenu.cpp" line="93"/>
        <source>Open</source>
        <translation>Åpne</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="99"/>
        <source>Show Contents</source>
        <translation type="unfinished">Vis innhold</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="128"/>
        <source>&amp;Restore</source>
        <translation>&amp;Gjenopprett</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="136"/>
        <source>Open With...</source>
        <translation>Åpne med …</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="164"/>
        <source>Other Applications</source>
        <translation>Andre programmer</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="170"/>
        <source>Create &amp;New</source>
        <translation>Opprett &amp;ny</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="177"/>
        <source>Cut</source>
        <translation>Klipp ut</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="181"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="185"/>
        <source>Paste</source>
        <translation>Lim inn</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="190"/>
        <source>&amp;Move to Trash</source>
        <translation type="unfinished">&amp;Flytt til papirkurv</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="194"/>
        <source>Rename</source>
        <translation>Gi nytt navn</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="242"/>
        <source>Get Info</source>
        <translation>Hent info</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="247"/>
        <source>&amp;Empty Trash</source>
        <translation>&amp;Tøm papirkurv</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="349"/>
        <source>Output</source>
        <translation type="unfinished">Utdata</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperation</name>
    <message>
        <location filename="../fileoperation.cpp" line="221"/>
        <source>Error</source>
        <translation type="unfinished">Feil</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="222"/>
        <source>Some files cannot be moved to trash can because the underlying file systems don&apos;t support this operation.
Do you want to delete them instead?</source>
        <translation>Noen filer kan ikke flyttes til papirkurven fordi de underliggende filsystemene ikke støtter denne operasjonen.
Vil du slette dem istedenfor?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="263"/>
        <location filename="../fileoperation.cpp" line="279"/>
        <source>Confirm</source>
        <translation>Bekreft</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="264"/>
        <source>Do you want to delete the selected files?</source>
        <translation>Slett valgte filer?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="280"/>
        <source>Do you want to move the selected files to trash can?</source>
        <translation>Vil du flytte valgte filer til papirkurven?</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperationDialog</name>
    <message>
        <location filename="../fileoperationdialog.cpp" line="41"/>
        <source>Move files</source>
        <translation>Flytt filer</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="42"/>
        <source>Moving the following files to destination folder:</source>
        <translation type="unfinished">Flytt følgende filer til målmappen:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="45"/>
        <source>Copy Files</source>
        <translation>Kopier filer</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="46"/>
        <source>Copying the following files to destination folder:</source>
        <translation type="unfinished">Kopier følgende filer til målmappen:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="49"/>
        <source>Trash Files</source>
        <translation type="unfinished">Filer i papirkurven</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="50"/>
        <source>Moving the following files to trash can:</source>
        <translation type="unfinished">Flytt følgende filer til papirkurven:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="53"/>
        <source>Delete Files</source>
        <translation>Slett filer</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="54"/>
        <source>Deleting the following files</source>
        <translation>Slett følgende filer</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="59"/>
        <source>Create Symlinks</source>
        <translation>Opprett symbolske lenker</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="60"/>
        <source>Creating symlinks for the following files:</source>
        <translation type="unfinished">Opprett symbolske lenker for følgende filer:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="63"/>
        <source>Change Attributes</source>
        <translation>Endre rettigheter</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="64"/>
        <source>Changing attributes of the following files:</source>
        <translation>Endre rettigheter for følgende filer:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="69"/>
        <source>Restore Trashed Files</source>
        <translation>Gjenopprett filer fra papirkurven</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="70"/>
        <source>Restoring the following files from trash can:</source>
        <translation>Gjenopprett følgende filer fra papirkurven:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="139"/>
        <source>Error</source>
        <translation type="unfinished">Feil</translation>
    </message>
</context>
<context>
    <name>Fm::FilePropsDialog</name>
    <message>
        <location filename="../filepropsdialog.cpp" line="148"/>
        <source>View folder content</source>
        <translation>Vis mappeinnhold</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="149"/>
        <source>View and modify folder content</source>
        <translation>Vis og endre mappeinnhold</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="153"/>
        <source>Read</source>
        <translation>Les</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="154"/>
        <source>Read and write</source>
        <translation>Les og skriv</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="156"/>
        <source>Forbidden</source>
        <translation>Forbudt</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="274"/>
        <source>Files of different types</source>
        <translation>Filer av forskjellige typer</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="304"/>
        <source>Multiple Files</source>
        <translation>Flere filer</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="432"/>
        <source>Apply changes</source>
        <translation>Bruk endringer</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="433"/>
        <source>Do you want to recursively apply these changes to all files and sub-folders?</source>
        <translation>Vil du bruke disse valgene rekursivt (for alle underliggende filer og undermapper)?</translation>
    </message>
</context>
<context>
    <name>Fm::FileSearchDialog</name>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>Error</source>
        <translation type="unfinished">Feil</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>You should add at least add one directory to search.</source>
        <translation>Du må minst legge til én mappe å søke i.</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="127"/>
        <source>Select a folder</source>
        <translation>Velg en mappe</translation>
    </message>
</context>
<context>
    <name>Fm::FolderMenu</name>
    <message>
        <location filename="../foldermenu.cpp" line="37"/>
        <source>Create &amp;New</source>
        <translation>Opprett &amp;ny</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="44"/>
        <source>&amp;Paste</source>
        <translation type="unfinished">&amp;Lim inn</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="50"/>
        <source>Select &amp;All</source>
        <translation type="unfinished">Velg &amp;alt</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="54"/>
        <source>Invert Selection</source>
        <translation>Inverter utvalg</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="60"/>
        <source>Sorting</source>
        <translation>Sortering</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="65"/>
        <source>Show Hidden</source>
        <translation>Vis skjulte</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="73"/>
        <source>Get Info</source>
        <translation>Hent info</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="99"/>
        <source>By File Name</source>
        <translation>Etter filnavn</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="100"/>
        <source>By Modification Time</source>
        <translation>Etter endringstid</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="101"/>
        <source>By File Size</source>
        <translation>Etter filstørrelse</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="102"/>
        <source>By File Type</source>
        <translation>Etter filtype</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="103"/>
        <source>By File Owner</source>
        <translation>Etter fileier</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="115"/>
        <source>Ascending</source>
        <translation>Stigende</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="120"/>
        <source>Descending</source>
        <translation>Synkende</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="135"/>
        <source>Folder First</source>
        <translation type="unfinished">Mappe først</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="144"/>
        <source>Case Sensitive</source>
        <translation>Forskjell på små og store bokstaver</translation>
    </message>
</context>
<context>
    <name>Fm::FolderModel</name>
    <message>
        <location filename="../foldermodel.cpp" line="313"/>
        <source>Name</source>
        <translation>Navn</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="316"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="319"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="322"/>
        <source>Modified</source>
        <translation>Endret</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="325"/>
        <source>Owner</source>
        <translation>Eier</translation>
    </message>
</context>
<context>
    <name>Fm::FontButton</name>
    <message>
        <location filename="../fontbutton.cpp" line="46"/>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../fontbutton.cpp" line="50"/>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
</context>
<context>
    <name>Fm::MountOperationPasswordDialog</name>
    <message>
        <location filename="../mountoperationpassworddialog.cpp" line="40"/>
        <source>&amp;Connect</source>
        <translation>&amp;Koble til</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesModel</name>
    <message>
        <location filename="../placesmodel.cpp" line="40"/>
        <source>Devices</source>
        <translation type="unfinished">Enheter</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="82"/>
        <source>Places</source>
        <translation>Steder</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="90"/>
        <source>Desktop</source>
        <translation type="unfinished">Skrivebord</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="99"/>
        <source>Computer</source>
        <translation type="unfinished">Datamaskin</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="126"/>
        <source>Network</source>
        <translation type="unfinished">Nettverker</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="137"/>
        <source>Bookmarks</source>
        <translation>Bokmerker</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="212"/>
        <source>Trash</source>
        <translation>Papirkurv</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesView</name>
    <message>
        <location filename="../placesview.cpp" line="351"/>
        <source>Open in New Window</source>
        <translation>Åpne i nytt vindu</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="362"/>
        <source>Empty Trash</source>
        <translation>Tøm papirkurv</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="371"/>
        <source>Move Bookmark Up</source>
        <translation>Flytt bokmerke oppover</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="376"/>
        <source>Move Bookmark Down</source>
        <translation>Flytt bokmerke nedover</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="380"/>
        <source>Rename Bookmark</source>
        <translation>Gi bokmerke nytt navn</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="383"/>
        <source>Remove Bookmark</source>
        <translation>Fjern bokmerke</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="392"/>
        <location filename="../placesview.cpp" line="409"/>
        <source>Unmount</source>
        <translation>Avmonter</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="396"/>
        <source>Mount</source>
        <translation>Monter</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="402"/>
        <source>Eject</source>
        <translation>Løs ut</translation>
    </message>
</context>
<context>
    <name>Fm::RenameDialog</name>
    <message>
        <location filename="../renamedialog.cpp" line="50"/>
        <location filename="../renamedialog.cpp" line="69"/>
        <source>Type: %1
Size: %2
Modified: %3</source>
        <translation>Type: %1
Størrelse: %2
Endret: %3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="56"/>
        <source>Type: %1
Modified: %2</source>
        <translation>Type: %1
Endret: %2</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="75"/>
        <source>Type: %1
Modified: %3</source>
        <translation>Type: %1
Endret: %3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="89"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Overskriv</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="91"/>
        <source>&amp;Rename</source>
        <translation type="unfinished">&amp;Gi nytt navn</translation>
    </message>
</context>
<context>
    <name>Fm::SidePane</name>
    <message>
        <location filename="../sidepane.cpp" line="49"/>
        <location filename="../sidepane.cpp" line="133"/>
        <source>Places</source>
        <translation>Steder</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="50"/>
        <location filename="../sidepane.cpp" line="135"/>
        <source>Directory Tree</source>
        <translation>Mappetre</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="143"/>
        <source>Shows list of common places, devices, and bookmarks in sidebar</source>
        <translation>Viser en liste over vanlige steder, enheter og bokmerker i sidefeltet</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="145"/>
        <source>Shows tree of directories in sidebar</source>
        <translation>Viser et tre over mapper i sidefeltet</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <location filename="../../build/src/ui_main-win.h" line="602"/>
        <source>File Manager</source>
        <translation>Filbehandler</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="271"/>
        <location filename="../../build/src/ui_main-win.h" line="603"/>
        <source>Go &amp;Up</source>
        <translation>Gå &amp;oppover</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="274"/>
        <location filename="../../build/src/ui_main-win.h" line="605"/>
        <source>Go Up</source>
        <translation>Gå oppover</translation>
    </message>
    <message>
        <source>Alt+Up</source>
        <translation type="vanished">Alt+↑</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <location filename="../../build/src/ui_main-win.h" line="610"/>
        <source>&amp;Home</source>
        <translation></translation>
    </message>
    <message>
        <source>Alt+Home</source>
        <translation type="vanished">Alt+Home</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <location filename="../../build/src/ui_main-win.h" line="614"/>
        <source>&amp;Reload</source>
        <translation>&amp;Last inn igjen</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="vanished">F5</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="313"/>
        <location filename="../../build/src/ui_main-win.h" line="618"/>
        <source>Go</source>
        <translation>Start</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="325"/>
        <location filename="../../build/src/ui_main-win.h" line="619"/>
        <source>Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">&amp;Om</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="343"/>
        <location filename="../../build/src/ui_main-win.h" line="621"/>
        <source>&amp;New Window</source>
        <translation>&amp;Nytt vindu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <location filename="../../build/src/ui_main-win.h" line="623"/>
        <source>New Window</source>
        <translation>Nytt vindu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <location filename="../../build/src/ui_main-win.h" line="626"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="357"/>
        <location filename="../../build/src/ui_main-win.h" line="628"/>
        <source>Show &amp;Hidden</source>
        <translation>Vis &amp;skjulte</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="366"/>
        <location filename="../../build/src/ui_main-win.h" line="629"/>
        <source>&amp;Computer</source>
        <translation>&amp;Datamaskin</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="369"/>
        <location filename="../../build/src/ui_main-win.h" line="631"/>
        <source>Ctrl+Shift+C</source>
        <translation>Ctrl+Shift+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="378"/>
        <location filename="../../build/src/ui_main-win.h" line="633"/>
        <source>&amp;Trash</source>
        <translation>&amp;Papirkurv</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <location filename="../../build/src/ui_main-win.h" line="635"/>
        <source>Ctrl+Shift+T</source>
        <translation>Ctrl+Shift+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <location filename="../../build/src/ui_main-win.h" line="637"/>
        <source>&amp;Network</source>
        <translation>&amp;Nettverk</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="398"/>
        <location filename="../../build/src/ui_main-win.h" line="641"/>
        <source>&amp;Desktop</source>
        <translation>&amp;Skrivebord</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="401"/>
        <location filename="../../build/src/ui_main-win.h" line="643"/>
        <source>Ctrl+Shift+D</source>
        <translation>Ctrl+Shift+D</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="410"/>
        <location filename="../../build/src/ui_main-win.h" line="645"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>&amp;Legg til som bokmerke</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="415"/>
        <location filename="../../build/src/ui_main-win.h" line="646"/>
        <source>&amp;Applications</source>
        <translation>&amp;Programmer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="418"/>
        <location filename="../../build/src/ui_main-win.h" line="648"/>
        <source>Ctrl+Shift+A</source>
        <translation>Ctrl+Shift+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="426"/>
        <location filename="../../build/src/ui_main-win.h" line="650"/>
        <source>Reload</source>
        <translation>Last inn igjen</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="434"/>
        <location filename="../../build/src/ui_main-win.h" line="651"/>
        <source>&amp;Icon View</source>
        <translation>&amp;Ikonvisning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="437"/>
        <location filename="../../build/src/ui_main-win.h" line="653"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="445"/>
        <location filename="../../build/src/ui_main-win.h" line="655"/>
        <source>&amp;Compact View</source>
        <translation>&amp;Kompakt visning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="453"/>
        <location filename="../../build/src/ui_main-win.h" line="656"/>
        <source>&amp;Detailed List</source>
        <translation>&amp;Detaljert liste</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="456"/>
        <location filename="../../build/src/ui_main-win.h" line="658"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="464"/>
        <location filename="../../build/src/ui_main-win.h" line="660"/>
        <source>&amp;Thumbnail View</source>
        <translation>&amp;Miniatyrbildevisning</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="473"/>
        <location filename="../../build/src/ui_main-win.h" line="661"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Klipp ut</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="476"/>
        <location filename="../../build/src/ui_main-win.h" line="663"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="485"/>
        <location filename="../../build/src/ui_main-win.h" line="665"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="488"/>
        <location filename="../../build/src/ui_main-win.h" line="667"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="497"/>
        <location filename="../../build/src/ui_main-win.h" line="669"/>
        <source>&amp;Paste</source>
        <translation>&amp;Lim inn</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="500"/>
        <location filename="../../build/src/ui_main-win.h" line="671"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="505"/>
        <location filename="../../build/src/ui_main-win.h" line="673"/>
        <source>Select &amp;All</source>
        <translation>Velg &amp;alt</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="508"/>
        <location filename="../../build/src/ui_main-win.h" line="675"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="513"/>
        <location filename="../../build/src/ui_main-win.h" line="677"/>
        <source>Pr&amp;eferences</source>
        <translation>&amp;Innstillinger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="521"/>
        <location filename="../../build/src/ui_main-win.h" line="678"/>
        <source>&amp;Ascending</source>
        <translation>&amp;Stigende</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="529"/>
        <location filename="../../build/src/ui_main-win.h" line="679"/>
        <source>&amp;Descending</source>
        <translation>&amp;Synkende</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="537"/>
        <location filename="../../build/src/ui_main-win.h" line="680"/>
        <source>&amp;By File Name</source>
        <translation>&amp;Etter filnavn</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="545"/>
        <location filename="../../build/src/ui_main-win.h" line="681"/>
        <source>By &amp;Modification Time</source>
        <translation>Etter &amp;endringsdato</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="553"/>
        <location filename="../../build/src/ui_main-win.h" line="682"/>
        <source>By File &amp;Type</source>
        <translation>Etter fil&amp;type</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="561"/>
        <location filename="../../build/src/ui_main-win.h" line="683"/>
        <source>By &amp;Owner</source>
        <translation>Etter &amp;eier</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="569"/>
        <location filename="../../build/src/ui_main-win.h" line="684"/>
        <source>&amp;Folder First</source>
        <translation>&amp;Mapper først</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="640"/>
        <location filename="../../build/src/ui_main-win.h" line="707"/>
        <source>&amp;Move to Trash</source>
        <translation type="unfinished">&amp;Flytt til papirkurv</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="643"/>
        <location filename="../../build/src/ui_main-win.h" line="709"/>
        <source>Ctrl+Backspace</source>
        <translation>Ctrl+Rettetast</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <location filename="../../build/src/ui_main-win.h" line="716"/>
        <source>Get &amp;Info</source>
        <translation>Hent &amp;info</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <location filename="../../build/src/ui_main-win.h" line="718"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <location filename="../../build/src/ui_main-win.h" line="720"/>
        <source>&amp;Case Sensitive</source>
        <translation>&amp;Forskjell på små og store bokstaver</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="680"/>
        <location filename="../../build/src/ui_main-win.h" line="721"/>
        <source>By File &amp;Size</source>
        <translation>Etter fil&amp;størrelse</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <location filename="../../build/src/ui_main-win.h" line="722"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Lukk vindu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="720"/>
        <location filename="../../build/src/ui_main-win.h" line="733"/>
        <source>&amp;Folder</source>
        <translation>&amp;Mappe</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="732"/>
        <location filename="../../build/src/ui_main-win.h" line="737"/>
        <source>&amp;Blank File</source>
        <translation>&amp;Blank fil</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="743"/>
        <location filename="../../build/src/ui_main-win.h" line="743"/>
        <source>Ctrl+F</source>
        <translation>Ctrl+F</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="760"/>
        <location filename="../../build/src/ui_main-win.h" line="746"/>
        <source>&amp;Go To Folder</source>
        <translation>&amp;Gå til mappe</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="763"/>
        <location filename="../../build/src/ui_main-win.h" line="748"/>
        <source>Go To Folder</source>
        <translation>Gå til mappe</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="766"/>
        <location filename="../../build/src/ui_main-win.h" line="751"/>
        <source>Ctrl+Shift+G</source>
        <translation>Ctrl+Shift+G</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="771"/>
        <location filename="../../build/src/ui_main-win.h" line="753"/>
        <source>&amp;Downloads</source>
        <translation>&amp;Nedlastinger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="774"/>
        <location filename="../../build/src/ui_main-win.h" line="755"/>
        <source>Downloads</source>
        <translation>Nedlastinger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="777"/>
        <location filename="../../build/src/ui_main-win.h" line="758"/>
        <source>Ctrl+Shift+L</source>
        <translation>Ctrl+Shift+L</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="785"/>
        <location filename="../../build/src/ui_main-win.h" line="760"/>
        <source>&amp;Utilities</source>
        <translation>&amp;Verktøy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="788"/>
        <location filename="../../build/src/ui_main-win.h" line="762"/>
        <source>Utilities</source>
        <translation>Verktøy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="791"/>
        <location filename="../../build/src/ui_main-win.h" line="765"/>
        <source>Ctrl+Shift+U</source>
        <translation>Ctrl+Shift+U</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="799"/>
        <location filename="../../build/src/ui_main-win.h" line="767"/>
        <source>&amp;Documents</source>
        <translation>&amp;Dokumenter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="802"/>
        <location filename="../../build/src/ui_main-win.h" line="769"/>
        <source>Documents</source>
        <translation>Dokumenter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="805"/>
        <location filename="../../build/src/ui_main-win.h" line="772"/>
        <source>Ctrl+Shift+O</source>
        <translation>Ctrl+Shift+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="813"/>
        <location filename="../../build/src/ui_main-win.h" line="774"/>
        <source>Open</source>
        <translation>Åpne</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="816"/>
        <location filename="../../build/src/ui_main-win.h" line="776"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../../build/src/ui_main-win.h" line="778"/>
        <source>&amp;Duplicate</source>
        <translation>&amp;Dupliser</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="824"/>
        <location filename="../../build/src/ui_main-win.h" line="780"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="829"/>
        <location filename="../../build/src/ui_main-win.h" line="782"/>
        <source>Empty Trash</source>
        <translation>Tøm papirkurv</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <location filename="../../build/src/ui_main-win.h" line="784"/>
        <source>Ctrl+Alt+Backspace</source>
        <translation>Ctrl+Alt+Rettetast</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <location filename="../../build/src/ui_main-win.h" line="786"/>
        <source>Show Contents</source>
        <translation type="unfinished">Vis innhold</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="840"/>
        <location filename="../../build/src/ui_main-win.h" line="788"/>
        <source>Ctrl+Alt+O</source>
        <translation>Ctrl+Alt+O</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="849"/>
        <location filename="../main-win.ui" line="852"/>
        <location filename="../../build/src/ui_main-win.h" line="790"/>
        <location filename="../../build/src/ui_main-win.h" line="792"/>
        <source>Go Up and Close Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="855"/>
        <location filename="../../build/src/ui_main-win.h" line="795"/>
        <source>Ctrl+Shift+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="578"/>
        <location filename="../../build/src/ui_main-win.h" line="685"/>
        <source>New &amp;Tab</source>
        <translation>Ny &amp;fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="277"/>
        <location filename="../../build/src/ui_main-win.h" line="608"/>
        <source>Ctrl+Up</source>
        <translation type="unfinished">Ctrl+↑</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="289"/>
        <location filename="../../build/src/ui_main-win.h" line="612"/>
        <source>Ctrl+Shift+H</source>
        <translation>Ctrl+Shift+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="301"/>
        <location filename="../../build/src/ui_main-win.h" line="616"/>
        <source>Ctrl+Shift+R</source>
        <translation>Ctrl+Shift+R</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="334"/>
        <location filename="../../build/src/ui_main-win.h" line="620"/>
        <source>&amp;About Filer</source>
        <translation>&amp;Om Filer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="581"/>
        <location filename="../../build/src/ui_main-win.h" line="687"/>
        <source>New Tab</source>
        <translation>Ny fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="584"/>
        <location filename="../main-win.ui" line="701"/>
        <location filename="../../build/src/ui_main-win.h" line="690"/>
        <location filename="../../build/src/ui_main-win.h" line="729"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="596"/>
        <location filename="../../build/src/ui_main-win.h" line="692"/>
        <source>Go &amp;Back</source>
        <translation>Gå &amp;tilbake</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="599"/>
        <location filename="../../build/src/ui_main-win.h" line="694"/>
        <source>Go Back</source>
        <translation>Gå tilbake</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="602"/>
        <location filename="../../build/src/ui_main-win.h" line="697"/>
        <source>Alt+Left</source>
        <translation>Alt+←</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="611"/>
        <location filename="../../build/src/ui_main-win.h" line="699"/>
        <source>Go &amp;Forward</source>
        <translation>Gå &amp;forover</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="614"/>
        <location filename="../../build/src/ui_main-win.h" line="701"/>
        <source>Go Forward</source>
        <translation>Gå forover</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="617"/>
        <location filename="../../build/src/ui_main-win.h" line="704"/>
        <source>Alt+Right</source>
        <translation>Alt+→</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="631"/>
        <location filename="../../build/src/ui_main-win.h" line="706"/>
        <source>&amp;Invert Selection</source>
        <translation>&amp;Inverter utvalg</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Slett</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <location filename="../../build/src/ui_main-win.h" line="711"/>
        <source>&amp;Rename</source>
        <translation>&amp;Gi nytt navn</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="651"/>
        <location filename="../../build/src/ui_main-win.h" line="713"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <location filename="../../build/src/ui_main-win.h" line="715"/>
        <source>C&amp;lose Tab</source>
        <translation>&amp;Lukk fane</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="688"/>
        <location filename="../../build/src/ui_main-win.h" line="724"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>File &amp;Properties</source>
        <translation type="vanished">Fil &amp;egenskaper</translation>
    </message>
    <message>
        <source>Alt+Return</source>
        <translation type="obsolete">Alt+Enter</translation>
    </message>
    <message>
        <source>&amp;Folder Properties</source>
        <translation type="vanished">&amp;Mappeegenskaper</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <location filename="../../build/src/ui_main-win.h" line="726"/>
        <source>Edit Bookmarks</source>
        <translation>Rediger bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <location filename="../../build/src/ui_main-win.h" line="727"/>
        <source>Open &amp;Terminal</source>
        <translation>Åpne &amp;terminal</translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="vanished">F4</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <location filename="../../build/src/ui_main-win.h" line="731"/>
        <source>Open as &amp;Root</source>
        <translation>Åpne som &amp;rot</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <location filename="../../build/src/ui_main-win.h" line="732"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>&amp;Rediger bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="389"/>
        <location filename="../main-win.ui" line="723"/>
        <location filename="../../build/src/ui_main-win.h" line="639"/>
        <location filename="../../build/src/ui_main-win.h" line="735"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="735"/>
        <location filename="../../build/src/ui_main-win.h" line="739"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="740"/>
        <location filename="../../build/src/ui_main-win.h" line="741"/>
        <source>&amp;Find Files</source>
        <translation>&amp;Finn filer</translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="vanished">F3</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="751"/>
        <location filename="../../build/src/ui_main-win.h" line="745"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="93"/>
        <location filename="../../build/src/ui_main-win.h" line="797"/>
        <source>Filter by string...</source>
        <translation>Filtrer etter streng …</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="117"/>
        <location filename="../../build/src/ui_main-win.h" line="798"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="121"/>
        <location filename="../../build/src/ui_main-win.h" line="799"/>
        <source>C&amp;reate New</source>
        <translation>&amp;Opprett ny</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <location filename="../../build/src/ui_main-win.h" line="800"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjelp</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="146"/>
        <location filename="../../build/src/ui_main-win.h" line="801"/>
        <source>&amp;View</source>
        <translation>&amp;Vis</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="150"/>
        <location filename="../../build/src/ui_main-win.h" line="802"/>
        <source>&amp;Sorting</source>
        <translation>&amp;Sortering</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="176"/>
        <location filename="../../build/src/ui_main-win.h" line="803"/>
        <source>&amp;Edit</source>
        <translation>&amp;Rediger</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="194"/>
        <location filename="../../build/src/ui_main-win.h" line="804"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Bokmerker</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="201"/>
        <location filename="../../build/src/ui_main-win.h" line="805"/>
        <source>&amp;Go</source>
        <translation>&amp;Start</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="222"/>
        <location filename="../../build/src/ui_main-win.h" line="806"/>
        <source>&amp;Tool</source>
        <translation>&amp;Verktøy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="239"/>
        <location filename="../../build/src/ui_main-win.h" line="807"/>
        <source>Main Toolbar</source>
        <translation>Hovedverktøylinje</translation>
    </message>
</context>
<context>
    <name>MountOperationPasswordDialog</name>
    <message>
        <location filename="../mount-operation-password.ui" line="20"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="172"/>
        <source>Mount</source>
        <translation type="unfinished">Monter</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="48"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="174"/>
        <source>Connect &amp;anonymously</source>
        <translation>Koble til &amp;anonymt</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="58"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="175"/>
        <source>Connect as u&amp;ser:</source>
        <translation>Koble til som &amp;bruker:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="79"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="176"/>
        <source>&amp;Username:</source>
        <translation>&amp;Brukernavn:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="102"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="177"/>
        <source>&amp;Password:</source>
        <translation>&amp;Passord:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="112"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="178"/>
        <source>&amp;Domain:</source>
        <translation>&amp;Domene:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="127"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="179"/>
        <source>Forget password &amp;immediately</source>
        <translation>Glem passord &amp;umiddelbart</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="137"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="180"/>
        <source>Remember password until you &amp;logout</source>
        <translation type="unfinished">Husk passord til neste &amp;utlogging</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="147"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="181"/>
        <source>Remember &amp;forever</source>
        <translation>Husk &amp;for alltid</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <location filename="../../build/src/ui_preferences.h" line="641"/>
        <source>Preferences</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <location filename="../../build/src/ui_preferences.h" line="650"/>
        <source>User Interface</source>
        <translation>Brukergrensesnitt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <location filename="../../build/src/ui_preferences.h" line="646"/>
        <source>Behavior</source>
        <translation>Oppførsel</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="451"/>
        <location filename="../../build/src/ui_preferences.h" line="652"/>
        <location filename="../../build/src/ui_preferences.h" line="701"/>
        <source>Thumbnail</source>
        <translation>Miniatyrbilde</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">Lydstyrke</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <location filename="../../build/src/ui_preferences.h" line="654"/>
        <source>Advanced</source>
        <translation>Avansert</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="92"/>
        <location filename="../../build/src/ui_preferences.h" line="659"/>
        <source>Save metadata to directories (.DirInfo files)</source>
        <translation>Lagre metadata i mapper (.DirInfo-filer)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="102"/>
        <location filename="../../build/src/ui_preferences.h" line="660"/>
        <source>Spatial mode (folders open in a new window)</source>
        <translation>Spesialmodus (mapper åpnes i et nytt vindu)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="166"/>
        <location filename="../../build/src/ui_preferences.h" line="666"/>
        <source>Icons</source>
        <translation>Ikoner</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <location filename="../../build/src/ui_preferences.h" line="668"/>
        <source>Size of big icons:</source>
        <translation>Stor ikonstørrelse:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="205"/>
        <location filename="../../build/src/ui_preferences.h" line="669"/>
        <source>Size of small icons:</source>
        <translation>Liten ikonstørrelse:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="215"/>
        <location filename="../../build/src/ui_preferences.h" line="670"/>
        <source>Size of thumbnails:</source>
        <translation>Miniatyrbildestørrelse:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="225"/>
        <location filename="../../build/src/ui_preferences.h" line="671"/>
        <source>Size of side pane icons:</source>
        <translation>Sidefeltsikonstørrelse:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="178"/>
        <location filename="../../build/src/ui_preferences.h" line="667"/>
        <source>Icon theme:</source>
        <translation>Ikondrakt:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="298"/>
        <location filename="../../build/src/ui_preferences.h" line="677"/>
        <source>Window</source>
        <translation>Vindu</translation>
    </message>
    <message>
        <source>Always show the tab bar</source>
        <translation type="vanished">Alltid vis i fanefeltet</translation>
    </message>
    <message>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation type="vanished">Vis «Lukk»-knaper i faner	</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="307"/>
        <location filename="../../build/src/ui_preferences.h" line="678"/>
        <source>Remember the size of the last closed window</source>
        <translation>Husk størrelsen av sist lukkede vindu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="314"/>
        <location filename="../../build/src/ui_preferences.h" line="679"/>
        <source>Default width of new windows:</source>
        <translation>Forvalgt bredde for nye vinduer:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="328"/>
        <location filename="../../build/src/ui_preferences.h" line="680"/>
        <source>Default height of new windows:</source>
        <translation>Forvalgt høyde på nye vinduer:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="76"/>
        <location filename="../../build/src/ui_preferences.h" line="657"/>
        <source>Browsing</source>
        <translation>Utforskning</translation>
    </message>
    <message>
        <source>Open files with single click</source>
        <translation type="vanished">Åpne filer med ett klikk</translation>
    </message>
    <message>
        <source>Delay of auto-selection in single click mode (0 to disable)</source>
        <translation type="vanished">Forsinkelse på auto-velging i enkeltklikksmodus (0 for å skru av)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="82"/>
        <location filename="../../build/src/ui_preferences.h" line="658"/>
        <source>Default view mode:</source>
        <translation>Forvalgt visningsmodus:</translation>
    </message>
    <message>
        <source> sec</source>
        <translation type="vanished"> sek</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <location filename="../../build/src/ui_preferences.h" line="661"/>
        <source>File Operations</source>
        <translation>Filoperasjoner</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="118"/>
        <location filename="../../build/src/ui_preferences.h" line="662"/>
        <source>Confirm before deleting files</source>
        <translation>Bekreft før sletting av filer</translation>
    </message>
    <message>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation type="vanished">Flytt slettede filer til papirkurv istedenfor å slette dem fra disken.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="481"/>
        <location filename="../../build/src/ui_preferences.h" line="705"/>
        <source>Show thumbnails of files</source>
        <translation>Vis miniatyrbilder av filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="474"/>
        <location filename="../../build/src/ui_preferences.h" line="704"/>
        <source>Only show thumbnails for local files</source>
        <translation>Kun vis miniatyrbilder for lokale filer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <location filename="../../build/src/ui_preferences.h" line="648"/>
        <source>Display</source>
        <translation type="unfinished">Skjerm</translation>
    </message>
    <message>
        <source>Bookmarks:</source>
        <translation type="vanished">Bokmerker:</translation>
    </message>
    <message>
        <source>Open in current tab</source>
        <translation type="vanished">Åpne i nåværende fane</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="vanished">Åpne i ny fane</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation type="vanished">Åpne i nytt vindu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="125"/>
        <location filename="../../build/src/ui_preferences.h" line="663"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>Slett filer på flyttbare media istedenfor «papirkurv»-opprettelse</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="132"/>
        <location filename="../../build/src/ui_preferences.h" line="664"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation type="unfinished">Bekreft før flytting av filer til papirkurv</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <location filename="../../build/src/ui_preferences.h" line="665"/>
        <source>Don&apos;t ask options on launch executable file</source>
        <translation type="unfinished">Ikke spør om valg ved start av kjørbar fil</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="238"/>
        <location filename="../../build/src/ui_preferences.h" line="672"/>
        <source>User interface</source>
        <translation>Brukergrensesnitt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="251"/>
        <location filename="../../build/src/ui_preferences.h" line="674"/>
        <source>Treat backup files as hidden</source>
        <translation>Behandle sikkerhetskopifiler som skjulte</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="261"/>
        <location filename="../../build/src/ui_preferences.h" line="675"/>
        <source>Always show full file names</source>
        <translation type="unfinished">Alltid vis fullstendige filnavn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <location filename="../../build/src/ui_preferences.h" line="676"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>Vis ikoner av skjulte filer skyggelagt</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="345"/>
        <location filename="../../build/src/ui_preferences.h" line="681"/>
        <source>Show in places</source>
        <translation type="unfinished">Vis i Steder</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <location filename="../../build/src/ui_preferences.h" line="686"/>
        <source>Home</source>
        <translation>Hjem</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="364"/>
        <location filename="../../build/src/ui_preferences.h" line="688"/>
        <source>Desktop</source>
        <translation>Skrivebord</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="376"/>
        <location filename="../../build/src/ui_preferences.h" line="690"/>
        <source>Trash can</source>
        <translation>Papirkurv</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="388"/>
        <location filename="../../build/src/ui_preferences.h" line="692"/>
        <source>Computer</source>
        <translation>Datamaskin</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="400"/>
        <location filename="../../build/src/ui_preferences.h" line="694"/>
        <source>Applications</source>
        <translation>Programmer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="408"/>
        <location filename="../../build/src/ui_preferences.h" line="696"/>
        <source>Devices</source>
        <translation>Enheter</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="416"/>
        <location filename="../../build/src/ui_preferences.h" line="698"/>
        <source>Network</source>
        <translation>Nettverker</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="457"/>
        <location filename="../../build/src/ui_preferences.h" line="702"/>
        <source>Do not generate thumbnails for image files exceeding this size:</source>
        <translation>Ikke generer miniatyrbilder for bildefiler som overstiger denne størrelsen:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="464"/>
        <location filename="../../build/src/ui_preferences.h" line="703"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="605"/>
        <location filename="../../build/src/ui_preferences.h" line="715"/>
        <source>Auto Mount</source>
        <translation>Auto-montering</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <location filename="../../build/src/ui_preferences.h" line="716"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>Monter monterbare volum automatisk ved programoppstart</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="618"/>
        <location filename="../../build/src/ui_preferences.h" line="717"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>Monter flyttbare media automatisk når de settes inn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="625"/>
        <location filename="../../build/src/ui_preferences.h" line="718"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>Vis tilgjengelige valg for flyttbar media når de settes inn</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="635"/>
        <location filename="../../build/src/ui_preferences.h" line="719"/>
        <source>When removable medium unmounted:</source>
        <translation>Når flyttbare media avmonteres:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="641"/>
        <location filename="../../build/src/ui_preferences.h" line="720"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>Lukk &amp;fane inneholdende flyttbart medium</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="648"/>
        <location filename="../../build/src/ui_preferences.h" line="721"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>En&amp;dre mappe i fanen til hjemmemappe</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="508"/>
        <location filename="../../build/src/ui_preferences.h" line="706"/>
        <source>Programs</source>
        <translation>Programmer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="517"/>
        <location filename="../../build/src/ui_preferences.h" line="707"/>
        <source>Terminal emulator:</source>
        <translation>Terminalemulator:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="524"/>
        <location filename="../../build/src/ui_preferences.h" line="708"/>
        <source>Switch &amp;user command:</source>
        <translation>Kommando for &amp;bytting av bruker.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="537"/>
        <location filename="../../build/src/ui_preferences.h" line="709"/>
        <source>Examples: &quot;xterm -e %s&quot; for terminal or &quot;gksu %s&quot; for switching user.
%s = the command line you want to execute with terminal or su.</source>
        <translation>Eksempler: &quot;xterm -e %s&quot; for terminal, eller &quot;gksu %s&quot; for bytting av bruker.
%s = kommandolinjen du ønsker å kjøre med terminal eller su.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="558"/>
        <location filename="../../build/src/ui_preferences.h" line="711"/>
        <source>Templates</source>
        <translation>Maler</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <location filename="../../build/src/ui_preferences.h" line="712"/>
        <source>Show only user defined templates in menu</source>
        <translation>Vis kun brukerdefinerte maler i meny</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="571"/>
        <location filename="../../build/src/ui_preferences.h" line="713"/>
        <source>Show only one template for each MIME type</source>
        <translation type="unfinished">Vis kun én mal for hver mediatype</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <location filename="../../build/src/ui_preferences.h" line="714"/>
        <source>Run default application after creation from template</source>
        <translation>Kjør forvalgt program etter opprettelse fra mal</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="244"/>
        <location filename="../../build/src/ui_preferences.h" line="673"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>Bruk SI-desimalprefiks istedenfor IEC</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filelauncher.cpp" line="186"/>
        <location filename="../mountoperation.cpp" line="185"/>
        <location filename="../utilities.cpp" line="133"/>
        <location filename="../utilities.cpp" line="209"/>
        <location filename="../utilities.cpp" line="285"/>
        <source>Error</source>
        <translation type="unfinished">Feil</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="127"/>
        <source>Rename</source>
        <translation>Gi nytt navn</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="128"/>
        <source>Please enter a new name:</source>
        <translation>Skriv inn et nytt navn:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="133"/>
        <source>The startvolume cannot be renamed.</source>
        <translation>Kan ikke endre navn på startvolum.</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="221"/>
        <source>Create Folder</source>
        <translation>Opprett mappe</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="222"/>
        <source>Create File</source>
        <translation>Opprett fil</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="226"/>
        <source>Please enter a new file name:</source>
        <translation>Skriv inn et nytt filnavn:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="227"/>
        <source>New text file</source>
        <translation>Ny tekstfil</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="231"/>
        <source>Please enter a new folder name:</source>
        <translation>Skriv inn et nytt mappenavn:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="232"/>
        <source>New folder</source>
        <translation>Ny mappe</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="237"/>
        <source>Enter a name for the new %1:</source>
        <translation type="unfinished">Skriv inn navn på den nye %1:</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <location filename="../rename-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="156"/>
        <source>Confirm to replace files</source>
        <translation>Bekreft for å erstatte filer</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="35"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;There is already a file with the same name in this location.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Do you want to replace the existing file?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Det finnes allerede en fil med samme navn her.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Vil du erstatte eksisterende fil?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="56"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="158"/>
        <source>dest</source>
        <translation>mål</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="63"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="159"/>
        <source>with the following file?</source>
        <translation>med følgende fil?</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="76"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="160"/>
        <source>src file info</source>
        <translation>kildefilsinfo</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="89"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="161"/>
        <source>dest file info</source>
        <translation>målfilsinfo</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="102"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="162"/>
        <source>src</source>
        <translation>kilde</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="122"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="163"/>
        <source>&amp;File name:</source>
        <translation type="unfinished">&amp;Filnavn:</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="137"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="164"/>
        <source>Apply this option to all existing files</source>
        <translation>Bruk dette valget for alle eksisterende filer</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../filesearch.ui" line="14"/>
        <location filename="../../build/src/ui_filesearch.h" line="398"/>
        <source>Search Files</source>
        <translation>Søk i filer</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="29"/>
        <location filename="../../build/src/ui_filesearch.h" line="408"/>
        <source>Name/Location</source>
        <translation>Navn/sted</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="35"/>
        <location filename="../../build/src/ui_filesearch.h" line="399"/>
        <source>File Name Patterns:</source>
        <translation>Filnavnsmønster:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="41"/>
        <location filename="../../build/src/ui_filesearch.h" line="400"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="48"/>
        <location filename="../../build/src/ui_filesearch.h" line="401"/>
        <source>Case insensitive</source>
        <translation>Ingen forskjell på små og store bokstaver</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="55"/>
        <location filename="../../build/src/ui_filesearch.h" line="402"/>
        <source>Use regular expression</source>
        <translation>Bruk regulært uttrykk</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="65"/>
        <location filename="../../build/src/ui_filesearch.h" line="403"/>
        <source>Places to Search:</source>
        <translation>Steder å søke:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="78"/>
        <location filename="../../build/src/ui_filesearch.h" line="404"/>
        <source>&amp;Add</source>
        <translation>&amp;Legg til</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="90"/>
        <location filename="../../build/src/ui_filesearch.h" line="405"/>
        <source>&amp;Remove</source>
        <translation>&amp;Fjern</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="119"/>
        <location filename="../../build/src/ui_filesearch.h" line="406"/>
        <source>Search in sub directories</source>
        <translation>Søk i undermapper</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="126"/>
        <location filename="../../build/src/ui_filesearch.h" line="407"/>
        <source>Search for hidden files</source>
        <translation>Søk etter skjulte filer</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="137"/>
        <location filename="../../build/src/ui_filesearch.h" line="416"/>
        <source>File Type</source>
        <translation>Filtype</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="143"/>
        <location filename="../../build/src/ui_filesearch.h" line="409"/>
        <source>Only search for files of following types:</source>
        <translation>Kun søk etter filer av følgende typer:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="149"/>
        <location filename="../../build/src/ui_filesearch.h" line="410"/>
        <source>Text files</source>
        <translation>Tekstfiler</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="156"/>
        <location filename="../../build/src/ui_filesearch.h" line="411"/>
        <source>Image files</source>
        <translation>Bildefiler</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="163"/>
        <location filename="../../build/src/ui_filesearch.h" line="412"/>
        <source>Audio files</source>
        <translation>Lydfiler</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="170"/>
        <location filename="../../build/src/ui_filesearch.h" line="413"/>
        <source>Video files</source>
        <translation>Videofiler</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="177"/>
        <location filename="../../build/src/ui_filesearch.h" line="414"/>
        <source>Documents</source>
        <translation>Dokumenter</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="184"/>
        <location filename="../../build/src/ui_filesearch.h" line="415"/>
        <source>Folders</source>
        <translation>Mapper</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="208"/>
        <location filename="../../build/src/ui_filesearch.h" line="420"/>
        <source>Content</source>
        <translation>Innhold</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="214"/>
        <location filename="../../build/src/ui_filesearch.h" line="417"/>
        <source>File contains:</source>
        <translation type="unfinished">Filinnhold:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="223"/>
        <location filename="../../build/src/ui_filesearch.h" line="418"/>
        <source>Case insensiti&amp;ve</source>
        <translation>&amp;Ingen forskjell på små og store bokstaver</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="230"/>
        <location filename="../../build/src/ui_filesearch.h" line="419"/>
        <source>&amp;Use regular expression</source>
        <translation>&amp;Bruk regulært uttrykk</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="254"/>
        <location filename="../../build/src/ui_filesearch.h" line="437"/>
        <source>Properties</source>
        <translation>Egenskaper</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="260"/>
        <location filename="../../build/src/ui_filesearch.h" line="421"/>
        <source>File Size:</source>
        <translation>Filstørrelse:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="266"/>
        <location filename="../../build/src/ui_filesearch.h" line="422"/>
        <source>Larger than:</source>
        <translation>Større enn:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="282"/>
        <location filename="../filesearch.ui" line="323"/>
        <location filename="../../build/src/ui_filesearch.h" line="423"/>
        <location filename="../../build/src/ui_filesearch.h" line="429"/>
        <source>Bytes</source>
        <translation>Byte</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="287"/>
        <location filename="../filesearch.ui" line="328"/>
        <location filename="../../build/src/ui_filesearch.h" line="424"/>
        <location filename="../../build/src/ui_filesearch.h" line="430"/>
        <source>KiB</source>
        <translation>KiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="292"/>
        <location filename="../filesearch.ui" line="333"/>
        <location filename="../../build/src/ui_filesearch.h" line="425"/>
        <location filename="../../build/src/ui_filesearch.h" line="431"/>
        <source>MiB</source>
        <translation>MiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="297"/>
        <location filename="../filesearch.ui" line="338"/>
        <location filename="../../build/src/ui_filesearch.h" line="426"/>
        <location filename="../../build/src/ui_filesearch.h" line="432"/>
        <source>GiB</source>
        <translation>GiB</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="307"/>
        <location filename="../../build/src/ui_filesearch.h" line="428"/>
        <source>Smaller than:</source>
        <translation>Mindre enn:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="351"/>
        <location filename="../../build/src/ui_filesearch.h" line="434"/>
        <source>Last Modified Time:</source>
        <translation>Sist endret:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="357"/>
        <location filename="../../build/src/ui_filesearch.h" line="435"/>
        <source>Earlier than:</source>
        <translation>Tidligere enn:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="364"/>
        <location filename="../../build/src/ui_filesearch.h" line="436"/>
        <source>Later than:</source>
        <translation>Senere enn:</translation>
    </message>
</context>
</TS>
