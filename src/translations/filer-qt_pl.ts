<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="pl_PL">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <location filename="../../build/src/ui_about.h" line="140"/>
        <source>About</source>
        <translation>O programie</translation>
    </message>
    <message>
        <location filename="../about.ui" line="37"/>
        <location filename="../../build/src/ui_about.h" line="142"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="60"/>
        <location filename="../../build/src/ui_about.h" line="144"/>
        <source>The Desktop Experience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="70"/>
        <location filename="../../build/src/ui_about.h" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../../build/src/ui_about.h" line="154"/>
        <source>Filer

Copyright (C) 2020-21 Simon Peter
Copyright (C) 2021 Chris Moore

Originally based on PCMan File Manager
Portions Copyright (C) 2009-14 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Application icon

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

Copyright (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

Used with permission of the creator https://raphaellopes.me/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lightweight file manager</source>
        <translation type="vanished">Lekki menedżer plików</translation>
    </message>
    <message>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation type="vanished">Programowanie:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://lxqt.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://lxqt-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="90"/>
        <location filename="../../build/src/ui_about.h" line="153"/>
        <source>Authors</source>
        <translation>Autorzy</translation>
    </message>
    <message>
        <location filename="../about.ui" line="99"/>
        <location filename="../../build/src/ui_about.h" line="146"/>
        <source>Programming:
* Simon Peter (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Application icon:
* Raphael Lopes (https://raphaellopes.me/)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="116"/>
        <location filename="../../build/src/ui_about.h" line="184"/>
        <source>License</source>
        <translation>Licencja</translation>
    </message>
</context>
<context>
    <name>AppChooserDialog</name>
    <message>
        <location filename="../app-chooser-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="150"/>
        <source>Choose an Application</source>
        <translation>Wybór programu</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="36"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="151"/>
        <source>Installed Applications</source>
        <translation>Zainstalowane programy</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="46"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="163"/>
        <source>Custom Command</source>
        <translation>Własny wiersz poleceń</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="52"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="152"/>
        <source>Command line to execute:</source>
        <translation>Wiersz poleceń do wykonania:</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="62"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="153"/>
        <source>Application name:</source>
        <translation>Nazwa programu:</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="72"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="154"/>
        <source>&lt;b&gt;These special codes can be used in the command line:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Represents a single file name&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Represents multiple file names&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Represents a single URI of the file&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Represents multiple URIs&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation>&lt;b&gt;W wierszu poleceń można użyć następujących oznaczeń:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: nazwa pierwszego z zaznaczonych plików&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: nazwy wszystkich zaznaczonych plików&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: ścieżka pierwszego z zaznaczonych plików&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: ścieżki wszystkich zaznaczonych plików&lt;/li&gt;
&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="91"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="161"/>
        <source>Keep terminal window open after command execution</source>
        <translation>Zachowanie otwartego terminala po wykonaniu polecenia</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="98"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="162"/>
        <source>Execute in terminal emulator</source>
        <translation>Uruchomienie w emulatorze terminala</translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="109"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="164"/>
        <source>Set selected application as default action of this file type</source>
        <translation>Ustaw wybrany program jako domyślną akcję dla tego typu plików</translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <location filename="../../build/src/ui_autorun.h" line="108"/>
        <source>Removable medium is inserted</source>
        <translation>Wykryto nośnik wymienny</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <location filename="../../build/src/ui_autorun.h" line="110"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;Wykryto nośnik wymienny&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <location filename="../../build/src/ui_autorun.h" line="111"/>
        <source>Type of medium:</source>
        <translation>Typ nośnika:</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <location filename="../../build/src/ui_autorun.h" line="112"/>
        <source>Detecting...</source>
        <translation>Wykrywanie...</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <location filename="../../build/src/ui_autorun.h" line="113"/>
        <source>Please select the action you want to perform:</source>
        <translation>Wybierz działanie, które chcesz wykonać:</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="72"/>
        <source>Form</source>
        <translation>Formularz</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="73"/>
        <source>Desktop</source>
        <translation>Pulpit</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="74"/>
        <source>Desktop folder:</source>
        <translation>Katalog Pulpitu:</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="76"/>
        <source>Image file</source>
        <translation>Plik obrazka</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="81"/>
        <source>Folder path</source>
        <translation>Ścieżka katalogu</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="82"/>
        <source>&amp;Browse</source>
        <translation>&amp;Wybierz</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="205"/>
        <source>Desktop Preferences</source>
        <translation>Ustawienia pulpitu</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="20"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="206"/>
        <source>Background</source>
        <translation>Tło</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="207"/>
        <source>Wallpaper mode:</source>
        <translation>Tryb:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="55"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="208"/>
        <source>Wallpaper image file:</source>
        <translation>Obraz tła:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="75"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="210"/>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="147"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="222"/>
        <source>Text color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="160"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="223"/>
        <source>Shadow color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="173"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="224"/>
        <source>Font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select background color:</source>
        <translation type="vanished">Kolor tła:</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="84"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="212"/>
        <source>Image file</source>
        <translation>Plik obrazka</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="90"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="217"/>
        <source>Image file path</source>
        <translation>Ścieżka obrazka</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="97"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="218"/>
        <source>&amp;Browse</source>
        <translation>&amp;Wybierz</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="109"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="219"/>
        <source>Label Text</source>
        <translation>Tekst</translation>
    </message>
    <message>
        <source>Select  text color:</source>
        <translation type="vanished">Kolor tekstu:</translation>
    </message>
    <message>
        <source>Select shadow color:</source>
        <translation type="vanished">Kolor cienia:</translation>
    </message>
    <message>
        <source>Select font:</source>
        <translation type="vanished">Czcionka:</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">Wygląd</translation>
    </message>
    <message>
        <source>Window Manager</source>
        <translation type="vanished">Menedżer Okien</translation>
    </message>
    <message>
        <source>Show menus provided by window managers when desktop is clicked</source>
        <translation type="vanished">Wyświetlanie menu menedżera okien po kliknięciu na pulpicie</translation>
    </message>
    <message>
        <source>Desktop</source>
        <translation type="vanished">Pulpit</translation>
    </message>
    <message>
        <source>Desktop folder:</source>
        <translation type="vanished">Katalog Pulpitu:</translation>
    </message>
    <message>
        <source>Folder path</source>
        <translation type="vanished">Ścieżka katalogu</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">Zaawansowane</translation>
    </message>
</context>
<context>
    <name>EditBookmarksDialog</name>
    <message>
        <location filename="../edit-bookmarks.ui" line="14"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="103"/>
        <source>Edit Bookmarks</source>
        <translation>Edycja zakładek</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="42"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="106"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="47"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="105"/>
        <source>Location</source>
        <translation>Położenie</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="67"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="107"/>
        <source>&amp;Add Item</source>
        <translation>&amp;Dodaj</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="77"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="108"/>
        <source>&amp;Remove Item</source>
        <translation>&amp;Usuń</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="102"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="109"/>
        <source>Use drag and drop to reorder the items</source>
        <translation>Przeciągnij i upuść aby zmienić kolejność</translation>
    </message>
</context>
<context>
    <name>ExecFileDialog</name>
    <message>
        <location filename="../exec-file.ui" line="14"/>
        <location filename="../../build/src/ui_exec-file.h" line="114"/>
        <source>Execute file</source>
        <translation>Uruchom</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="39"/>
        <location filename="../../build/src/ui_exec-file.h" line="116"/>
        <source>&amp;Open</source>
        <translation>&amp;Otwórz</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="52"/>
        <location filename="../../build/src/ui_exec-file.h" line="117"/>
        <source>E&amp;xecute</source>
        <translation>&amp;Uruchom</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="62"/>
        <location filename="../../build/src/ui_exec-file.h" line="118"/>
        <source>Execute in &amp;Terminal</source>
        <translation>Uruchom w &amp;terminalu</translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="85"/>
        <location filename="../../build/src/ui_exec-file.h" line="119"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>FileOperationDialog</name>
    <message>
        <location filename="../file-operation-dialog.ui" line="25"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="124"/>
        <source>Destination:</source>
        <translation>Cel:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="48"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="126"/>
        <source>Processing:</source>
        <translation>Przetwarzanie:</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="61"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="127"/>
        <source>Preparing...</source>
        <translation>Przygotowanie...</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="68"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="128"/>
        <source>Progress</source>
        <translation>Postęp</translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="88"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="129"/>
        <source>Time remaining:</source>
        <translation>Pozostały czas:</translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <source>File Properties</source>
        <translation type="vanished">Właściwości pliku</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="133"/>
        <location filename="../../build/src/ui_file-props.h" line="365"/>
        <source>General</source>
        <translation>Ogólne</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="208"/>
        <location filename="../../build/src/ui_file-props.h" line="372"/>
        <source>Location:</source>
        <translation>Położenie:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="139"/>
        <location filename="../../build/src/ui_file-props.h" line="366"/>
        <source>File type:</source>
        <translation>Typ pliku:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="317"/>
        <location filename="../../build/src/ui_file-props.h" line="380"/>
        <source>Open with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="323"/>
        <location filename="../../build/src/ui_file-props.h" line="381"/>
        <source>Mime type:</source>
        <translation>Typ mime:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="162"/>
        <location filename="../../build/src/ui_file-props.h" line="368"/>
        <source>File size:</source>
        <translation>Rozmiar pliku:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="20"/>
        <location filename="../../build/src/ui_file-props.h" line="362"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="60"/>
        <location filename="../../build/src/ui_file-props.h" line="363"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="185"/>
        <location filename="../../build/src/ui_file-props.h" line="370"/>
        <source>On-disk size:</source>
        <translation>Rozmiar na dysku:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="260"/>
        <location filename="../../build/src/ui_file-props.h" line="376"/>
        <source>Last modified:</source>
        <translation>Ostatnia modyfikacja:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="423"/>
        <location filename="../../build/src/ui_file-props.h" line="385"/>
        <source>Everyone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="234"/>
        <location filename="../../build/src/ui_file-props.h" line="374"/>
        <source>Link target:</source>
        <translation>Położenie docelowe:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="346"/>
        <location filename="../../build/src/ui_file-props.h" line="383"/>
        <source>Open With:</source>
        <translation>Otwieranie za pomocą:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="283"/>
        <location filename="../../build/src/ui_file-props.h" line="378"/>
        <source>Last accessed:</source>
        <translation>Ostatni dostęp:</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation type="vanished">Uprawnienia</translation>
    </message>
    <message>
        <source>Ownership</source>
        <translation type="vanished">Właściciel</translation>
    </message>
    <message>
        <source>Group:</source>
        <translation type="vanished">Grupa:</translation>
    </message>
    <message>
        <source>Owner:</source>
        <translation type="vanished">Właściciel:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="380"/>
        <location filename="../../build/src/ui_file-props.h" line="384"/>
        <source>Access Control</source>
        <translation>Prawa dostępu</translation>
    </message>
    <message>
        <source>Other:</source>
        <translation type="vanished">Inni:</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="440"/>
        <location filename="../../build/src/ui_file-props.h" line="386"/>
        <source>Make the file executable</source>
        <translation>Ustaw prawo wykonywalności</translation>
    </message>
    <message>
        <source>Read</source>
        <translation type="vanished">Odczyt</translation>
    </message>
    <message>
        <source>Write</source>
        <translation type="vanished">Zapis</translation>
    </message>
    <message>
        <source>Execute</source>
        <translation type="vanished">Wykonanie</translation>
    </message>
    <message>
        <source>Sticky</source>
        <translation type="vanished">Sticky</translation>
    </message>
    <message>
        <source>SetUID</source>
        <translation type="vanished">SetUID</translation>
    </message>
    <message>
        <source>SetGID</source>
        <translation type="vanished">SetGID</translation>
    </message>
    <message>
        <source>Advanced Mode</source>
        <translation type="vanished">Zaawansowane</translation>
    </message>
</context>
<context>
    <name>Filer::Application</name>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Name of configuration profile</source>
        <translation>Nazwa profilu konfiguracji</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>PROFILE</source>
        <translation>PROFIL</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Run Filer as a daemon</source>
        <translation>Uruchom Filer jako daemon</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Quit Filer</source>
        <translation>Zakończ Filer</translation>
    </message>
    <message>
        <source>Launch desktop manager</source>
        <translation type="vanished">Uruchom menedżera pulpitu</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Launch desktop manager (deprecated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>Wyłądż menedżera pulpitu jeżeli jest uruchomiony</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>Otwórz okno dialogowe ustawień pulpitu na stronie o określonej nazwie</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <location filename="../application.cpp" line="220"/>
        <source>NAME</source>
        <translation>NAZWA</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="207"/>
        <source>Open new window</source>
        <translation>Otwórz nowe okno</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="210"/>
        <source>Open Find Files utility</source>
        <translation>Otwórz narzędzie &quot;Szukaj plików&quot;</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>Ustaw tło pulpitu z obrazka PLIK</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>FILE</source>
        <translation>PLIK</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>Set mode of desktop wallpaper. MODE=(color|stretch|fit|center|tile)</source>
        <translation>Ustaw tryb tapety pulpitu. TRYB=(color|stretch|fit|center|tile)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>MODE</source>
        <translation>TRYB</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="220"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>Otwórz okno dialogowe Ustawień na stronie o określonej nazwie</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>Files or directories to open</source>
        <translation>Pliki lub katalogi do otwarcia</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[PLIK1, PLIK2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="512"/>
        <location filename="../application.cpp" line="519"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="519"/>
        <source>Terminal emulator is not set.</source>
        <translation>Nie ustawiono emulatora terminala.</translation>
    </message>
</context>
<context>
    <name>Filer::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="43"/>
        <source>Open in file manager</source>
        <translation>Otwórz w menedżerze plików</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="133"/>
        <source>Removable Disk</source>
        <translation>Nośnik wymienny</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="49"/>
        <source>Fill with background color only</source>
        <translation>Wypełnij tylko kolorem</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="50"/>
        <source>Transparent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="51"/>
        <source>Stretch to fill the entire screen</source>
        <translation>Rozciągnij do rozmiaru ekranu</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="52"/>
        <source>Stretch to fit the screen</source>
        <translation>Dopasuj do rozmiaru ekranu</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Center on the screen</source>
        <translation>Wyśrodkuj</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>Sąsiadująco</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="145"/>
        <source>Image Files</source>
        <translation>Pliki obrazków</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="439"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>Przyp&amp;nij do aktualnej pozycji</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="461"/>
        <source>Desktop Preferences</source>
        <translation>Ustawienia pulpitu</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="528"/>
        <source>Version: %1</source>
        <translation type="unfinished">Wersja: %1</translation>
    </message>
</context>
<context>
    <name>Filer::GotoFolderDialog</name>
    <message>
        <location filename="../gotofolderwindow.cpp" line="47"/>
        <source>Go</source>
        <translation type="unfinished">Przejdź</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="48"/>
        <source>Cancel</source>
        <translation type="unfinished">Anuluj</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="59"/>
        <source>Go To Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Filer::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="210"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>Wyczyść tekst (Ctrl+K)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>Version: %1</source>
        <translation>Wersja: %1</translation>
    </message>
    <message>
        <source>&amp;Move to Trash</source>
        <translation type="vanished">&amp;Przenieś do kosza</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Usuń</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1464"/>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Switch user command is not set.</source>
        <translation>Nie ustawiono polecenia do przełączania użytkowników.</translation>
    </message>
</context>
<context>
    <name>Filer::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="192"/>
        <source>Icon View</source>
        <translation>Ikony</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="193"/>
        <source>Compact Icon View</source>
        <translation>Lista zwarta</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="194"/>
        <source>Thumbnail View</source>
        <translation>Miniatury</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="195"/>
        <source>Detailed List View</source>
        <translation>Lista szczegółowa</translation>
    </message>
</context>
<context>
    <name>Filer::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="246"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="278"/>
        <source>%n items</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="280"/>
        <source>1 item</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="436"/>
        <source>%1 items selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="440"/>
        <source>%1 item selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Free space: %1 (Total: %2)</source>
        <translation type="vanished">Wolna przestrzeń: %1 (całkowita: %2)</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="265"/>
        <source>%1 available</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%n item(s)</source>
        <translation type="vanished">
            <numerusform>%n element</numerusform>
            <numerusform>%n elementy</numerusform>
            <numerusform>%n elementów</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n hidden)</source>
        <translation type="vanished">
            <numerusform> (%n ukryty)</numerusform>
            <numerusform> (%n ukryte)</numerusform>
            <numerusform> (%n ukrytych)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 item(s) selected</source>
        <translation type="obsolete">
            <numerusform>Zaznaczono %1 elementy</numerusform>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Filer::View</name>
    <message>
        <source>Open in New T&amp;ab</source>
        <translation type="vanished">Otwórz w nowej &amp;karcie</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="111"/>
        <source>Open in New Win&amp;dow</source>
        <translation>Otwórz w nowym &amp;oknie</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="119"/>
        <source>Open in Termina&amp;l</source>
        <translation>Otwórz w &amp;terminalu</translation>
    </message>
</context>
<context>
    <name>FindFilesDialog</name>
    <message>
        <location filename="../file-search.ui" line="14"/>
        <source>Find Files</source>
        <translation>Znajdź pliki</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="24"/>
        <source>Name/Location</source>
        <translation>Nazwa/Lokalizacja</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="30"/>
        <source>File name patterns</source>
        <translation>Wzorce nazwy pliku</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="36"/>
        <source>Pattern:</source>
        <translation>Wzorzec:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="46"/>
        <location filename="../file-search.ui" line="221"/>
        <source>Case insensitive</source>
        <translation>Ignoruj wielkość liter</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="53"/>
        <location filename="../file-search.ui" line="228"/>
        <source>Use regular expression</source>
        <translation>Użyj wyrażeń regularnych</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="63"/>
        <source>Places to search</source>
        <translation>Szukaj w</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="76"/>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="88"/>
        <source>Remove</source>
        <translation>Usuń</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="117"/>
        <source>Search in sub directories</source>
        <translation>Szukaj w podkatalogach</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="124"/>
        <source>Search hidden files</source>
        <translation>Szukaj wśród ukrytych plików</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="135"/>
        <location filename="../file-search.ui" line="141"/>
        <source>File Type</source>
        <translation>Typ pliku</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="147"/>
        <source>Only search for files of following types:</source>
        <translation>Szukaj tylko plików o podanym typie:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="154"/>
        <source>Text files</source>
        <translation>Pliki tekstowe</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="161"/>
        <source>Image files</source>
        <translation>Obrazy</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="168"/>
        <source>Audio files</source>
        <translation>Pliki dźwiękowe</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="175"/>
        <source>Video files</source>
        <translation>Pliki wideo</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="182"/>
        <source>Documents</source>
        <translation>Dokumenty</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="206"/>
        <source>Content</source>
        <translation>Zawartość</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="212"/>
        <source>File contains</source>
        <translation>Pliki zawierające</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="252"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Ustawienia</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="258"/>
        <source>File Size</source>
        <translation>Rozmiar pliku</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="264"/>
        <source>Bigger than:</source>
        <translation>Większy niż:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="289"/>
        <source>Smaller than:</source>
        <translation>Mniejszy niż:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="317"/>
        <source>Last Modified Time</source>
        <translation>Czas ostatniej modyfikacji</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="323"/>
        <source>Earlier than:</source>
        <translation>Wcześniejszy niż:</translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="340"/>
        <source>Later than:</source>
        <translation>Późniejszy niż:</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserComboBox</name>
    <message>
        <location filename="../appchoosercombobox.cpp" line="79"/>
        <source>Customize</source>
        <translation>Dostosuj</translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserDialog</name>
    <message>
        <location filename="../appchooserdialog.cpp" line="262"/>
        <source>Select an application to open &quot;%1&quot; files</source>
        <translation>WYbierz program do otwarcia &quot;%1&quot; plików</translation>
    </message>
</context>
<context>
    <name>Fm::CreateNewMenu</name>
    <message>
        <location filename="../createnewmenu.cpp" line="29"/>
        <source>Folder</source>
        <translation type="unfinished">Katalog</translation>
    </message>
    <message>
        <location filename="../createnewmenu.cpp" line="33"/>
        <source>Blank File</source>
        <translation type="unfinished">Pusty plik</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeModel</name>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="77"/>
        <source>Loading...</source>
        <translation>Wczytywanie...</translation>
    </message>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="208"/>
        <source>&lt;No sub folders&gt;</source>
        <translation>&lt;Brak katalogów&gt;</translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeView</name>
    <message>
        <location filename="../dirtreeview.cpp" line="220"/>
        <source>Open in New Win&amp;dow</source>
        <translation type="unfinished">Otwórz w nowym &amp;oknie</translation>
    </message>
    <message>
        <location filename="../dirtreeview.cpp" line="226"/>
        <source>Open in Termina&amp;l</source>
        <translation type="unfinished">Otwórz w &amp;terminalu</translation>
    </message>
</context>
<context>
    <name>Fm::DndActionMenu</name>
    <message>
        <location filename="../dndactionmenu.cpp" line="26"/>
        <source>Copy here</source>
        <translation>Kopiuj tutaj</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="27"/>
        <source>Move here</source>
        <translation>Przenieś tutaj</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="28"/>
        <source>Create symlink here</source>
        <translation>Utwórz dowiązanie symboliczne</translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="30"/>
        <source>Cancel</source>
        <translation>Anuluj</translation>
    </message>
</context>
<context>
    <name>Fm::EditBookmarksDialog</name>
    <message>
        <location filename="../editbookmarksdialog.cpp" line="96"/>
        <source>New bookmark</source>
        <translation>Nowa zakładka</translation>
    </message>
</context>
<context>
    <name>Fm::ExecFileDialog</name>
    <message>
        <location filename="../execfiledialog.cpp" line="40"/>
        <source>This text file &apos;%1&apos; seems to be an executable script.
What do you want to do with it?</source>
        <translation>Plik tekstowy &apos;%1&apos; wygląda na wykonywalny skrypt.
Co chcesz zrobić z tym plikiem?</translation>
    </message>
    <message>
        <location filename="../execfiledialog.cpp" line="45"/>
        <source>This file &apos;%1&apos; is executable. Do you want to execute it?</source>
        <translation>Plik &apos;%1&apos; jest wykonywalny. Czy chcesz go uruchomić?</translation>
    </message>
</context>
<context>
    <name>Fm::FileMenu</name>
    <message>
        <location filename="../filemenu.cpp" line="93"/>
        <source>Open</source>
        <translation>Otwórz</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="170"/>
        <source>Create &amp;New</source>
        <translation type="unfinished">Utwórz &amp;nowy</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="128"/>
        <source>&amp;Restore</source>
        <translation>&amp;Przywróć</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="99"/>
        <source>Show Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="177"/>
        <source>Cut</source>
        <translation>Wytnij</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="181"/>
        <source>Copy</source>
        <translation>Kopiuj</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="185"/>
        <source>Paste</source>
        <translation>Wklej</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="190"/>
        <source>&amp;Move to Trash</source>
        <translation>Przenieś do &amp;kosza</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="242"/>
        <source>Get Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="247"/>
        <source>&amp;Empty Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="349"/>
        <source>Output</source>
        <translation>Wyjście</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Usuń</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="194"/>
        <source>Rename</source>
        <translation>Zmień nazwę</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="136"/>
        <source>Open With...</source>
        <translation>Otwórz za pomocą...</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="164"/>
        <source>Other Applications</source>
        <translation>Inny program</translation>
    </message>
    <message>
        <source>Extract to...</source>
        <translation type="vanished">Rozpakuj do...</translation>
    </message>
    <message>
        <source>Extract Here</source>
        <translation type="vanished">Rozpakuj tutaj</translation>
    </message>
    <message>
        <source>Compress</source>
        <translation type="vanished">Skompresuj</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation type="vanished">Właściwości</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperation</name>
    <message>
        <location filename="../fileoperation.cpp" line="221"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="222"/>
        <source>Some files cannot be moved to trash can because the underlying file systems don&apos;t support this operation.
Do you want to delete them instead?</source>
        <translation>Niektóre pliki nie mogą zostać przeniesione do kosza ponieważ system plików nie obsługuje takiej operacji.
Czy zamiast tego usunąć te pliki?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="263"/>
        <location filename="../fileoperation.cpp" line="279"/>
        <source>Confirm</source>
        <translation>Potwierdź</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="264"/>
        <source>Do you want to delete the selected files?</source>
        <translation>Czy chcesz usunąć wybrane pliki?</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="280"/>
        <source>Do you want to move the selected files to trash can?</source>
        <translation>Czy chcesz przenieść wybrane pliki do kosza?</translation>
    </message>
</context>
<context>
    <name>Fm::FileOperationDialog</name>
    <message>
        <location filename="../fileoperationdialog.cpp" line="41"/>
        <source>Move files</source>
        <translation>Przenoszenie plików</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="42"/>
        <source>Moving the following files to destination folder:</source>
        <translation>Przenoszenie następujących plików do katalogu docelowego:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="45"/>
        <source>Copy Files</source>
        <translation>Kopiowanie plików</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="46"/>
        <source>Copying the following files to destination folder:</source>
        <translation>Kopiowanie następujących plików do katalogu docelowego:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="49"/>
        <source>Trash Files</source>
        <translation>Przenoszenie plików do kosza</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="50"/>
        <source>Moving the following files to trash can:</source>
        <translation>Przenoszenie następujących plików do kosza:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="53"/>
        <source>Delete Files</source>
        <translation>Usuwanie plików</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="54"/>
        <source>Deleting the following files</source>
        <translation>Usuwanie następujących plików</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="59"/>
        <source>Create Symlinks</source>
        <translation>Tworzenie dowiązań symbolicznych</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="60"/>
        <source>Creating symlinks for the following files:</source>
        <translation>Tworzenie dowiązań dla następujących plików:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="63"/>
        <source>Change Attributes</source>
        <translation>Zmiana uprawnień</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="64"/>
        <source>Changing attributes of the following files:</source>
        <translation>Zmiana uprawnień dla następujących plików:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="69"/>
        <source>Restore Trashed Files</source>
        <translation>Przywracanie plików z kosza</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="70"/>
        <source>Restoring the following files from trash can:</source>
        <translation>Przywracanie następujących plików z kosza:</translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="139"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
</context>
<context>
    <name>Fm::FilePropsDialog</name>
    <message>
        <location filename="../filepropsdialog.cpp" line="148"/>
        <source>View folder content</source>
        <translation>Wyświetlanie zawartości</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="149"/>
        <source>View and modify folder content</source>
        <translation>Wyświetlanie i modyfikowanie zawartości</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="153"/>
        <source>Read</source>
        <translation>Odczyt</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="154"/>
        <source>Read and write</source>
        <translation>Odczyt i zapis</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="156"/>
        <source>Forbidden</source>
        <translation>Zabronione</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="274"/>
        <source>Files of different types</source>
        <translation>Pliki różnego typu</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="304"/>
        <source>Multiple Files</source>
        <translation>Wiele plików</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="432"/>
        <source>Apply changes</source>
        <translation>Zastosuj zmiany</translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="433"/>
        <source>Do you want to recursively apply these changes to all files and sub-folders?</source>
        <translation>Czy chcesz rekursywnie zastosować zmiany do wszystkich plików i podkatalogów?</translation>
    </message>
</context>
<context>
    <name>Fm::FileSearchDialog</name>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>Error</source>
        <translation type="unfinished">Błąd</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>You should add at least add one directory to search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="127"/>
        <source>Select a folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FolderMenu</name>
    <message>
        <location filename="../foldermenu.cpp" line="37"/>
        <source>Create &amp;New</source>
        <translation>Utwórz &amp;nowy</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="44"/>
        <source>&amp;Paste</source>
        <translation>&amp;Wklej</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="50"/>
        <source>Select &amp;All</source>
        <translation>Z&amp;aznacz wszystko</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="54"/>
        <source>Invert Selection</source>
        <translation>Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="60"/>
        <source>Sorting</source>
        <translation>Sortowanie</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="65"/>
        <source>Show Hidden</source>
        <translation>Wyświetlanie ukrytych plików</translation>
    </message>
    <message>
        <source>Folder Pr&amp;operties</source>
        <translation type="vanished">Właściwości &amp;katalogu</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="vanished">Katalog</translation>
    </message>
    <message>
        <source>Blank File</source>
        <translation type="vanished">Pusty plik</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="73"/>
        <source>Get Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="99"/>
        <source>By File Name</source>
        <translation>Według nazwy</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="100"/>
        <source>By Modification Time</source>
        <translation>Według czasu modyfikacji</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="101"/>
        <source>By File Size</source>
        <translation>Według rozmiaru</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="102"/>
        <source>By File Type</source>
        <translation>Według typu</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="103"/>
        <source>By File Owner</source>
        <translation>Wedłuh właściciela pliku</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="115"/>
        <source>Ascending</source>
        <translation>Rosnąco</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="120"/>
        <source>Descending</source>
        <translation>Malejąco</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="135"/>
        <source>Folder First</source>
        <translation>Najpierw katalogi</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="144"/>
        <source>Case Sensitive</source>
        <translation>Uwzględnij wielkość liter</translation>
    </message>
</context>
<context>
    <name>Fm::FolderModel</name>
    <message>
        <location filename="../foldermodel.cpp" line="313"/>
        <source>Name</source>
        <translation>Nazwa</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="316"/>
        <source>Type</source>
        <translation>Typ</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="319"/>
        <source>Size</source>
        <translation>Rozmiar</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="322"/>
        <source>Modified</source>
        <translation>Zmodyfikowany</translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="325"/>
        <source>Owner</source>
        <translation>Właściciel</translation>
    </message>
</context>
<context>
    <name>Fm::FontButton</name>
    <message>
        <location filename="../fontbutton.cpp" line="46"/>
        <source>Bold</source>
        <translation>Pogrubienie</translation>
    </message>
    <message>
        <location filename="../fontbutton.cpp" line="50"/>
        <source>Italic</source>
        <translation>Kursywa</translation>
    </message>
</context>
<context>
    <name>Fm::MountOperationPasswordDialog</name>
    <message>
        <location filename="../mountoperationpassworddialog.cpp" line="40"/>
        <source>&amp;Connect</source>
        <translation>&amp;Podłącz</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesModel</name>
    <message>
        <location filename="../placesmodel.cpp" line="82"/>
        <source>Places</source>
        <translation>Położenia</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="90"/>
        <source>Desktop</source>
        <translation>Pulpit</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="212"/>
        <source>Trash</source>
        <translation>Kosz</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="99"/>
        <source>Computer</source>
        <translation>Komputer</translation>
    </message>
    <message>
        <source>Applications</source>
        <translation type="vanished">Programy</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="126"/>
        <source>Network</source>
        <translation>Sieć</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="40"/>
        <source>Devices</source>
        <translation>Urządzenia</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="137"/>
        <source>Bookmarks</source>
        <translation>Zakładki</translation>
    </message>
</context>
<context>
    <name>Fm::PlacesView</name>
    <message>
        <location filename="../placesview.cpp" line="362"/>
        <source>Empty Trash</source>
        <translation>Opróżnij kosz</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">Zmień nazwę</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="vanished">Usuń</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="351"/>
        <source>Open in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="371"/>
        <source>Move Bookmark Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="376"/>
        <source>Move Bookmark Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="380"/>
        <source>Rename Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="383"/>
        <source>Remove Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="392"/>
        <location filename="../placesview.cpp" line="409"/>
        <source>Unmount</source>
        <translation>Odmontuj</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="396"/>
        <source>Mount</source>
        <translation>Montuj</translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="402"/>
        <source>Eject</source>
        <translation>Wysuń</translation>
    </message>
</context>
<context>
    <name>Fm::RenameDialog</name>
    <message>
        <location filename="../renamedialog.cpp" line="50"/>
        <location filename="../renamedialog.cpp" line="69"/>
        <source>Type: %1
Size: %2
Modified: %3</source>
        <translation>Typ: %1
Rozmiar: %2
Zmodyfikowany: %3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="56"/>
        <source>Type: %1
Modified: %2</source>
        <translation>Typ: %1
Zmodyfikowany: %2</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="75"/>
        <source>Type: %1
Modified: %3</source>
        <translation>Typ: %1
Zmodyfikowany: %3</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="89"/>
        <source>&amp;Overwrite</source>
        <translation>&amp;Nadpisz</translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="91"/>
        <source>&amp;Rename</source>
        <translation>&amp;Zmień nazwę</translation>
    </message>
</context>
<context>
    <name>Fm::SidePane</name>
    <message>
        <location filename="../sidepane.cpp" line="49"/>
        <location filename="../sidepane.cpp" line="133"/>
        <source>Places</source>
        <translation>Położenia</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="50"/>
        <location filename="../sidepane.cpp" line="135"/>
        <source>Directory Tree</source>
        <translation>Drzewo katalogów</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="143"/>
        <source>Shows list of common places, devices, and bookmarks in sidebar</source>
        <translation>Pokazuje listę miejsc, urządzeń oraz zakładek w panelu bocznym</translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="145"/>
        <source>Shows tree of directories in sidebar</source>
        <translation>Pokazuje drzewo katalogów w panelu bocznym</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <location filename="../../build/src/ui_main-win.h" line="602"/>
        <source>File Manager</source>
        <translation>Menedżer plików</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="274"/>
        <location filename="../../build/src/ui_main-win.h" line="605"/>
        <source>Go Up</source>
        <translation>Katalog nadrzędny</translation>
    </message>
    <message>
        <source>Alt+Up</source>
        <translation type="vanished">Alt+Góra</translation>
    </message>
    <message>
        <source>Home</source>
        <translation type="vanished">Katalog użytkownika</translation>
    </message>
    <message>
        <source>Alt+Home</source>
        <translation type="vanished">Alt+Home</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="426"/>
        <location filename="../../build/src/ui_main-win.h" line="650"/>
        <source>Reload</source>
        <translation>Odśwież</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="vanished">F5</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <location filename="../../build/src/ui_main-win.h" line="610"/>
        <source>&amp;Home</source>
        <translation>&amp;Dom</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <location filename="../../build/src/ui_main-win.h" line="614"/>
        <source>&amp;Reload</source>
        <translation>&amp;Odśwież</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="313"/>
        <location filename="../../build/src/ui_main-win.h" line="618"/>
        <source>Go</source>
        <translation>Przejdź</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="325"/>
        <location filename="../../build/src/ui_main-win.h" line="619"/>
        <source>Quit</source>
        <translation>Zamknij</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">&amp;O programie</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <location filename="../../build/src/ui_main-win.h" line="623"/>
        <source>New Window</source>
        <translation>Nowe okno</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <location filename="../../build/src/ui_main-win.h" line="626"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="357"/>
        <location filename="../../build/src/ui_main-win.h" line="628"/>
        <source>Show &amp;Hidden</source>
        <translation>Wyświetl &amp;ukryte pliki</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="366"/>
        <location filename="../../build/src/ui_main-win.h" line="629"/>
        <source>&amp;Computer</source>
        <translation>&amp;Komputer</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="369"/>
        <location filename="../../build/src/ui_main-win.h" line="631"/>
        <source>Ctrl+Shift+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="378"/>
        <location filename="../../build/src/ui_main-win.h" line="633"/>
        <source>&amp;Trash</source>
        <translation>Ko&amp;sz</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <location filename="../../build/src/ui_main-win.h" line="635"/>
        <source>Ctrl+Shift+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <location filename="../../build/src/ui_main-win.h" line="637"/>
        <source>&amp;Network</source>
        <translation>&amp;Sieć</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="398"/>
        <location filename="../../build/src/ui_main-win.h" line="641"/>
        <source>&amp;Desktop</source>
        <translation>&amp;Pulpit</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="401"/>
        <location filename="../../build/src/ui_main-win.h" line="643"/>
        <source>Ctrl+Shift+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="410"/>
        <location filename="../../build/src/ui_main-win.h" line="645"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>&amp;Dodaj zakładkę</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="415"/>
        <location filename="../../build/src/ui_main-win.h" line="646"/>
        <source>&amp;Applications</source>
        <translation>P&amp;rogramy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="418"/>
        <location filename="../../build/src/ui_main-win.h" line="648"/>
        <source>Ctrl+Shift+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="437"/>
        <location filename="../../build/src/ui_main-win.h" line="653"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="456"/>
        <location filename="../../build/src/ui_main-win.h" line="658"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="476"/>
        <location filename="../../build/src/ui_main-win.h" line="663"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="485"/>
        <location filename="../../build/src/ui_main-win.h" line="665"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopiuj</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="488"/>
        <location filename="../../build/src/ui_main-win.h" line="667"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="497"/>
        <location filename="../../build/src/ui_main-win.h" line="669"/>
        <source>&amp;Paste</source>
        <translation>&amp;Wklej</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="500"/>
        <location filename="../../build/src/ui_main-win.h" line="671"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="505"/>
        <location filename="../../build/src/ui_main-win.h" line="673"/>
        <source>Select &amp;All</source>
        <translation>Zazn&amp;acz wszystko</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="513"/>
        <location filename="../../build/src/ui_main-win.h" line="677"/>
        <source>Pr&amp;eferences</source>
        <translation>Pr&amp;eferencje</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="521"/>
        <location filename="../../build/src/ui_main-win.h" line="678"/>
        <source>&amp;Ascending</source>
        <translation>&amp;Wzrastająco</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="529"/>
        <location filename="../../build/src/ui_main-win.h" line="679"/>
        <source>&amp;Descending</source>
        <translation>&amp;Malejąco</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="537"/>
        <location filename="../../build/src/ui_main-win.h" line="680"/>
        <source>&amp;By File Name</source>
        <translation>&amp;Po nazwie pliku</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="545"/>
        <location filename="../../build/src/ui_main-win.h" line="681"/>
        <source>By &amp;Modification Time</source>
        <translation>Po czasie &amp;modyfikacji</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="553"/>
        <location filename="../../build/src/ui_main-win.h" line="682"/>
        <source>By File &amp;Type</source>
        <translation>Po &amp;typie pliku</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="561"/>
        <location filename="../../build/src/ui_main-win.h" line="683"/>
        <source>By &amp;Owner</source>
        <translation>P&amp;o właścicielu</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="569"/>
        <location filename="../../build/src/ui_main-win.h" line="684"/>
        <source>&amp;Folder First</source>
        <translation>&amp;Katalogi pierwsze</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="631"/>
        <location filename="../../build/src/ui_main-win.h" line="706"/>
        <source>&amp;Invert Selection</source>
        <translation>&amp;Odwróć zaznaczenie</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="743"/>
        <location filename="../../build/src/ui_main-win.h" line="743"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="760"/>
        <location filename="../../build/src/ui_main-win.h" line="746"/>
        <source>&amp;Go To Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="763"/>
        <location filename="../../build/src/ui_main-win.h" line="748"/>
        <source>Go To Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="766"/>
        <location filename="../../build/src/ui_main-win.h" line="751"/>
        <source>Ctrl+Shift+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="771"/>
        <location filename="../../build/src/ui_main-win.h" line="753"/>
        <source>&amp;Downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="774"/>
        <location filename="../../build/src/ui_main-win.h" line="755"/>
        <source>Downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="777"/>
        <location filename="../../build/src/ui_main-win.h" line="758"/>
        <source>Ctrl+Shift+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="785"/>
        <location filename="../../build/src/ui_main-win.h" line="760"/>
        <source>&amp;Utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="788"/>
        <location filename="../../build/src/ui_main-win.h" line="762"/>
        <source>Utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="791"/>
        <location filename="../../build/src/ui_main-win.h" line="765"/>
        <source>Ctrl+Shift+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="799"/>
        <location filename="../../build/src/ui_main-win.h" line="767"/>
        <source>&amp;Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="802"/>
        <location filename="../../build/src/ui_main-win.h" line="769"/>
        <source>Documents</source>
        <translation type="unfinished">Dokumenty</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="805"/>
        <location filename="../../build/src/ui_main-win.h" line="772"/>
        <source>Ctrl+Shift+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="813"/>
        <location filename="../../build/src/ui_main-win.h" line="774"/>
        <source>Open</source>
        <translation type="unfinished">Otwórz</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="816"/>
        <location filename="../../build/src/ui_main-win.h" line="776"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../../build/src/ui_main-win.h" line="778"/>
        <source>&amp;Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="824"/>
        <location filename="../../build/src/ui_main-win.h" line="780"/>
        <source>Ctrl+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="829"/>
        <location filename="../../build/src/ui_main-win.h" line="782"/>
        <source>Empty Trash</source>
        <translation type="unfinished">Opróżnij kosz</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <location filename="../../build/src/ui_main-win.h" line="784"/>
        <source>Ctrl+Alt+Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <location filename="../../build/src/ui_main-win.h" line="786"/>
        <source>Show Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="840"/>
        <location filename="../../build/src/ui_main-win.h" line="788"/>
        <source>Ctrl+Alt+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="849"/>
        <location filename="../main-win.ui" line="852"/>
        <location filename="../../build/src/ui_main-win.h" line="790"/>
        <location filename="../../build/src/ui_main-win.h" line="792"/>
        <source>Go Up and Close Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="855"/>
        <location filename="../../build/src/ui_main-win.h" line="795"/>
        <source>Ctrl+Shift+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Usuń</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <location filename="../../build/src/ui_main-win.h" line="711"/>
        <source>&amp;Rename</source>
        <translation>&amp;Zmień nazwę</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <location filename="../../build/src/ui_main-win.h" line="720"/>
        <source>&amp;Case Sensitive</source>
        <translation>Wielkość liter ma znaczenie</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="680"/>
        <location filename="../../build/src/ui_main-win.h" line="721"/>
        <source>By File &amp;Size</source>
        <translation>Po &amp;wielkości pliku</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <location filename="../../build/src/ui_main-win.h" line="722"/>
        <source>&amp;Close Window</source>
        <translation>&amp;Zamknij okno</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="508"/>
        <location filename="../../build/src/ui_main-win.h" line="675"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="271"/>
        <location filename="../../build/src/ui_main-win.h" line="603"/>
        <source>Go &amp;Up</source>
        <translation>Do &amp;góry</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="277"/>
        <location filename="../../build/src/ui_main-win.h" line="608"/>
        <source>Ctrl+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="289"/>
        <location filename="../../build/src/ui_main-win.h" line="612"/>
        <source>Ctrl+Shift+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="301"/>
        <location filename="../../build/src/ui_main-win.h" line="616"/>
        <source>Ctrl+Shift+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="334"/>
        <location filename="../../build/src/ui_main-win.h" line="620"/>
        <source>&amp;About Filer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="343"/>
        <location filename="../../build/src/ui_main-win.h" line="621"/>
        <source>&amp;New Window</source>
        <translation>&amp;Nowe okno</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="434"/>
        <location filename="../../build/src/ui_main-win.h" line="651"/>
        <source>&amp;Icon View</source>
        <translation>&amp;Ikony</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="445"/>
        <location filename="../../build/src/ui_main-win.h" line="655"/>
        <source>&amp;Compact View</source>
        <translation>Lista &amp;zwarta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="453"/>
        <location filename="../../build/src/ui_main-win.h" line="656"/>
        <source>&amp;Detailed List</source>
        <translation>Lista &amp;szczegółowa</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="464"/>
        <location filename="../../build/src/ui_main-win.h" line="660"/>
        <source>&amp;Thumbnail View</source>
        <translation>&amp;Miniatury</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="473"/>
        <location filename="../../build/src/ui_main-win.h" line="661"/>
        <source>Cu&amp;t</source>
        <translation>W&amp;ytnij</translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation type="vanished">Rosnąco</translation>
    </message>
    <message>
        <source>Descending</source>
        <translation type="vanished">Malejąco</translation>
    </message>
    <message>
        <source>By File Name</source>
        <translation type="vanished">Według nazwy</translation>
    </message>
    <message>
        <source>By Modification Time</source>
        <translation type="vanished">Według czasu modyfikacji</translation>
    </message>
    <message>
        <source>By File Type</source>
        <translation type="vanished">Według typu</translation>
    </message>
    <message>
        <source>By Owner</source>
        <translation type="vanished">Według właściciela</translation>
    </message>
    <message>
        <source>Folder First</source>
        <translation type="vanished">Najpierw katalogi</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="578"/>
        <location filename="../../build/src/ui_main-win.h" line="685"/>
        <source>New &amp;Tab</source>
        <translation>Nowa &amp;karta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="581"/>
        <location filename="../../build/src/ui_main-win.h" line="687"/>
        <source>New Tab</source>
        <translation>Nowa karta</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="584"/>
        <location filename="../main-win.ui" line="701"/>
        <location filename="../../build/src/ui_main-win.h" line="690"/>
        <location filename="../../build/src/ui_main-win.h" line="729"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="596"/>
        <location filename="../../build/src/ui_main-win.h" line="692"/>
        <source>Go &amp;Back</source>
        <translation>&amp;Poprzedni katalog</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="599"/>
        <location filename="../../build/src/ui_main-win.h" line="694"/>
        <source>Go Back</source>
        <translation>Poprzedni katalog</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="602"/>
        <location filename="../../build/src/ui_main-win.h" line="697"/>
        <source>Alt+Left</source>
        <translation>Alt+Lewo</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="611"/>
        <location filename="../../build/src/ui_main-win.h" line="699"/>
        <source>Go &amp;Forward</source>
        <translation>&amp;Następny katalog</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="614"/>
        <location filename="../../build/src/ui_main-win.h" line="701"/>
        <source>Go Forward</source>
        <translation>Następny katalog</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="617"/>
        <location filename="../../build/src/ui_main-win.h" line="704"/>
        <source>Alt+Right</source>
        <translation>Alt+Prawo</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="651"/>
        <location filename="../../build/src/ui_main-win.h" line="713"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <location filename="../../build/src/ui_main-win.h" line="715"/>
        <source>C&amp;lose Tab</source>
        <translation>&amp;Zamknij kartę</translation>
    </message>
    <message>
        <source>File &amp;Properties</source>
        <translation type="vanished">Właściwości &amp;pliku</translation>
    </message>
    <message>
        <source>&amp;Folder Properties</source>
        <translation type="vanished">Właściwości &amp;katalogu</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="vanished">Katalog</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="389"/>
        <location filename="../main-win.ui" line="723"/>
        <location filename="../../build/src/ui_main-win.h" line="639"/>
        <location filename="../../build/src/ui_main-win.h" line="735"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <source>Blank File</source>
        <translation type="vanished">Pusty plik</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="735"/>
        <location filename="../../build/src/ui_main-win.h" line="739"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="121"/>
        <location filename="../../build/src/ui_main-win.h" line="799"/>
        <source>C&amp;reate New</source>
        <translation>&amp;Nowy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="150"/>
        <location filename="../../build/src/ui_main-win.h" line="802"/>
        <source>&amp;Sorting</source>
        <translation>&amp;Sortowanie</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="239"/>
        <location filename="../../build/src/ui_main-win.h" line="807"/>
        <source>Main Toolbar</source>
        <translation>Pasek narzędziowy</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="640"/>
        <location filename="../../build/src/ui_main-win.h" line="707"/>
        <source>&amp;Move to Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="643"/>
        <location filename="../../build/src/ui_main-win.h" line="709"/>
        <source>Ctrl+Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <location filename="../../build/src/ui_main-win.h" line="716"/>
        <source>Get &amp;Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <location filename="../../build/src/ui_main-win.h" line="718"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="688"/>
        <location filename="../../build/src/ui_main-win.h" line="724"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>Alt+Return</source>
        <translation type="vanished">Alt+Return</translation>
    </message>
    <message>
        <source>Case Sensitive</source>
        <translation type="vanished">Uwzględniaj wielkość liter</translation>
    </message>
    <message>
        <source>By File Size</source>
        <translation type="vanished">Według rozmiaru</translation>
    </message>
    <message>
        <source>Close Window</source>
        <translation type="vanished">Zamknij okno</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <location filename="../../build/src/ui_main-win.h" line="726"/>
        <source>Edit Bookmarks</source>
        <translation>Edytuj zakładki</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <location filename="../../build/src/ui_main-win.h" line="727"/>
        <source>Open &amp;Terminal</source>
        <translation>Otwórz w &amp;terminalu</translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="vanished">F4</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <location filename="../../build/src/ui_main-win.h" line="731"/>
        <source>Open as &amp;Root</source>
        <translation>Otwórz jako &amp;Root</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <location filename="../../build/src/ui_main-win.h" line="732"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>Edytuj &amp;Zakładki</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="720"/>
        <location filename="../../build/src/ui_main-win.h" line="733"/>
        <source>&amp;Folder</source>
        <translation>&amp;Katalog</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="732"/>
        <location filename="../../build/src/ui_main-win.h" line="737"/>
        <source>&amp;Blank File</source>
        <translation>&amp;Pusty plik</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="740"/>
        <location filename="../../build/src/ui_main-win.h" line="741"/>
        <source>&amp;Find Files</source>
        <translation>&amp;Szukaj plików</translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="vanished">F3</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="751"/>
        <location filename="../../build/src/ui_main-win.h" line="745"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="93"/>
        <location filename="../../build/src/ui_main-win.h" line="797"/>
        <source>Filter by string...</source>
        <translation>Filtruj według ciągu...</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="117"/>
        <location filename="../../build/src/ui_main-win.h" line="798"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <location filename="../../build/src/ui_main-win.h" line="800"/>
        <source>&amp;Help</source>
        <translation>Po&amp;moc</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="146"/>
        <location filename="../../build/src/ui_main-win.h" line="801"/>
        <source>&amp;View</source>
        <translation>&amp;Widok</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="176"/>
        <location filename="../../build/src/ui_main-win.h" line="803"/>
        <source>&amp;Edit</source>
        <translation>&amp;Edycja</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="194"/>
        <location filename="../../build/src/ui_main-win.h" line="804"/>
        <source>&amp;Bookmarks</source>
        <translation>&amp;Zakładki</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="201"/>
        <location filename="../../build/src/ui_main-win.h" line="805"/>
        <source>&amp;Go</source>
        <translation>P&amp;rzejdź</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="222"/>
        <location filename="../../build/src/ui_main-win.h" line="806"/>
        <source>&amp;Tool</source>
        <translation>&amp;Narzędzia</translation>
    </message>
</context>
<context>
    <name>MountOperationPasswordDialog</name>
    <message>
        <location filename="../mount-operation-password.ui" line="20"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="172"/>
        <source>Mount</source>
        <translation>Montowanie</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="48"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="174"/>
        <source>Connect &amp;anonymously</source>
        <translation>Połącz &amp;anonimowo</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="58"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="175"/>
        <source>Connect as u&amp;ser:</source>
        <translation>Połącz jako &amp;użytkownik:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="79"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="176"/>
        <source>&amp;Username:</source>
        <translation>&amp;Użytkownik:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="102"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="177"/>
        <source>&amp;Password:</source>
        <translation>&amp;Hasło:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="112"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="178"/>
        <source>&amp;Domain:</source>
        <translation>&amp;Domena:</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="127"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="179"/>
        <source>Forget password &amp;immediately</source>
        <translation>Zapomnij hasło &amp;od razu</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="137"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="180"/>
        <source>Remember password until you &amp;logout</source>
        <translation>&amp;Pamiętaj hasło do wylogowania</translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="147"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="181"/>
        <source>Remember &amp;forever</source>
        <translation>Pamiętaj na &amp;zawsze</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <location filename="../../build/src/ui_preferences.h" line="641"/>
        <source>Preferences</source>
        <translation>Preferencje</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <location filename="../../build/src/ui_preferences.h" line="650"/>
        <source>User Interface</source>
        <translation>Wyświetlanie</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <location filename="../../build/src/ui_preferences.h" line="646"/>
        <source>Behavior</source>
        <translation>Zachowanie</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="451"/>
        <location filename="../../build/src/ui_preferences.h" line="652"/>
        <location filename="../../build/src/ui_preferences.h" line="701"/>
        <source>Thumbnail</source>
        <translation>Miniatury</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">Wolumen</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <location filename="../../build/src/ui_preferences.h" line="654"/>
        <source>Advanced</source>
        <translation>Zaawansowane</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="92"/>
        <location filename="../../build/src/ui_preferences.h" line="659"/>
        <source>Save metadata to directories (.DirInfo files)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="102"/>
        <location filename="../../build/src/ui_preferences.h" line="660"/>
        <source>Spatial mode (folders open in a new window)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="166"/>
        <location filename="../../build/src/ui_preferences.h" line="666"/>
        <source>Icons</source>
        <translation>Ikony</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <location filename="../../build/src/ui_preferences.h" line="668"/>
        <source>Size of big icons:</source>
        <translation>Rozmiar dużych ikon:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="205"/>
        <location filename="../../build/src/ui_preferences.h" line="669"/>
        <source>Size of small icons:</source>
        <translation>Rozmiar małych ikon:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="215"/>
        <location filename="../../build/src/ui_preferences.h" line="670"/>
        <source>Size of thumbnails:</source>
        <translation>Rozmiar miniatur:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="225"/>
        <location filename="../../build/src/ui_preferences.h" line="671"/>
        <source>Size of side pane icons:</source>
        <translation>Rozmiar ikon panelu bocznego:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="178"/>
        <location filename="../../build/src/ui_preferences.h" line="667"/>
        <source>Icon theme:</source>
        <translation>Motyw ikon:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="298"/>
        <location filename="../../build/src/ui_preferences.h" line="677"/>
        <source>Window</source>
        <translation>Okno</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="314"/>
        <location filename="../../build/src/ui_preferences.h" line="679"/>
        <source>Default width of new windows:</source>
        <translation>Domyślna szerokość nowych okien:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="328"/>
        <location filename="../../build/src/ui_preferences.h" line="680"/>
        <source>Default height of new windows:</source>
        <translation>Domyślna wysokość nowych okien:</translation>
    </message>
    <message>
        <source>Always show the tab bar</source>
        <translation type="vanished">Zawsze pokazuj pasek kart</translation>
    </message>
    <message>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation type="vanished">Pokaż przycisk &apos;Zamknij&apos; na kartach</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="307"/>
        <location filename="../../build/src/ui_preferences.h" line="678"/>
        <source>Remember the size of the last closed window</source>
        <translation>Pamiętaj rozmiar ostatnio zamkniętego okna</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="76"/>
        <location filename="../../build/src/ui_preferences.h" line="657"/>
        <source>Browsing</source>
        <translation>Przeglądanie</translation>
    </message>
    <message>
        <source>Open files with single click</source>
        <translation type="vanished">Otwórz pliki pojedynczym kliknięciem</translation>
    </message>
    <message>
        <source>Delay of auto-selection in single click mode (0 to disable)</source>
        <translation type="vanished">Opóźnienie zaznaczania (0 aby wyłączyć)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="82"/>
        <location filename="../../build/src/ui_preferences.h" line="658"/>
        <source>Default view mode:</source>
        <translation>Domyślny tryb widoku:</translation>
    </message>
    <message>
        <source> sec</source>
        <translation type="vanished"> s</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <location filename="../../build/src/ui_preferences.h" line="661"/>
        <source>File Operations</source>
        <translation>Operacje na plikach</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="118"/>
        <location filename="../../build/src/ui_preferences.h" line="662"/>
        <source>Confirm before deleting files</source>
        <translation>Potwierdź przed usunięciem plików</translation>
    </message>
    <message>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation type="vanished">Przenoś pliki do &quot;Kosza&quot; zamiast usuwania z dysku.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="481"/>
        <location filename="../../build/src/ui_preferences.h" line="705"/>
        <source>Show thumbnails of files</source>
        <translation>Wyświetl miniatury zawartości plików</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="474"/>
        <location filename="../../build/src/ui_preferences.h" line="704"/>
        <source>Only show thumbnails for local files</source>
        <translation>Wyświetl miniatury zawartości tylko lokalnych plików</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <location filename="../../build/src/ui_preferences.h" line="648"/>
        <source>Display</source>
        <translation>Ekran</translation>
    </message>
    <message>
        <source>Bookmarks:</source>
        <translation type="vanished">Zakładki:</translation>
    </message>
    <message>
        <source>Open in current tab</source>
        <translation type="vanished">Otwórz w aktualnej zakładce</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="vanished">Otwórz w nowej zakładce</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation type="vanished">Otwórz w nowym oknie</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="125"/>
        <location filename="../../build/src/ui_preferences.h" line="663"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>Usuwaj pliki z dysków wymiennych zamiast tworzenia &quot;Kosza&quot;</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="132"/>
        <location filename="../../build/src/ui_preferences.h" line="664"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>Potwierdź przed przeniesieniem plików do &quot;Kosza&quot;</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <location filename="../../build/src/ui_preferences.h" line="665"/>
        <source>Don&apos;t ask options on launch executable file</source>
        <translation>Nie pytaj o opcje przy uruchamianiu plików wykonywalnych</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="238"/>
        <location filename="../../build/src/ui_preferences.h" line="672"/>
        <source>User interface</source>
        <translation>Wyświetlanie</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="251"/>
        <location filename="../../build/src/ui_preferences.h" line="674"/>
        <source>Treat backup files as hidden</source>
        <translation>Traktuj pliki kopii zapasowych jako ukryte</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="261"/>
        <location filename="../../build/src/ui_preferences.h" line="675"/>
        <source>Always show full file names</source>
        <translation>Zawsze wyświetlaj pełne nazwy plików</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <location filename="../../build/src/ui_preferences.h" line="676"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>Przyciemń ikony ukrytych plików</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="345"/>
        <location filename="../../build/src/ui_preferences.h" line="681"/>
        <source>Show in places</source>
        <translation>Pokaż w miejscach</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <location filename="../../build/src/ui_preferences.h" line="686"/>
        <source>Home</source>
        <translation>Katalog użytkownika</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="364"/>
        <location filename="../../build/src/ui_preferences.h" line="688"/>
        <source>Desktop</source>
        <translation>Pulpit</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="376"/>
        <location filename="../../build/src/ui_preferences.h" line="690"/>
        <source>Trash can</source>
        <translation>Kosz</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="388"/>
        <location filename="../../build/src/ui_preferences.h" line="692"/>
        <source>Computer</source>
        <translation>Komputer</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="400"/>
        <location filename="../../build/src/ui_preferences.h" line="694"/>
        <source>Applications</source>
        <translation>Aplikacje</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="408"/>
        <location filename="../../build/src/ui_preferences.h" line="696"/>
        <source>Devices</source>
        <translation>Urządzenia</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="416"/>
        <location filename="../../build/src/ui_preferences.h" line="698"/>
        <source>Network</source>
        <translation>Sieć</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="457"/>
        <location filename="../../build/src/ui_preferences.h" line="702"/>
        <source>Do not generate thumbnails for image files exceeding this size:</source>
        <translation>Nie generuj miniatur dla plików większych niż:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="464"/>
        <location filename="../../build/src/ui_preferences.h" line="703"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="605"/>
        <location filename="../../build/src/ui_preferences.h" line="715"/>
        <source>Auto Mount</source>
        <translation>Automatyczne montowanie</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <location filename="../../build/src/ui_preferences.h" line="716"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>Montowanie wolumenów podczas uruchamiania programu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="618"/>
        <location filename="../../build/src/ui_preferences.h" line="717"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>Montowanie nośników wymiennych po wsunięciu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="625"/>
        <location filename="../../build/src/ui_preferences.h" line="718"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>Wyświetlanie możliwych czynności po wsunięciu nośnika wymiennego</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="635"/>
        <location filename="../../build/src/ui_preferences.h" line="719"/>
        <source>When removable medium unmounted:</source>
        <translation>Działania po odmontowaniu nośnika wymiennego:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="641"/>
        <location filename="../../build/src/ui_preferences.h" line="720"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>Zamknij zakładkę nośnika wymiennego</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="648"/>
        <location filename="../../build/src/ui_preferences.h" line="721"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>Zmień katalog w zakładce na katalog użytkownika</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="524"/>
        <location filename="../../build/src/ui_preferences.h" line="708"/>
        <source>Switch &amp;user command:</source>
        <translation>Zmień polecenie &amp;użytkownika:</translation>
    </message>
    <message>
        <source>Archiver in&amp;tegration:</source>
        <translation type="vanished">In&amp;tegracja z menedżerem archiwów:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="558"/>
        <location filename="../../build/src/ui_preferences.h" line="711"/>
        <source>Templates</source>
        <translation>Szablony</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <location filename="../../build/src/ui_preferences.h" line="712"/>
        <source>Show only user defined templates in menu</source>
        <translation>Pokaż tylko szablony zdefiniowane przez użytkownika w menu</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="571"/>
        <location filename="../../build/src/ui_preferences.h" line="713"/>
        <source>Show only one template for each MIME type</source>
        <translation>Pokazuj tylko jeden szablon dla każdego typu MIME</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <location filename="../../build/src/ui_preferences.h" line="714"/>
        <source>Run default application after creation from template</source>
        <translation>Uruchom domyślną aplikację po utworzeniu z szablonu</translation>
    </message>
    <message>
        <source>Close tab containing removable medium</source>
        <translation type="vanished">Zamknięcie karty</translation>
    </message>
    <message>
        <source>Change folder in the tab to home folder</source>
        <translation type="vanished">Przejście do katalogu użytkownika</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="508"/>
        <location filename="../../build/src/ui_preferences.h" line="706"/>
        <source>Programs</source>
        <translation>Programy</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="517"/>
        <location filename="../../build/src/ui_preferences.h" line="707"/>
        <source>Terminal emulator:</source>
        <translation>Emulator terminala:</translation>
    </message>
    <message>
        <source>Switch user command:</source>
        <translation type="vanished">Przełączanie użytkowników:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="537"/>
        <location filename="../../build/src/ui_preferences.h" line="709"/>
        <source>Examples: &quot;xterm -e %s&quot; for terminal or &quot;gksu %s&quot; for switching user.
%s = the command line you want to execute with terminal or su.</source>
        <translation>Przykład: &quot;xterm -e %s&quot; dla terminala lub &quot;gksu %s&quot; dla przełączania użytkownika.
%s = polecenie, które chcesz wykonać za pomocą terminala lub su.</translation>
    </message>
    <message>
        <source>Archiver integration:</source>
        <translation type="vanished">Menedżer archiwów:</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="244"/>
        <location filename="../../build/src/ui_preferences.h" line="673"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>Używanie dziesiętnych prefiksów SI zamiast binarnych IEC</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filelauncher.cpp" line="186"/>
        <location filename="../mountoperation.cpp" line="185"/>
        <location filename="../utilities.cpp" line="133"/>
        <location filename="../utilities.cpp" line="209"/>
        <location filename="../utilities.cpp" line="285"/>
        <source>Error</source>
        <translation>Błąd</translation>
    </message>
    <message>
        <source>Rename File</source>
        <translation type="vanished">Zmiana nazwy</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="127"/>
        <source>Rename</source>
        <translation type="unfinished">Zmień nazwę</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="128"/>
        <source>Please enter a new name:</source>
        <translation>Podaj nową nazwę:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="133"/>
        <source>The startvolume cannot be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="221"/>
        <source>Create Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="226"/>
        <source>Please enter a new file name:</source>
        <translation>Podaj nazwę pliku:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="227"/>
        <source>New text file</source>
        <translation>Nowy plik tekstowy</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="231"/>
        <source>Please enter a new folder name:</source>
        <translation>POdaj nazwę nowego katalogu:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="232"/>
        <source>New folder</source>
        <translation>Nowy katalog</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="237"/>
        <source>Enter a name for the new %1:</source>
        <translation>Podaj nazwę dla pliku %1:</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="222"/>
        <source>Create File</source>
        <translation>Utwórz plik</translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <location filename="../rename-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="156"/>
        <source>Confirm to replace files</source>
        <translation>Potwierdź aby nadpisać pliki</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="35"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;There is already a file with the same name in this location.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Do you want to replace the existing file?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Plik o podanej nazwie już istnieje w katalogu docelowym.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Czy chcesz nadpisać istniejący plik?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="56"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="158"/>
        <source>dest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="63"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="159"/>
        <source>with the following file?</source>
        <translation>następującym plikiem?</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="76"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="160"/>
        <source>src file info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="89"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="161"/>
        <source>dest file info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="102"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="162"/>
        <source>src</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="122"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="163"/>
        <source>&amp;File name:</source>
        <translation>&amp;Nazwa pliku:</translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="137"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="164"/>
        <source>Apply this option to all existing files</source>
        <translation>Zastosuj do wszystkich plików</translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../filesearch.ui" line="14"/>
        <location filename="../../build/src/ui_filesearch.h" line="398"/>
        <source>Search Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="29"/>
        <location filename="../../build/src/ui_filesearch.h" line="408"/>
        <source>Name/Location</source>
        <translation type="unfinished">Nazwa/Lokalizacja</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="35"/>
        <location filename="../../build/src/ui_filesearch.h" line="399"/>
        <source>File Name Patterns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="41"/>
        <location filename="../../build/src/ui_filesearch.h" line="400"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="48"/>
        <location filename="../../build/src/ui_filesearch.h" line="401"/>
        <source>Case insensitive</source>
        <translation type="unfinished">Ignoruj wielkość liter</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="55"/>
        <location filename="../../build/src/ui_filesearch.h" line="402"/>
        <source>Use regular expression</source>
        <translation type="unfinished">Użyj wyrażeń regularnych</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="65"/>
        <location filename="../../build/src/ui_filesearch.h" line="403"/>
        <source>Places to Search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="78"/>
        <location filename="../../build/src/ui_filesearch.h" line="404"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="90"/>
        <location filename="../../build/src/ui_filesearch.h" line="405"/>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="119"/>
        <location filename="../../build/src/ui_filesearch.h" line="406"/>
        <source>Search in sub directories</source>
        <translation type="unfinished">Szukaj w podkatalogach</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="126"/>
        <location filename="../../build/src/ui_filesearch.h" line="407"/>
        <source>Search for hidden files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="137"/>
        <location filename="../../build/src/ui_filesearch.h" line="416"/>
        <source>File Type</source>
        <translation type="unfinished">Typ pliku</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="143"/>
        <location filename="../../build/src/ui_filesearch.h" line="409"/>
        <source>Only search for files of following types:</source>
        <translation type="unfinished">Szukaj tylko plików o podanym typie:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="149"/>
        <location filename="../../build/src/ui_filesearch.h" line="410"/>
        <source>Text files</source>
        <translation type="unfinished">Pliki tekstowe</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="156"/>
        <location filename="../../build/src/ui_filesearch.h" line="411"/>
        <source>Image files</source>
        <translation type="unfinished">Obrazy</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="163"/>
        <location filename="../../build/src/ui_filesearch.h" line="412"/>
        <source>Audio files</source>
        <translation type="unfinished">Pliki dźwiękowe</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="170"/>
        <location filename="../../build/src/ui_filesearch.h" line="413"/>
        <source>Video files</source>
        <translation type="unfinished">Pliki wideo</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="177"/>
        <location filename="../../build/src/ui_filesearch.h" line="414"/>
        <source>Documents</source>
        <translation type="unfinished">Dokumenty</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="184"/>
        <location filename="../../build/src/ui_filesearch.h" line="415"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="208"/>
        <location filename="../../build/src/ui_filesearch.h" line="420"/>
        <source>Content</source>
        <translation type="unfinished">Zawartość</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="214"/>
        <location filename="../../build/src/ui_filesearch.h" line="417"/>
        <source>File contains:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="223"/>
        <location filename="../../build/src/ui_filesearch.h" line="418"/>
        <source>Case insensiti&amp;ve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="230"/>
        <location filename="../../build/src/ui_filesearch.h" line="419"/>
        <source>&amp;Use regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="254"/>
        <location filename="../../build/src/ui_filesearch.h" line="437"/>
        <source>Properties</source>
        <translation type="unfinished">Właściwości</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="260"/>
        <location filename="../../build/src/ui_filesearch.h" line="421"/>
        <source>File Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="266"/>
        <location filename="../../build/src/ui_filesearch.h" line="422"/>
        <source>Larger than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="282"/>
        <location filename="../filesearch.ui" line="323"/>
        <location filename="../../build/src/ui_filesearch.h" line="423"/>
        <location filename="../../build/src/ui_filesearch.h" line="429"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="287"/>
        <location filename="../filesearch.ui" line="328"/>
        <location filename="../../build/src/ui_filesearch.h" line="424"/>
        <location filename="../../build/src/ui_filesearch.h" line="430"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="292"/>
        <location filename="../filesearch.ui" line="333"/>
        <location filename="../../build/src/ui_filesearch.h" line="425"/>
        <location filename="../../build/src/ui_filesearch.h" line="431"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="297"/>
        <location filename="../filesearch.ui" line="338"/>
        <location filename="../../build/src/ui_filesearch.h" line="426"/>
        <location filename="../../build/src/ui_filesearch.h" line="432"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="307"/>
        <location filename="../../build/src/ui_filesearch.h" line="428"/>
        <source>Smaller than:</source>
        <translation type="unfinished">Mniejszy niż:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="351"/>
        <location filename="../../build/src/ui_filesearch.h" line="434"/>
        <source>Last Modified Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="357"/>
        <location filename="../../build/src/ui_filesearch.h" line="435"/>
        <source>Earlier than:</source>
        <translation type="unfinished">Wcześniejszy niż:</translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="364"/>
        <location filename="../../build/src/ui_filesearch.h" line="436"/>
        <source>Later than:</source>
        <translation type="unfinished">Późniejszy niż:</translation>
    </message>
</context>
</TS>
