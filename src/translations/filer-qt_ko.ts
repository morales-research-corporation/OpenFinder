<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <location filename="../../build/src/ui_about.h" line="140"/>
        <source>About</source>
        <translation>정보</translation>
    </message>
    <message>
        <location filename="../about.ui" line="37"/>
        <location filename="../../build/src/ui_about.h" line="142"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:16pt; font-weight:600;&quot;&gt;Filer&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../about.ui" line="60"/>
        <location filename="../../build/src/ui_about.h" line="144"/>
        <source>The Desktop Experience</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="70"/>
        <location filename="../../build/src/ui_about.h" line="145"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;https://github.com/helloSystem/Filer/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://github.com/helloSystem/Filer/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="99"/>
        <location filename="../../build/src/ui_about.h" line="146"/>
        <source>Programming:
* Simon Peter (probono)
* Chris Moore (moochris)
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;

Application icon:
* Raphael Lopes (https://raphaellopes.me/)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.ui" line="125"/>
        <location filename="../../build/src/ui_about.h" line="154"/>
        <source>Filer

Copyright (C) 2020-21 Simon Peter
Copyright (C) 2021 Chris Moore

Originally based on PCMan File Manager
Portions Copyright (C) 2009-14 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Application icon

https://dribbble.com/shots/2541211--Pirate-Finder-icon#

Copyright (C) 2016 Raphael Lopes &lt;raphaellopes8@gmail.com&gt;

Used with permission of the creator https://raphaellopes.me/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lightweight file manager</source>
        <translation type="vanished">경량 파일 관리자</translation>
    </message>
    <message>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://lxqt.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="vanished">&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://lxqt.org/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;https://lxqt-project.org/&lt;/span&gt;&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>Programming:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</source>
        <translation type="vanished">프로그래밍:
* Hong Jen Yee (PCMan) &lt;pcman.tw@gmail.com&gt;
</translation>
    </message>
    <message>
        <location filename="../about.ui" line="90"/>
        <location filename="../../build/src/ui_about.h" line="153"/>
        <source>Authors</source>
        <translation>저자</translation>
    </message>
    <message>
        <source>PCMan File Manager

Copyright (C) 2009 - 2014 洪任諭 (Hong Jen Yee)

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.</source>
        <translation type="vanished">PCMan 파일 관리자

저작권 (C) 2009-2014 洪任諭 (Hong Jen Yee)

이 프로그램은 자유 소프트웨어이며 자유 소프트웨어 재단을 따를 수 있습니다
(Free Software Foundation)에서 발행한 GNU 일반 공개 라이센스 조건입니다
(GNU General Public License ) 두 번째 버전 을 사용하여 수정 및 재게시합니다
이 프로그램 또는 업데이트 된 버전을 자유롭게 사용할 수 있습니다.

이 프로그램을 게시하는 목적은 유용하기를 원합니다.
그러나 특정 목적에 적합한 보증이나 암시적 보증은 없습니다.
자세한 내용은 GNU 일반 공용 라이선스를 참조하십시오.


GNU 일반 공용 라이선스 사본을 프로그램 과 함께 받았습니다.
아직 받지 못한 경우 다음 주소로 편지를 보내주십시오.
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="../about.ui" line="116"/>
        <location filename="../../build/src/ui_about.h" line="184"/>
        <source>License</source>
        <translation>라이선스</translation>
    </message>
</context>
<context>
    <name>AppChooserDialog</name>
    <message>
        <location filename="../app-chooser-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="150"/>
        <source>Choose an Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="36"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="151"/>
        <source>Installed Applications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="46"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="163"/>
        <source>Custom Command</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="52"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="152"/>
        <source>Command line to execute:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="62"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="153"/>
        <source>Application name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="72"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="154"/>
        <source>&lt;b&gt;These special codes can be used in the command line:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;b&gt;%f&lt;/b&gt;: Represents a single file name&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%F&lt;/b&gt;: Represents multiple file names&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%u&lt;/b&gt;: Represents a single URI of the file&lt;/li&gt;
&lt;li&gt;&lt;b&gt;%U&lt;/b&gt;: Represents multiple URIs&lt;/li&gt;
&lt;/ul&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="91"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="161"/>
        <source>Keep terminal window open after command execution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="98"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="162"/>
        <source>Execute in terminal emulator</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../app-chooser-dialog.ui" line="109"/>
        <location filename="../../build/src/ui_app-chooser-dialog.h" line="164"/>
        <source>Set selected application as default action of this file type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AutoRunDialog</name>
    <message>
        <location filename="../autorun.ui" line="14"/>
        <location filename="../../build/src/ui_autorun.h" line="108"/>
        <source>Removable medium is inserted</source>
        <translation>이동식 매체가 삽입되었습니다</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="33"/>
        <location filename="../../build/src/ui_autorun.h" line="110"/>
        <source>&lt;b&gt;Removable medium is inserted&lt;/b&gt;</source>
        <translation>&lt;b&gt;이동식 매체가 삽입됩니다&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="40"/>
        <location filename="../../build/src/ui_autorun.h" line="111"/>
        <source>Type of medium:</source>
        <translation>매체 유형 :</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="47"/>
        <location filename="../../build/src/ui_autorun.h" line="112"/>
        <source>Detecting...</source>
        <translation>감지 중 ...</translation>
    </message>
    <message>
        <location filename="../autorun.ui" line="56"/>
        <location filename="../../build/src/ui_autorun.h" line="113"/>
        <source>Please select the action you want to perform:</source>
        <translation>수행하려는 작업을 선택하십시오 :</translation>
    </message>
</context>
<context>
    <name>DesktopFolder</name>
    <message>
        <location filename="../desktop-folder.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="72"/>
        <source>Form</source>
        <translation>형식입니다</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="23"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="73"/>
        <source>Desktop</source>
        <translation>데스크탑</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="29"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="74"/>
        <source>Desktop folder:</source>
        <translation>데스크탑 폴더 :</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="36"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="76"/>
        <source>Image file</source>
        <translation>이미지 파일</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="81"/>
        <source>Folder path</source>
        <translation>폴더 경로</translation>
    </message>
    <message>
        <location filename="../desktop-folder.ui" line="49"/>
        <location filename="../../build/src/ui_desktop-folder.h" line="82"/>
        <source>&amp;Browse</source>
        <translation>찾아보기 (&amp;B)</translation>
    </message>
</context>
<context>
    <name>DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktop-preferences.ui" line="14"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="205"/>
        <source>Desktop Preferences</source>
        <translation>데스크탑 환경설정</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="20"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="206"/>
        <source>Background</source>
        <translation>배경</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="42"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="207"/>
        <source>Wallpaper mode:</source>
        <translation>벽지 모드 :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="55"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="208"/>
        <source>Wallpaper image file:</source>
        <translation>배경 화면 이미지 파일 :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="75"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="210"/>
        <source>Background color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="147"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="222"/>
        <source>Text color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="160"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="223"/>
        <source>Shadow color:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="173"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="224"/>
        <source>Font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select background color:</source>
        <translation type="vanished">배경 색 선택 :</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="84"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="212"/>
        <source>Image file</source>
        <translation>이미지 파일</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="90"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="217"/>
        <source>Image file path</source>
        <translation>이미지 파일 경로</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="97"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="218"/>
        <source>&amp;Browse</source>
        <translation>찾아보기 (&amp;B)</translation>
    </message>
    <message>
        <location filename="../desktop-preferences.ui" line="109"/>
        <location filename="../../build/src/ui_desktop-preferences.h" line="219"/>
        <source>Label Text</source>
        <translation>라벨 텍스트</translation>
    </message>
    <message>
        <source>Select  text color:</source>
        <translation type="vanished">텍스트 색상 선택 :</translation>
    </message>
    <message>
        <source>Select shadow color:</source>
        <translation type="vanished">그림자 색상 선택 :</translation>
    </message>
    <message>
        <source>Select font:</source>
        <translation type="vanished">글꼴 선택 :</translation>
    </message>
    <message>
        <source>General</source>
        <translation type="vanished">일반</translation>
    </message>
    <message>
        <source>Window Manager</source>
        <translation type="vanished">창 관리자</translation>
    </message>
    <message>
        <source>Show menus provided by window managers when desktop is clicked</source>
        <translation type="vanished">데스크톱을 클릭하면 창 관리자가 제공하는 메뉴 표시</translation>
    </message>
    <message>
        <source>Advanced</source>
        <translation type="vanished">고급</translation>
    </message>
</context>
<context>
    <name>EditBookmarksDialog</name>
    <message>
        <location filename="../edit-bookmarks.ui" line="14"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="103"/>
        <source>Edit Bookmarks</source>
        <translation type="unfinished">북마크 편집</translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="42"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="106"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="47"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="105"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="67"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="107"/>
        <source>&amp;Add Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="77"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="108"/>
        <source>&amp;Remove Item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../edit-bookmarks.ui" line="102"/>
        <location filename="../../build/src/ui_edit-bookmarks.h" line="109"/>
        <source>Use drag and drop to reorder the items</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExecFileDialog</name>
    <message>
        <location filename="../exec-file.ui" line="14"/>
        <location filename="../../build/src/ui_exec-file.h" line="114"/>
        <source>Execute file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="39"/>
        <location filename="../../build/src/ui_exec-file.h" line="116"/>
        <source>&amp;Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="52"/>
        <location filename="../../build/src/ui_exec-file.h" line="117"/>
        <source>E&amp;xecute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="62"/>
        <location filename="../../build/src/ui_exec-file.h" line="118"/>
        <source>Execute in &amp;Terminal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../exec-file.ui" line="85"/>
        <location filename="../../build/src/ui_exec-file.h" line="119"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileOperationDialog</name>
    <message>
        <location filename="../file-operation-dialog.ui" line="25"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="124"/>
        <source>Destination:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="48"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="126"/>
        <source>Processing:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="61"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="127"/>
        <source>Preparing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="68"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="128"/>
        <source>Progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-operation-dialog.ui" line="88"/>
        <location filename="../../build/src/ui_file-operation-dialog.h" line="129"/>
        <source>Time remaining:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FilePropsDialog</name>
    <message>
        <location filename="../file-props.ui" line="20"/>
        <location filename="../../build/src/ui_file-props.h" line="362"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="60"/>
        <location filename="../../build/src/ui_file-props.h" line="363"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="133"/>
        <location filename="../../build/src/ui_file-props.h" line="365"/>
        <source>General</source>
        <translation type="unfinished">일반</translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="139"/>
        <location filename="../../build/src/ui_file-props.h" line="366"/>
        <source>File type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="162"/>
        <location filename="../../build/src/ui_file-props.h" line="368"/>
        <source>File size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="185"/>
        <location filename="../../build/src/ui_file-props.h" line="370"/>
        <source>On-disk size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="208"/>
        <location filename="../../build/src/ui_file-props.h" line="372"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="234"/>
        <location filename="../../build/src/ui_file-props.h" line="374"/>
        <source>Link target:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="260"/>
        <location filename="../../build/src/ui_file-props.h" line="376"/>
        <source>Last modified:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="283"/>
        <location filename="../../build/src/ui_file-props.h" line="378"/>
        <source>Last accessed:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="317"/>
        <location filename="../../build/src/ui_file-props.h" line="380"/>
        <source>Open with</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="323"/>
        <location filename="../../build/src/ui_file-props.h" line="381"/>
        <source>Mime type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="346"/>
        <location filename="../../build/src/ui_file-props.h" line="383"/>
        <source>Open With:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="380"/>
        <location filename="../../build/src/ui_file-props.h" line="384"/>
        <source>Access Control</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="423"/>
        <location filename="../../build/src/ui_file-props.h" line="385"/>
        <source>Everyone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-props.ui" line="440"/>
        <location filename="../../build/src/ui_file-props.h" line="386"/>
        <source>Make the file executable</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Filer::Application</name>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>Name of configuration profile</source>
        <translation>구성 프로필 이름</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="189"/>
        <source>PROFILE</source>
        <translation>프로필</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="192"/>
        <source>Run Filer as a daemon</source>
        <translation>Filer 를 데몬으로 실행</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="195"/>
        <source>Quit Filer</source>
        <translation>Filer 를 종료</translation>
    </message>
    <message>
        <source>Launch desktop manager</source>
        <translation type="vanished">데스크탑 관리자 실행</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="198"/>
        <source>Launch desktop manager (deprecated)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../application.cpp" line="201"/>
        <source>Turn off desktop manager if it&apos;s running</source>
        <translation>데스크톱 관리자가 실행중인 경우 끄기</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <source>Open desktop preference dialog on the page with the specified name</source>
        <translation>지정된 이름으로 페이지에서 데스크탑 환경설정 대화 상자를 엽니 다</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="204"/>
        <location filename="../application.cpp" line="220"/>
        <source>NAME</source>
        <translation>이름</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="207"/>
        <source>Open new window</source>
        <translation>새 창 열기</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="210"/>
        <source>Open Find Files utility</source>
        <translation>파일 찾기 유틸리티 열기</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>Set desktop wallpaper from image FILE</source>
        <translation>이미지 파일에서 설정 데스크톱 배경 화면</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="213"/>
        <source>FILE</source>
        <translation>파일</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>Set mode of desktop wallpaper. MODE=(color|stretch|fit|center|tile)</source>
        <translation>바탕 화면 배경 무늬의 모드를 설정합니다. MODE = (색상 | 늘이기 | 맞춤 | 중심 | 타일)</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="217"/>
        <source>MODE</source>
        <translation>모드</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="220"/>
        <source>Open Preferences dialog on the page with the specified name</source>
        <translation>지정된 이름으로 페이지에서 환경설정 대화 상자를 엽니 다</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>Files or directories to open</source>
        <translation>열 파일 또는 디렉토리</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="223"/>
        <source>[FILE1, FILE2,...]</source>
        <translation>[새로운 폴더1, 새로운 폴더2,...]</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="512"/>
        <location filename="../application.cpp" line="519"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../application.cpp" line="519"/>
        <source>Terminal emulator is not set.</source>
        <translation>터미널 에뮬레이터가 설정되지 않았습니다.</translation>
    </message>
</context>
<context>
    <name>Filer::AutoRunDialog</name>
    <message>
        <location filename="../autorundialog.cpp" line="43"/>
        <source>Open in file manager</source>
        <translation>파일 관리자에서 열기</translation>
    </message>
    <message>
        <location filename="../autorundialog.cpp" line="133"/>
        <source>Removable Disk</source>
        <translation>이동식 디스크</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopPreferencesDialog</name>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="49"/>
        <source>Fill with background color only</source>
        <translation>배경색으로 만 채우기</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="50"/>
        <source>Transparent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="51"/>
        <source>Stretch to fill the entire screen</source>
        <translation>전체 화면을 채우기 위해 늘이기</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="52"/>
        <source>Stretch to fit the screen</source>
        <translation>화면에 맞게 늘이기</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="53"/>
        <source>Center on the screen</source>
        <translation>화면 중앙</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="54"/>
        <source>Tile the image to fill the entire screen</source>
        <translation>전체 화면을 채우도록 이미지 타일링</translation>
    </message>
    <message>
        <location filename="../desktoppreferencesdialog.cpp" line="145"/>
        <source>Image Files</source>
        <translation>이미지 파일</translation>
    </message>
</context>
<context>
    <name>Filer::DesktopWindow</name>
    <message>
        <location filename="../desktopwindow.cpp" line="439"/>
        <source>Stic&amp;k to Current Position</source>
        <translation>현재 위치에 충실 (&amp;K)</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="461"/>
        <source>Desktop Preferences</source>
        <translation>데스크탑 환경설정</translation>
    </message>
    <message>
        <location filename="../desktopwindow.cpp" line="528"/>
        <source>Version: %1</source>
        <translation type="unfinished">버전 : %1</translation>
    </message>
</context>
<context>
    <name>Filer::GotoFolderDialog</name>
    <message>
        <location filename="../gotofolderwindow.cpp" line="47"/>
        <source>Go</source>
        <translation type="unfinished">실행</translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="48"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../gotofolderwindow.cpp" line="59"/>
        <source>Go To Folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Filer::MainWindow</name>
    <message>
        <location filename="../mainwindow.cpp" line="210"/>
        <source>Clear text (Ctrl+K)</source>
        <translation>텍스트 지우기 (Ctrl+K)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="761"/>
        <source>Version: %1</source>
        <translation>버전 : %1</translation>
    </message>
    <message>
        <source>&amp;Move to Trash</source>
        <translation type="vanished">휴지통으로 이동 (&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">삭제 (&amp;D)</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1464"/>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1475"/>
        <source>Switch user command is not set.</source>
        <translation>사용자 전환 명령이 설정되지 않았습니다.</translation>
    </message>
</context>
<context>
    <name>Filer::PreferencesDialog</name>
    <message>
        <location filename="../preferencesdialog.cpp" line="192"/>
        <source>Icon View</source>
        <translation>아이콘 보기</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="193"/>
        <source>Compact Icon View</source>
        <translation>컴팩트 아이콘보기</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="194"/>
        <source>Thumbnail View</source>
        <translation>썸네일보기</translation>
    </message>
    <message>
        <location filename="../preferencesdialog.cpp" line="195"/>
        <source>Detailed List View</source>
        <translation>상세 목록보기</translation>
    </message>
</context>
<context>
    <name>Filer::TabPage</name>
    <message>
        <location filename="../tabpage.cpp" line="246"/>
        <source>Error</source>
        <translation>오류</translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="278"/>
        <source>%n items</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="280"/>
        <source>1 item</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="436"/>
        <source>%1 items selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../tabpage.cpp" line="440"/>
        <source>%1 item selected</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <source>Free space: %1 (Total: %2)</source>
        <translation type="vanished">여유 공간 : %1 (전체 : %2)</translation>
    </message>
    <message>
        <location filename="../tabpage.cpp" line="265"/>
        <source>%1 available</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <source>%n item(s)</source>
        <translation type="vanished">
            <numerusform>%n 항목</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source> (%n hidden)</source>
        <translation type="vanished">
            <numerusform> (%n 숨겨진)</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <source>%1 item(s) selected</source>
        <translation type="obsolete">
            <numerusform>%1 선택한 항목</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Filer::View</name>
    <message>
        <source>Open in New T&amp;ab</source>
        <translation type="vanished">새 탭에서 열기 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="111"/>
        <source>Open in New Win&amp;dow</source>
        <translation>새 창에서 열기 (&amp;D)</translation>
    </message>
    <message>
        <location filename="../view.cpp" line="119"/>
        <source>Open in Termina&amp;l</source>
        <translation>터미널에서 열기 (&amp;L)</translation>
    </message>
</context>
<context>
    <name>FindFilesDialog</name>
    <message>
        <location filename="../file-search.ui" line="14"/>
        <source>Find Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="24"/>
        <source>Name/Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="30"/>
        <source>File name patterns</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="36"/>
        <source>Pattern:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="46"/>
        <location filename="../file-search.ui" line="221"/>
        <source>Case insensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="53"/>
        <location filename="../file-search.ui" line="228"/>
        <source>Use regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="63"/>
        <source>Places to search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="76"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="88"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="117"/>
        <source>Search in sub directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="124"/>
        <source>Search hidden files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="135"/>
        <location filename="../file-search.ui" line="141"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="147"/>
        <source>Only search for files of following types:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="154"/>
        <source>Text files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="161"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="168"/>
        <source>Audio files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="175"/>
        <source>Video files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="182"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="206"/>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="212"/>
        <source>File contains</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="252"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="258"/>
        <source>File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="264"/>
        <source>Bigger than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="289"/>
        <source>Smaller than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="317"/>
        <source>Last Modified Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="323"/>
        <source>Earlier than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../file-search.ui" line="340"/>
        <source>Later than:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserComboBox</name>
    <message>
        <location filename="../appchoosercombobox.cpp" line="79"/>
        <source>Customize</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::AppChooserDialog</name>
    <message>
        <location filename="../appchooserdialog.cpp" line="262"/>
        <source>Select an application to open &quot;%1&quot; files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::CreateNewMenu</name>
    <message>
        <location filename="../createnewmenu.cpp" line="29"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../createnewmenu.cpp" line="33"/>
        <source>Blank File</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeModel</name>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="77"/>
        <source>Loading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dirtreemodelitem.cpp" line="208"/>
        <source>&lt;No sub folders&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::DirTreeView</name>
    <message>
        <location filename="../dirtreeview.cpp" line="220"/>
        <source>Open in New Win&amp;dow</source>
        <translation type="unfinished">새 창에서 열기 (&amp;D)</translation>
    </message>
    <message>
        <location filename="../dirtreeview.cpp" line="226"/>
        <source>Open in Termina&amp;l</source>
        <translation type="unfinished">터미널에서 열기 (&amp;L)</translation>
    </message>
</context>
<context>
    <name>Fm::DndActionMenu</name>
    <message>
        <location filename="../dndactionmenu.cpp" line="26"/>
        <source>Copy here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="27"/>
        <source>Move here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="28"/>
        <source>Create symlink here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dndactionmenu.cpp" line="30"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::EditBookmarksDialog</name>
    <message>
        <location filename="../editbookmarksdialog.cpp" line="96"/>
        <source>New bookmark</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::ExecFileDialog</name>
    <message>
        <location filename="../execfiledialog.cpp" line="40"/>
        <source>This text file &apos;%1&apos; seems to be an executable script.
What do you want to do with it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../execfiledialog.cpp" line="45"/>
        <source>This file &apos;%1&apos; is executable. Do you want to execute it?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FileMenu</name>
    <message>
        <location filename="../filemenu.cpp" line="93"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="99"/>
        <source>Show Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="128"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="136"/>
        <source>Open With...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="164"/>
        <source>Other Applications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="170"/>
        <source>Create &amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="177"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="181"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="185"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="190"/>
        <source>&amp;Move to Trash</source>
        <translation type="unfinished">휴지통으로 이동 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="194"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="242"/>
        <source>Get Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="247"/>
        <source>&amp;Empty Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filemenu.cpp" line="349"/>
        <source>Output</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FileOperation</name>
    <message>
        <location filename="../fileoperation.cpp" line="221"/>
        <source>Error</source>
        <translation type="unfinished">오류</translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="222"/>
        <source>Some files cannot be moved to trash can because the underlying file systems don&apos;t support this operation.
Do you want to delete them instead?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="263"/>
        <location filename="../fileoperation.cpp" line="279"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="264"/>
        <source>Do you want to delete the selected files?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperation.cpp" line="280"/>
        <source>Do you want to move the selected files to trash can?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FileOperationDialog</name>
    <message>
        <location filename="../fileoperationdialog.cpp" line="41"/>
        <source>Move files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="42"/>
        <source>Moving the following files to destination folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="45"/>
        <source>Copy Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="46"/>
        <source>Copying the following files to destination folder:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="49"/>
        <source>Trash Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="50"/>
        <source>Moving the following files to trash can:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="53"/>
        <source>Delete Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="54"/>
        <source>Deleting the following files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="59"/>
        <source>Create Symlinks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="60"/>
        <source>Creating symlinks for the following files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="63"/>
        <source>Change Attributes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="64"/>
        <source>Changing attributes of the following files:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="69"/>
        <source>Restore Trashed Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="70"/>
        <source>Restoring the following files from trash can:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fileoperationdialog.cpp" line="139"/>
        <source>Error</source>
        <translation type="unfinished">오류</translation>
    </message>
</context>
<context>
    <name>Fm::FilePropsDialog</name>
    <message>
        <location filename="../filepropsdialog.cpp" line="148"/>
        <source>View folder content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="149"/>
        <source>View and modify folder content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="153"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="154"/>
        <source>Read and write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="156"/>
        <source>Forbidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="274"/>
        <source>Files of different types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="304"/>
        <source>Multiple Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="432"/>
        <source>Apply changes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filepropsdialog.cpp" line="433"/>
        <source>Do you want to recursively apply these changes to all files and sub-folders?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FileSearchDialog</name>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>Error</source>
        <translation type="unfinished">오류</translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="120"/>
        <source>You should add at least add one directory to search.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearchdialog.cpp" line="127"/>
        <source>Select a folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FolderMenu</name>
    <message>
        <location filename="../foldermenu.cpp" line="37"/>
        <source>Create &amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="44"/>
        <source>&amp;Paste</source>
        <translation type="unfinished">붙여넣기 (&amp;P)</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="50"/>
        <source>Select &amp;All</source>
        <translation type="unfinished">전체 선택 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="54"/>
        <source>Invert Selection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="60"/>
        <source>Sorting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="65"/>
        <source>Show Hidden</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="73"/>
        <source>Get Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="99"/>
        <source>By File Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="100"/>
        <source>By Modification Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="101"/>
        <source>By File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="102"/>
        <source>By File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="103"/>
        <source>By File Owner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="115"/>
        <source>Ascending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="120"/>
        <source>Descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="135"/>
        <source>Folder First</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermenu.cpp" line="144"/>
        <source>Case Sensitive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FolderModel</name>
    <message>
        <location filename="../foldermodel.cpp" line="313"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="316"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="319"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="322"/>
        <source>Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../foldermodel.cpp" line="325"/>
        <source>Owner</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::FontButton</name>
    <message>
        <location filename="../fontbutton.cpp" line="46"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../fontbutton.cpp" line="50"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::MountOperationPasswordDialog</name>
    <message>
        <location filename="../mountoperationpassworddialog.cpp" line="40"/>
        <source>&amp;Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::PlacesModel</name>
    <message>
        <location filename="../placesmodel.cpp" line="40"/>
        <source>Devices</source>
        <translation type="unfinished">장치</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="82"/>
        <source>Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="90"/>
        <source>Desktop</source>
        <translation type="unfinished">데스크탑</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="99"/>
        <source>Computer</source>
        <translation type="unfinished">컴퓨터</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="126"/>
        <source>Network</source>
        <translation type="unfinished">네트워크</translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="137"/>
        <source>Bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesmodel.cpp" line="212"/>
        <source>Trash</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::PlacesView</name>
    <message>
        <location filename="../placesview.cpp" line="351"/>
        <source>Open in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="362"/>
        <source>Empty Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="371"/>
        <source>Move Bookmark Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="376"/>
        <source>Move Bookmark Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="380"/>
        <source>Rename Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="383"/>
        <source>Remove Bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="392"/>
        <location filename="../placesview.cpp" line="409"/>
        <source>Unmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="396"/>
        <source>Mount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../placesview.cpp" line="402"/>
        <source>Eject</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Fm::RenameDialog</name>
    <message>
        <location filename="../renamedialog.cpp" line="50"/>
        <location filename="../renamedialog.cpp" line="69"/>
        <source>Type: %1
Size: %2
Modified: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="56"/>
        <source>Type: %1
Modified: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="75"/>
        <source>Type: %1
Modified: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="89"/>
        <source>&amp;Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../renamedialog.cpp" line="91"/>
        <source>&amp;Rename</source>
        <translation type="unfinished">이름 바꾸기 (&amp;R)</translation>
    </message>
</context>
<context>
    <name>Fm::SidePane</name>
    <message>
        <location filename="../sidepane.cpp" line="49"/>
        <location filename="../sidepane.cpp" line="133"/>
        <source>Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="50"/>
        <location filename="../sidepane.cpp" line="135"/>
        <source>Directory Tree</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="143"/>
        <source>Shows list of common places, devices, and bookmarks in sidebar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sidepane.cpp" line="145"/>
        <source>Shows tree of directories in sidebar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../main-win.ui" line="14"/>
        <location filename="../../build/src/ui_main-win.h" line="602"/>
        <source>File Manager</source>
        <translation>파일 관리자</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="271"/>
        <location filename="../../build/src/ui_main-win.h" line="603"/>
        <source>Go &amp;Up</source>
        <translation>상위로 (&amp;U)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="274"/>
        <location filename="../../build/src/ui_main-win.h" line="605"/>
        <source>Go Up</source>
        <translation>상위로</translation>
    </message>
    <message>
        <source>Alt+Up</source>
        <translation type="vanished">Alt+Up</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="286"/>
        <location filename="../../build/src/ui_main-win.h" line="610"/>
        <source>&amp;Home</source>
        <translation>홈 폴더가 (&amp;H)</translation>
    </message>
    <message>
        <source>Alt+Home</source>
        <translation type="vanished">Alt+Home</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="298"/>
        <location filename="../../build/src/ui_main-win.h" line="614"/>
        <source>&amp;Reload</source>
        <translation>새로고침 (&amp;R)</translation>
    </message>
    <message>
        <source>F5</source>
        <translation type="vanished">F5</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="313"/>
        <location filename="../../build/src/ui_main-win.h" line="618"/>
        <source>Go</source>
        <translation>실행</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="325"/>
        <location filename="../../build/src/ui_main-win.h" line="619"/>
        <source>Quit</source>
        <translation>사임하다</translation>
    </message>
    <message>
        <source>&amp;About</source>
        <translation type="vanished">정보 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="343"/>
        <location filename="../../build/src/ui_main-win.h" line="621"/>
        <source>&amp;New Window</source>
        <translation>새 창 (&amp;N)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="346"/>
        <location filename="../../build/src/ui_main-win.h" line="623"/>
        <source>New Window</source>
        <translation>새 창</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="349"/>
        <location filename="../../build/src/ui_main-win.h" line="626"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="357"/>
        <location filename="../../build/src/ui_main-win.h" line="628"/>
        <source>Show &amp;Hidden</source>
        <translation>숨김 표시 (&amp;H)</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="366"/>
        <location filename="../../build/src/ui_main-win.h" line="629"/>
        <source>&amp;Computer</source>
        <translation>컴퓨터 (&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="369"/>
        <location filename="../../build/src/ui_main-win.h" line="631"/>
        <source>Ctrl+Shift+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="378"/>
        <location filename="../../build/src/ui_main-win.h" line="633"/>
        <source>&amp;Trash</source>
        <translation>휴지통 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="381"/>
        <location filename="../../build/src/ui_main-win.h" line="635"/>
        <source>Ctrl+Shift+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="386"/>
        <location filename="../../build/src/ui_main-win.h" line="637"/>
        <source>&amp;Network</source>
        <translation>네트워크 (&amp;N)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="398"/>
        <location filename="../../build/src/ui_main-win.h" line="641"/>
        <source>&amp;Desktop</source>
        <translation>데스크탑 (&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="401"/>
        <location filename="../../build/src/ui_main-win.h" line="643"/>
        <source>Ctrl+Shift+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="410"/>
        <location filename="../../build/src/ui_main-win.h" line="645"/>
        <source>&amp;Add to Bookmarks</source>
        <translation>북마크에 추가 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="415"/>
        <location filename="../../build/src/ui_main-win.h" line="646"/>
        <source>&amp;Applications</source>
        <translation>응용 프로그램 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="418"/>
        <location filename="../../build/src/ui_main-win.h" line="648"/>
        <source>Ctrl+Shift+A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="426"/>
        <location filename="../../build/src/ui_main-win.h" line="650"/>
        <source>Reload</source>
        <translation>새로고침</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="434"/>
        <location filename="../../build/src/ui_main-win.h" line="651"/>
        <source>&amp;Icon View</source>
        <translation>아이콘 보기 (&amp;I)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="437"/>
        <location filename="../../build/src/ui_main-win.h" line="653"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="445"/>
        <location filename="../../build/src/ui_main-win.h" line="655"/>
        <source>&amp;Compact View</source>
        <translation>컴팩트 보기 (&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="453"/>
        <location filename="../../build/src/ui_main-win.h" line="656"/>
        <source>&amp;Detailed List</source>
        <translation>상세 목록 (&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="456"/>
        <location filename="../../build/src/ui_main-win.h" line="658"/>
        <source>Ctrl+2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="464"/>
        <location filename="../../build/src/ui_main-win.h" line="660"/>
        <source>&amp;Thumbnail View</source>
        <translation>썸네일 보기 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="473"/>
        <location filename="../../build/src/ui_main-win.h" line="661"/>
        <source>Cu&amp;t</source>
        <translation>잘라내기 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="476"/>
        <location filename="../../build/src/ui_main-win.h" line="663"/>
        <source>Ctrl+X</source>
        <translation>Ctrl+X</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="485"/>
        <location filename="../../build/src/ui_main-win.h" line="665"/>
        <source>&amp;Copy</source>
        <translation>복사 (&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="488"/>
        <location filename="../../build/src/ui_main-win.h" line="667"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="497"/>
        <location filename="../../build/src/ui_main-win.h" line="669"/>
        <source>&amp;Paste</source>
        <translation>붙여넣기 (&amp;P)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="500"/>
        <location filename="../../build/src/ui_main-win.h" line="671"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="505"/>
        <location filename="../../build/src/ui_main-win.h" line="673"/>
        <source>Select &amp;All</source>
        <translation>전체 선택 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="508"/>
        <location filename="../../build/src/ui_main-win.h" line="675"/>
        <source>Ctrl+A</source>
        <translation>Ctrl+A</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="513"/>
        <location filename="../../build/src/ui_main-win.h" line="677"/>
        <source>Pr&amp;eferences</source>
        <translation>설정 (&amp;E)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="521"/>
        <location filename="../../build/src/ui_main-win.h" line="678"/>
        <source>&amp;Ascending</source>
        <translation>오름차순 (&amp;A)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="529"/>
        <location filename="../../build/src/ui_main-win.h" line="679"/>
        <source>&amp;Descending</source>
        <translation>내림차순 (&amp;D)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="537"/>
        <location filename="../../build/src/ui_main-win.h" line="680"/>
        <source>&amp;By File Name</source>
        <translation>파일 이름별 (&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="545"/>
        <location filename="../../build/src/ui_main-win.h" line="681"/>
        <source>By &amp;Modification Time</source>
        <translation>수정 시간별 (&amp;M)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="553"/>
        <location filename="../../build/src/ui_main-win.h" line="682"/>
        <source>By File &amp;Type</source>
        <translation>파일 유형별 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="561"/>
        <location filename="../../build/src/ui_main-win.h" line="683"/>
        <source>By &amp;Owner</source>
        <translation>소유자별 (&amp;O)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="569"/>
        <location filename="../../build/src/ui_main-win.h" line="684"/>
        <source>&amp;Folder First</source>
        <translation>폴더 먼저 (&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="640"/>
        <location filename="../../build/src/ui_main-win.h" line="707"/>
        <source>&amp;Move to Trash</source>
        <translation type="unfinished">휴지통으로 이동 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="643"/>
        <location filename="../../build/src/ui_main-win.h" line="709"/>
        <source>Ctrl+Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="661"/>
        <location filename="../../build/src/ui_main-win.h" line="716"/>
        <source>Get &amp;Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="664"/>
        <location filename="../../build/src/ui_main-win.h" line="718"/>
        <source>Ctrl+I</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="672"/>
        <location filename="../../build/src/ui_main-win.h" line="720"/>
        <source>&amp;Case Sensitive</source>
        <translation>대소문자 구분 (&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="680"/>
        <location filename="../../build/src/ui_main-win.h" line="721"/>
        <source>By File &amp;Size</source>
        <translation>파일 크기별 (&amp;S)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="685"/>
        <location filename="../../build/src/ui_main-win.h" line="722"/>
        <source>&amp;Close Window</source>
        <translation>창 닫기 (&amp;C)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="720"/>
        <location filename="../../build/src/ui_main-win.h" line="733"/>
        <source>&amp;Folder</source>
        <translation>폴더 (&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="732"/>
        <location filename="../../build/src/ui_main-win.h" line="737"/>
        <source>&amp;Blank File</source>
        <translation>빈 파일 (&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="743"/>
        <location filename="../../build/src/ui_main-win.h" line="743"/>
        <source>Ctrl+F</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="760"/>
        <location filename="../../build/src/ui_main-win.h" line="746"/>
        <source>&amp;Go To Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="763"/>
        <location filename="../../build/src/ui_main-win.h" line="748"/>
        <source>Go To Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="766"/>
        <location filename="../../build/src/ui_main-win.h" line="751"/>
        <source>Ctrl+Shift+G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="771"/>
        <location filename="../../build/src/ui_main-win.h" line="753"/>
        <source>&amp;Downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="774"/>
        <location filename="../../build/src/ui_main-win.h" line="755"/>
        <source>Downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="777"/>
        <location filename="../../build/src/ui_main-win.h" line="758"/>
        <source>Ctrl+Shift+L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="785"/>
        <location filename="../../build/src/ui_main-win.h" line="760"/>
        <source>&amp;Utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="788"/>
        <location filename="../../build/src/ui_main-win.h" line="762"/>
        <source>Utilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="791"/>
        <location filename="../../build/src/ui_main-win.h" line="765"/>
        <source>Ctrl+Shift+U</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="799"/>
        <location filename="../../build/src/ui_main-win.h" line="767"/>
        <source>&amp;Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="802"/>
        <location filename="../../build/src/ui_main-win.h" line="769"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="805"/>
        <location filename="../../build/src/ui_main-win.h" line="772"/>
        <source>Ctrl+Shift+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="813"/>
        <location filename="../../build/src/ui_main-win.h" line="774"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="816"/>
        <location filename="../../build/src/ui_main-win.h" line="776"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="821"/>
        <location filename="../../build/src/ui_main-win.h" line="778"/>
        <source>&amp;Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="824"/>
        <location filename="../../build/src/ui_main-win.h" line="780"/>
        <source>Ctrl+D</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="829"/>
        <location filename="../../build/src/ui_main-win.h" line="782"/>
        <source>Empty Trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="832"/>
        <location filename="../../build/src/ui_main-win.h" line="784"/>
        <source>Ctrl+Alt+Backspace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="837"/>
        <location filename="../../build/src/ui_main-win.h" line="786"/>
        <source>Show Contents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="840"/>
        <location filename="../../build/src/ui_main-win.h" line="788"/>
        <source>Ctrl+Alt+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="849"/>
        <location filename="../main-win.ui" line="852"/>
        <location filename="../../build/src/ui_main-win.h" line="790"/>
        <location filename="../../build/src/ui_main-win.h" line="792"/>
        <source>Go Up and Close Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="855"/>
        <location filename="../../build/src/ui_main-win.h" line="795"/>
        <source>Ctrl+Shift+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="578"/>
        <location filename="../../build/src/ui_main-win.h" line="685"/>
        <source>New &amp;Tab</source>
        <translation>새 탭 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="277"/>
        <location filename="../../build/src/ui_main-win.h" line="608"/>
        <source>Ctrl+Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="289"/>
        <location filename="../../build/src/ui_main-win.h" line="612"/>
        <source>Ctrl+Shift+H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="301"/>
        <location filename="../../build/src/ui_main-win.h" line="616"/>
        <source>Ctrl+Shift+R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="334"/>
        <location filename="../../build/src/ui_main-win.h" line="620"/>
        <source>&amp;About Filer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="581"/>
        <location filename="../../build/src/ui_main-win.h" line="687"/>
        <source>New Tab</source>
        <translation>새 탭</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="584"/>
        <location filename="../main-win.ui" line="701"/>
        <location filename="../../build/src/ui_main-win.h" line="690"/>
        <location filename="../../build/src/ui_main-win.h" line="729"/>
        <source>Ctrl+T</source>
        <translation>Ctrl+T</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="596"/>
        <location filename="../../build/src/ui_main-win.h" line="692"/>
        <source>Go &amp;Back</source>
        <translation>뒤로 (&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="599"/>
        <location filename="../../build/src/ui_main-win.h" line="694"/>
        <source>Go Back</source>
        <translation>뒤로</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="602"/>
        <location filename="../../build/src/ui_main-win.h" line="697"/>
        <source>Alt+Left</source>
        <translation>Alt+왼쪽</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="611"/>
        <location filename="../../build/src/ui_main-win.h" line="699"/>
        <source>Go &amp;Forward</source>
        <translation>앞으로 (&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="614"/>
        <location filename="../../build/src/ui_main-win.h" line="701"/>
        <source>Go Forward</source>
        <translation>앞으로</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="617"/>
        <location filename="../../build/src/ui_main-win.h" line="704"/>
        <source>Alt+Right</source>
        <translation>Alt + 오른쪽</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="631"/>
        <location filename="../../build/src/ui_main-win.h" line="706"/>
        <source>&amp;Invert Selection</source>
        <translation>반전 선택 (&amp;I)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">삭제 (&amp;D)</translation>
    </message>
    <message>
        <source>Del</source>
        <translation type="vanished">Del</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="648"/>
        <location filename="../../build/src/ui_main-win.h" line="711"/>
        <source>&amp;Rename</source>
        <translation>이름 바꾸기 (&amp;R)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="651"/>
        <location filename="../../build/src/ui_main-win.h" line="713"/>
        <source>F2</source>
        <translation>F2</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="656"/>
        <location filename="../../build/src/ui_main-win.h" line="715"/>
        <source>C&amp;lose Tab</source>
        <translation>탭 닫기 (&amp;L)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="688"/>
        <location filename="../../build/src/ui_main-win.h" line="724"/>
        <source>Ctrl+W</source>
        <translation>Ctrl+W</translation>
    </message>
    <message>
        <source>File &amp;Properties</source>
        <translation type="vanished">파일 및 속성</translation>
    </message>
    <message>
        <source>Alt+Return</source>
        <translation type="vanished">Alt+Return\Enter</translation>
    </message>
    <message>
        <source>&amp;Folder Properties</source>
        <translation type="vanished">폴더 속성 (&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="693"/>
        <location filename="../../build/src/ui_main-win.h" line="726"/>
        <source>Edit Bookmarks</source>
        <translation>북마크 편집</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="698"/>
        <location filename="../../build/src/ui_main-win.h" line="727"/>
        <source>Open &amp;Terminal</source>
        <translation>터미널 열기 (&amp;T)</translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="vanished">F4</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="706"/>
        <location filename="../../build/src/ui_main-win.h" line="731"/>
        <source>Open as &amp;Root</source>
        <translation>Root 로 열기 (&amp;R)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="711"/>
        <location filename="../../build/src/ui_main-win.h" line="732"/>
        <source>&amp;Edit Bookmarks</source>
        <translation>북마크 편집</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="389"/>
        <location filename="../main-win.ui" line="723"/>
        <location filename="../../build/src/ui_main-win.h" line="639"/>
        <location filename="../../build/src/ui_main-win.h" line="735"/>
        <source>Ctrl+Shift+N</source>
        <translation>Ctrl+Shift+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="735"/>
        <location filename="../../build/src/ui_main-win.h" line="739"/>
        <source>Ctrl+Alt+N</source>
        <translation>Ctrl+Alt+N</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="740"/>
        <location filename="../../build/src/ui_main-win.h" line="741"/>
        <source>&amp;Find Files</source>
        <translation>파일 찾기 (&amp;F)</translation>
    </message>
    <message>
        <source>F3</source>
        <translation type="vanished">F3</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="751"/>
        <location filename="../../build/src/ui_main-win.h" line="745"/>
        <source>Filter</source>
        <translation>필터</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="93"/>
        <location filename="../../build/src/ui_main-win.h" line="797"/>
        <source>Filter by string...</source>
        <translation>문자열로 필터링 ...</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="117"/>
        <location filename="../../build/src/ui_main-win.h" line="798"/>
        <source>&amp;File</source>
        <translation>파일 (&amp;F)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="121"/>
        <location filename="../../build/src/ui_main-win.h" line="799"/>
        <source>C&amp;reate New</source>
        <translation>새로 만들기 (&amp;R)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="140"/>
        <location filename="../../build/src/ui_main-win.h" line="800"/>
        <source>&amp;Help</source>
        <translation>도움 (&amp;H)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="146"/>
        <location filename="../../build/src/ui_main-win.h" line="801"/>
        <source>&amp;View</source>
        <translation>보기 (&amp;V)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="150"/>
        <location filename="../../build/src/ui_main-win.h" line="802"/>
        <source>&amp;Sorting</source>
        <translation>정렬 (&amp;S)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="176"/>
        <location filename="../../build/src/ui_main-win.h" line="803"/>
        <source>&amp;Edit</source>
        <translation>편집 (&amp;E)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="194"/>
        <location filename="../../build/src/ui_main-win.h" line="804"/>
        <source>&amp;Bookmarks</source>
        <translation>북마크 (&amp;B)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="201"/>
        <location filename="../../build/src/ui_main-win.h" line="805"/>
        <source>&amp;Go</source>
        <translation>가다 (&amp;G)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="222"/>
        <location filename="../../build/src/ui_main-win.h" line="806"/>
        <source>&amp;Tool</source>
        <translation>용구 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../main-win.ui" line="239"/>
        <location filename="../../build/src/ui_main-win.h" line="807"/>
        <source>Main Toolbar</source>
        <translation>메인 툴바</translation>
    </message>
</context>
<context>
    <name>MountOperationPasswordDialog</name>
    <message>
        <location filename="../mount-operation-password.ui" line="20"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="172"/>
        <source>Mount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="48"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="174"/>
        <source>Connect &amp;anonymously</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="58"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="175"/>
        <source>Connect as u&amp;ser:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="79"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="176"/>
        <source>&amp;Username:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="102"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="177"/>
        <source>&amp;Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="112"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="178"/>
        <source>&amp;Domain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="127"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="179"/>
        <source>Forget password &amp;immediately</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="137"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="180"/>
        <source>Remember password until you &amp;logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mount-operation-password.ui" line="147"/>
        <location filename="../../build/src/ui_mount-operation-password.h" line="181"/>
        <source>Remember &amp;forever</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../preferences.ui" line="14"/>
        <location filename="../../build/src/ui_preferences.h" line="641"/>
        <source>Preferences</source>
        <translation>환경설정</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="45"/>
        <location filename="../../build/src/ui_preferences.h" line="650"/>
        <source>User Interface</source>
        <translation>사용자 인터페이스</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="35"/>
        <location filename="../../build/src/ui_preferences.h" line="646"/>
        <source>Behavior</source>
        <translation>행동</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="50"/>
        <location filename="../preferences.ui" line="451"/>
        <location filename="../../build/src/ui_preferences.h" line="652"/>
        <location filename="../../build/src/ui_preferences.h" line="701"/>
        <source>Thumbnail</source>
        <translation>썸네일</translation>
    </message>
    <message>
        <source>Volume</source>
        <translation type="vanished">볼륨</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="55"/>
        <location filename="../../build/src/ui_preferences.h" line="654"/>
        <source>Advanced</source>
        <translation>많은</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="92"/>
        <location filename="../../build/src/ui_preferences.h" line="659"/>
        <source>Save metadata to directories (.DirInfo files)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="102"/>
        <location filename="../../build/src/ui_preferences.h" line="660"/>
        <source>Spatial mode (folders open in a new window)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="166"/>
        <location filename="../../build/src/ui_preferences.h" line="666"/>
        <source>Icons</source>
        <translation>아이콘</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="195"/>
        <location filename="../../build/src/ui_preferences.h" line="668"/>
        <source>Size of big icons:</source>
        <translation>큰 아이콘의 크기 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="205"/>
        <location filename="../../build/src/ui_preferences.h" line="669"/>
        <source>Size of small icons:</source>
        <translation>작은 아이콘 크기 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="215"/>
        <location filename="../../build/src/ui_preferences.h" line="670"/>
        <source>Size of thumbnails:</source>
        <translation>썸네일 크기 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="225"/>
        <location filename="../../build/src/ui_preferences.h" line="671"/>
        <source>Size of side pane icons:</source>
        <translation>측면 창 아이콘의 크기 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="178"/>
        <location filename="../../build/src/ui_preferences.h" line="667"/>
        <source>Icon theme:</source>
        <translation>아이콘 테마 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="298"/>
        <location filename="../../build/src/ui_preferences.h" line="677"/>
        <source>Window</source>
        <translation>창</translation>
    </message>
    <message>
        <source>Always show the tab bar</source>
        <translation type="vanished">항상 탭 표시 줄 표시</translation>
    </message>
    <message>
        <source>Show &apos;Close&apos; buttons on tabs	</source>
        <translation type="vanished">탭에 &apos;닫기&apos;버튼 표시	</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="307"/>
        <location filename="../../build/src/ui_preferences.h" line="678"/>
        <source>Remember the size of the last closed window</source>
        <translation>마지막으로 닫은 창의 크기 기억</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="314"/>
        <location filename="../../build/src/ui_preferences.h" line="679"/>
        <source>Default width of new windows:</source>
        <translation>새 창의 기본 너비 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="328"/>
        <location filename="../../build/src/ui_preferences.h" line="680"/>
        <source>Default height of new windows:</source>
        <translation>새 창의 기본 높이 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="76"/>
        <location filename="../../build/src/ui_preferences.h" line="657"/>
        <source>Browsing</source>
        <translation>보기</translation>
    </message>
    <message>
        <source>Open files with single click</source>
        <translation type="vanished">한 번의 클릭으로 파일 열기</translation>
    </message>
    <message>
        <source>Delay of auto-selection in single click mode (0 to disable)</source>
        <translation type="vanished">단일 클릭 모드에서 자동 선택 지연 (비활성화하려면 0)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="82"/>
        <location filename="../../build/src/ui_preferences.h" line="658"/>
        <source>Default view mode:</source>
        <translation>기본보기 모드 :</translation>
    </message>
    <message>
        <source> sec</source>
        <translation type="vanished"> 초</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="112"/>
        <location filename="../../build/src/ui_preferences.h" line="661"/>
        <source>File Operations</source>
        <translation>파일 작업</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="118"/>
        <location filename="../../build/src/ui_preferences.h" line="662"/>
        <source>Confirm before deleting files</source>
        <translation>파일을 삭제하기 전에 확인</translation>
    </message>
    <message>
        <source>Move deleted files to &quot;trash bin&quot; instead of erasing from disk.</source>
        <translation type="vanished">삭제 된 파일을 디스크에서 지우는 대신 &quot;휴지통&quot;으로 이동합니다.</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="481"/>
        <location filename="../../build/src/ui_preferences.h" line="705"/>
        <source>Show thumbnails of files</source>
        <translation>파일 썸네일 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="474"/>
        <location filename="../../build/src/ui_preferences.h" line="704"/>
        <source>Only show thumbnails for local files</source>
        <translation>로컬 파일의 축소판 만 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="40"/>
        <location filename="../../build/src/ui_preferences.h" line="648"/>
        <source>Display</source>
        <translation>디스플레이</translation>
    </message>
    <message>
        <source>Bookmarks:</source>
        <translation type="vanished">북마크 :</translation>
    </message>
    <message>
        <source>Open in current tab</source>
        <translation type="vanished">현재 탭에서 열기</translation>
    </message>
    <message>
        <source>Open in new tab</source>
        <translation type="vanished">새 탭에서 열기</translation>
    </message>
    <message>
        <source>Open in new window</source>
        <translation type="vanished">새 창에서 열기</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="125"/>
        <location filename="../../build/src/ui_preferences.h" line="663"/>
        <source>Erase files on removable media instead of &quot;trash can&quot; creation</source>
        <translation>&quot;휴지통&quot;생성 대신 이동식 미디어에있는 파일 지우기</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="132"/>
        <location filename="../../build/src/ui_preferences.h" line="664"/>
        <source>Confirm before moving files into &quot;trash can&quot;</source>
        <translation>파일을 &quot;휴지통&quot;으로 이동하기 전에 확인</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="139"/>
        <location filename="../../build/src/ui_preferences.h" line="665"/>
        <source>Don&apos;t ask options on launch executable file</source>
        <translation>실행 파일 실행시 옵션을 묻지 않음</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="238"/>
        <location filename="../../build/src/ui_preferences.h" line="672"/>
        <source>User interface</source>
        <translation>사용자 인터페이스</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="251"/>
        <location filename="../../build/src/ui_preferences.h" line="674"/>
        <source>Treat backup files as hidden</source>
        <translation>백업 파일을 숨김으로 처리</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="261"/>
        <location filename="../../build/src/ui_preferences.h" line="675"/>
        <source>Always show full file names</source>
        <translation>항상 전체 파일 이름 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="271"/>
        <location filename="../../build/src/ui_preferences.h" line="676"/>
        <source>Show icons of hidden files shadowed</source>
        <translation>숨겨진 파일의 아이콘 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="345"/>
        <location filename="../../build/src/ui_preferences.h" line="681"/>
        <source>Show in places</source>
        <translation>장소에 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="352"/>
        <location filename="../../build/src/ui_preferences.h" line="686"/>
        <source>Home</source>
        <translation>홈포인트</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="364"/>
        <location filename="../../build/src/ui_preferences.h" line="688"/>
        <source>Desktop</source>
        <translation>데스크탑</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="376"/>
        <location filename="../../build/src/ui_preferences.h" line="690"/>
        <source>Trash can</source>
        <translation>휴지통</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="388"/>
        <location filename="../../build/src/ui_preferences.h" line="692"/>
        <source>Computer</source>
        <translation>컴퓨터</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="400"/>
        <location filename="../../build/src/ui_preferences.h" line="694"/>
        <source>Applications</source>
        <translation>응용 프로그램</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="408"/>
        <location filename="../../build/src/ui_preferences.h" line="696"/>
        <source>Devices</source>
        <translation>장치</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="416"/>
        <location filename="../../build/src/ui_preferences.h" line="698"/>
        <source>Network</source>
        <translation>네트워크</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="457"/>
        <location filename="../../build/src/ui_preferences.h" line="702"/>
        <source>Do not generate thumbnails for image files exceeding this size:</source>
        <translation>이 크기를 초과하는 이미지 파일에 대한 축소판 을 생성하지 마십시오 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="464"/>
        <location filename="../../build/src/ui_preferences.h" line="703"/>
        <source> KB</source>
        <translation> KB</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="605"/>
        <location filename="../../build/src/ui_preferences.h" line="715"/>
        <source>Auto Mount</source>
        <translation>자동 마운트</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="611"/>
        <location filename="../../build/src/ui_preferences.h" line="716"/>
        <source>Mount mountable volumes automatically on program startup</source>
        <translation>프로그램 시작시 마운트 가능한 볼륨을 자동으로 마운트</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="618"/>
        <location filename="../../build/src/ui_preferences.h" line="717"/>
        <source>Mount removable media automatically when they are inserted</source>
        <translation>이동식 미디어를 삽입하면 자동으로 마운트</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="625"/>
        <location filename="../../build/src/ui_preferences.h" line="718"/>
        <source>Show available options for removable media when they are inserted</source>
        <translation>이동식 미디어에 사용할 수 있는 옵션을 삽입할 때 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="635"/>
        <location filename="../../build/src/ui_preferences.h" line="719"/>
        <source>When removable medium unmounted:</source>
        <translation>이동식 매체가 마운트 해제 된 경우 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="641"/>
        <location filename="../../build/src/ui_preferences.h" line="720"/>
        <source>Close &amp;tab containing removable medium</source>
        <translation>이동식 매체를 포함하는 탭 닫기 (&amp;T)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="648"/>
        <location filename="../../build/src/ui_preferences.h" line="721"/>
        <source>Chan&amp;ge folder in the tab to home folder</source>
        <translation>탭의 폴더를 홈 폴더로 변경 (&amp;G)</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="508"/>
        <location filename="../../build/src/ui_preferences.h" line="706"/>
        <source>Programs</source>
        <translation>프로그램</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="517"/>
        <location filename="../../build/src/ui_preferences.h" line="707"/>
        <source>Terminal emulator:</source>
        <translation>터미널 에뮬레이터 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="524"/>
        <location filename="../../build/src/ui_preferences.h" line="708"/>
        <source>Switch &amp;user command:</source>
        <translation>사용자 전환 명령 (&amp;U) :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="537"/>
        <location filename="../../build/src/ui_preferences.h" line="709"/>
        <source>Examples: &quot;xterm -e %s&quot; for terminal or &quot;gksu %s&quot; for switching user.
%s = the command line you want to execute with terminal or su.</source>
        <translation>예: 단말의 &quot;xterm-e %s&quot;또는 사용자를 스위칭하기위한 &quot;gksu %s&quot;.
%s = 터미널 또는 su로 실행하려는 명령줄입니다.</translation>
    </message>
    <message>
        <source>Archiver in&amp;tegration:</source>
        <translation type="vanished">아카이버 통합 및 통합 :</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="558"/>
        <location filename="../../build/src/ui_preferences.h" line="711"/>
        <source>Templates</source>
        <translation>템플릿</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="564"/>
        <location filename="../../build/src/ui_preferences.h" line="712"/>
        <source>Show only user defined templates in menu</source>
        <translation>메뉴에 사용자 정의 템플릿만 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="571"/>
        <location filename="../../build/src/ui_preferences.h" line="713"/>
        <source>Show only one template for each MIME type</source>
        <translation>각 MIME 유형에 대해 하나의 템플릿만 표시</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="578"/>
        <location filename="../../build/src/ui_preferences.h" line="714"/>
        <source>Run default application after creation from template</source>
        <translation>템플릿에서 생성 후 기본 응용 프로그램 실행</translation>
    </message>
    <message>
        <location filename="../preferences.ui" line="244"/>
        <location filename="../../build/src/ui_preferences.h" line="673"/>
        <source>Use SI decimal prefixes instead of IEC binary prefixes</source>
        <translation>IEC 이진 접두사 대신 SI 소수점 접두사 사용</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filelauncher.cpp" line="186"/>
        <location filename="../mountoperation.cpp" line="185"/>
        <location filename="../utilities.cpp" line="133"/>
        <location filename="../utilities.cpp" line="209"/>
        <location filename="../utilities.cpp" line="285"/>
        <source>Error</source>
        <translation type="unfinished">오류</translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="127"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="128"/>
        <source>Please enter a new name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="133"/>
        <source>The startvolume cannot be renamed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="221"/>
        <source>Create Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="222"/>
        <source>Create File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="226"/>
        <source>Please enter a new file name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="227"/>
        <source>New text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="231"/>
        <source>Please enter a new folder name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="232"/>
        <source>New folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../utilities.cpp" line="237"/>
        <source>Enter a name for the new %1:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenameDialog</name>
    <message>
        <location filename="../rename-dialog.ui" line="14"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="156"/>
        <source>Confirm to replace files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="35"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="157"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;There is already a file with the same name in this location.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;Do you want to replace the existing file?&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="56"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="158"/>
        <source>dest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="63"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="159"/>
        <source>with the following file?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="76"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="160"/>
        <source>src file info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="89"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="161"/>
        <source>dest file info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="102"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="162"/>
        <source>src</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="122"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="163"/>
        <source>&amp;File name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../rename-dialog.ui" line="137"/>
        <location filename="../../build/src/ui_rename-dialog.h" line="164"/>
        <source>Apply this option to all existing files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SearchDialog</name>
    <message>
        <location filename="../filesearch.ui" line="14"/>
        <location filename="../../build/src/ui_filesearch.h" line="398"/>
        <source>Search Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="29"/>
        <location filename="../../build/src/ui_filesearch.h" line="408"/>
        <source>Name/Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="35"/>
        <location filename="../../build/src/ui_filesearch.h" line="399"/>
        <source>File Name Patterns:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="41"/>
        <location filename="../../build/src/ui_filesearch.h" line="400"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="48"/>
        <location filename="../../build/src/ui_filesearch.h" line="401"/>
        <source>Case insensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="55"/>
        <location filename="../../build/src/ui_filesearch.h" line="402"/>
        <source>Use regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="65"/>
        <location filename="../../build/src/ui_filesearch.h" line="403"/>
        <source>Places to Search:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="78"/>
        <location filename="../../build/src/ui_filesearch.h" line="404"/>
        <source>&amp;Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="90"/>
        <location filename="../../build/src/ui_filesearch.h" line="405"/>
        <source>&amp;Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="119"/>
        <location filename="../../build/src/ui_filesearch.h" line="406"/>
        <source>Search in sub directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="126"/>
        <location filename="../../build/src/ui_filesearch.h" line="407"/>
        <source>Search for hidden files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="137"/>
        <location filename="../../build/src/ui_filesearch.h" line="416"/>
        <source>File Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="143"/>
        <location filename="../../build/src/ui_filesearch.h" line="409"/>
        <source>Only search for files of following types:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="149"/>
        <location filename="../../build/src/ui_filesearch.h" line="410"/>
        <source>Text files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="156"/>
        <location filename="../../build/src/ui_filesearch.h" line="411"/>
        <source>Image files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="163"/>
        <location filename="../../build/src/ui_filesearch.h" line="412"/>
        <source>Audio files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="170"/>
        <location filename="../../build/src/ui_filesearch.h" line="413"/>
        <source>Video files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="177"/>
        <location filename="../../build/src/ui_filesearch.h" line="414"/>
        <source>Documents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="184"/>
        <location filename="../../build/src/ui_filesearch.h" line="415"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="208"/>
        <location filename="../../build/src/ui_filesearch.h" line="420"/>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="214"/>
        <location filename="../../build/src/ui_filesearch.h" line="417"/>
        <source>File contains:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="223"/>
        <location filename="../../build/src/ui_filesearch.h" line="418"/>
        <source>Case insensiti&amp;ve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="230"/>
        <location filename="../../build/src/ui_filesearch.h" line="419"/>
        <source>&amp;Use regular expression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="254"/>
        <location filename="../../build/src/ui_filesearch.h" line="437"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="260"/>
        <location filename="../../build/src/ui_filesearch.h" line="421"/>
        <source>File Size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="266"/>
        <location filename="../../build/src/ui_filesearch.h" line="422"/>
        <source>Larger than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="282"/>
        <location filename="../filesearch.ui" line="323"/>
        <location filename="../../build/src/ui_filesearch.h" line="423"/>
        <location filename="../../build/src/ui_filesearch.h" line="429"/>
        <source>Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="287"/>
        <location filename="../filesearch.ui" line="328"/>
        <location filename="../../build/src/ui_filesearch.h" line="424"/>
        <location filename="../../build/src/ui_filesearch.h" line="430"/>
        <source>KiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="292"/>
        <location filename="../filesearch.ui" line="333"/>
        <location filename="../../build/src/ui_filesearch.h" line="425"/>
        <location filename="../../build/src/ui_filesearch.h" line="431"/>
        <source>MiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="297"/>
        <location filename="../filesearch.ui" line="338"/>
        <location filename="../../build/src/ui_filesearch.h" line="426"/>
        <location filename="../../build/src/ui_filesearch.h" line="432"/>
        <source>GiB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="307"/>
        <location filename="../../build/src/ui_filesearch.h" line="428"/>
        <source>Smaller than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="351"/>
        <location filename="../../build/src/ui_filesearch.h" line="434"/>
        <source>Last Modified Time:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="357"/>
        <location filename="../../build/src/ui_filesearch.h" line="435"/>
        <source>Earlier than:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../filesearch.ui" line="364"/>
        <location filename="../../build/src/ui_filesearch.h" line="436"/>
        <source>Later than:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
